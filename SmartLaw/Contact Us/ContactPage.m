//
//  ContactPage.m
//  SmartLaw
//
//  Created by Krystel Chaccour on 9/27/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import "ContactPage.h"

@interface ContactPage ()

@end

@implementation ContactPage
@synthesize background_view, titleView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]]];

    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    if ([self respondsToSelector:@selector(automaticallyAdjustsScrollViewInsets)])
        self.automaticallyAdjustsScrollViewInsets = NO;
    
    // set the navigation bar style (color, tint, etc)
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [ColorTheme barBlack];
    self.navigationController.navigationBar.barTintColor = [ColorTheme barBlack];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;

    // create label for custom navigation bar title
    titleView = [[UILabel alloc] init];
    titleView.frame = CGRectMake(0, 0, 0, 44);
    [titleView setTextAlignment:NSTextAlignmentCenter];
    [titleView setTextColor:[UIColor whiteColor]];
    [titleView setText:JSLocalizedString(@"Contact", @"")];
    [titleView setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]+4]];
    [titleView setBackgroundColor:[UIColor clearColor]];
    [self.navigationItem setTitleView:titleView];
    
    [self setUpDismissButton];
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self setUpInterface];
}

-(void)setUpInterface
{
    [background_view removeFromSuperview];
    background_view = [[UIView alloc] init];
    [background_view setTranslatesAutoresizingMaskIntoConstraints:NO];
    [background_view setBackgroundColor:[UIColor whiteColor]];
    [background_view.layer setCornerRadius:7.0];
    [background_view setUserInteractionEnabled:YES];
    [self.view addSubview:background_view];
    [background_view layoutIfNeeded];
    [background_view setNeedsLayout];

    UITextView *contact_text = [[UITextView alloc] init];
    [contact_text setTranslatesAutoresizingMaskIntoConstraints:NO];
    [contact_text setTextColor:[ColorTheme textGrey]];
    [contact_text setTextAlignment:NSTextAlignmentRight];
    [contact_text setText:@"صندوق البريد 19\n أبوظبي، الإمارات العربية المتحدة\nالامانة العامة للمجلس التنفيذي\n\nهاتف:  +97126688888\n\nالبريد الإلكتروني: info@ecouncil.ae"];
    [contact_text setBackgroundColor:[UIColor clearColor]];
    [contact_text setFont:[FontManager mediumFontOfSize:14]];
    [contact_text setScrollEnabled:YES];
    [contact_text setDataDetectorTypes:UIDataDetectorTypeAll];
    [contact_text setShowsVerticalScrollIndicator:NO];
    [contact_text setShowsHorizontalScrollIndicator:NO];
    [contact_text setEditable:NO];
    [contact_text setAllowsEditingTextAttributes:NO];
    [contact_text setUserInteractionEnabled:YES];
    [background_view addSubview:contact_text];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:background_view forKey:@"bView"];
    [dict setObject:contact_text forKey:@"label"];
    
    float width = self.view.frame.size.width - 40;
    [dict setObject:[NSNumber numberWithFloat:width] forKey:@"width"];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[bView(width)]" options:0 metrics:dict views:dict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[bView(160)]" options:0 metrics:nil views:dict]];

    [background_view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[label]-10-|" options:0 metrics:dict views:dict]];
    [background_view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[label]-10-|" options:0 metrics:nil views:dict]];

}

-(void)setUpDismissButton
{
    // add custom back button to navigation bar
    UIImage *dismiss_image= [UIImage imageNamed:@"Left-Arrow"];
    self.dismiss_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.dismiss_btn.bounds = CGRectMake( 10, 0, dismiss_image.size.width, dismiss_image.size.height);
    [self.dismiss_btn addTarget:self action:@selector(dismissPage) forControlEvents:UIControlEventTouchUpInside];
    [self.dismiss_btn setImage:dismiss_image forState:UIControlStateNormal];
    
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:self.dismiss_btn];
    self.navigationItem.leftBarButtonItem = leftButton;
    
}

// Function called to go back to the previous/parent view controller
-(void)dismissPage
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
