//
//  UIFont+DBZAdditions.h
//  DubizzleHorizontal
//
//  Created by Robert Dimitrov on 4/29/14.
//  Copyright (c) 2014 Dubizzle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIFont (DBZAdditions)

+ (void)dbz_printAllFonts;

// Proxima Nova
+ (UIFont *)dbz_proximaNovaBoldWithSize:(CGFloat)size;
+ (UIFont *)dbz_proximaNovaLightWithSize:(CGFloat)size;
+ (UIFont *)dbz_proximaNovaRegularItalicWithSize:(CGFloat)size;
+ (UIFont *)dbz_proximaNovaRegularWithSize:(CGFloat)size;
+ (UIFont *)dbz_proximaNovaSemiboldWithSize:(CGFloat)size;
+ (UIFont *)dbz_proximaNovaThinItalicWithSize:(CGFloat)size;
+ (UIFont *)dbz_proximaNovaThinWithSize:(CGFloat)size;

// Ubuntu
+ (UIFont *)dbz_ubuntuBoldWithSize:(CGFloat)size;
+ (UIFont *)dbz_ubuntuBoldItalicWithSize:(CGFloat)size;
+ (UIFont *)dbz_ubuntuItalicWithSize:(CGFloat)size;
+ (UIFont *)dbz_ubuntuLightWithSize:(CGFloat)size;
+ (UIFont *)dbz_ubuntuLightItalicWithSize:(CGFloat)size;
+ (UIFont *)dbz_ubuntuMediumWithSize:(CGFloat)size;
+ (UIFont *)dbz_ubuntuMediumItalicWithSize:(CGFloat)size;
+ (UIFont *)dbz_ubuntuRegularWithSize:(CGFloat)size;

@end
