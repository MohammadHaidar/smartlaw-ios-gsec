//
//  ColorTheme.h
//  SmartLaw
//
//  Created by Krystel Chaccour on 3/1/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import <Foundation/Foundation.h>

static NSString *background_grey = @"#d0d2d3";
static NSString *background_beige = @"#f1e8d9";
static NSString *slaw_red = @"#891e04";
static NSString *slaw_lightRed = @"#ee3e43";
static NSString *pantone_grey = @"#adafb1";
static NSString *text_grey = @"#4d4d4d";
static NSString *light_grey = @"#d2d2d2";
static NSString *bar_black = @"#323232";
static NSString *categ_grey = @"#6d6d6d";
static NSString *bar_grey = @"#c5c5c5";

static NSString *highlight_blue = @"#BDE7F6";
static NSString *highlight_red = @"#F33042";
static NSString *highlight_green = @"#AFD6A3";
static NSString *highlight_yellow = @"#FFE5CA";


@interface ColorTheme : NSObject

//  convert hexadecimal colors to UIColor
+(UIColor *)colorFromHexString:(NSString *)hexString;

+(UIColor *)backgroundGrey;
+(UIColor *)backgroundBeige;
+(UIColor *)slawRed;
+(UIColor *)slawLightRed;
+(UIColor *)pantoneGrey;
+(UIColor *)textGrey;
+(UIColor *)barBlack;
+(UIColor *)categGrey;
+(UIColor *)lightGrey;
+(UIColor *)barGrey;

+(UIColor *)highlightBlue;
+(UIColor *)highlightYellow;
+(UIColor *)highlightRed;
+(UIColor *)highlightGreen;

@end
