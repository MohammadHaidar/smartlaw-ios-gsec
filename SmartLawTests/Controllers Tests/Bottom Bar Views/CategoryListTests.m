//
//  CategoryListTests.m
//  SmartLaw
//
//  Created by Krystel Chaccour on 6/15/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "CategoryList.h"
#import "JSLocalizedString.h"

@interface CategoryListTests : XCTestCase
@property CategoryList *categList;
@end

@implementation CategoryListTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.

    self.categList = [[CategoryList alloc] init];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    
    self.categList = nil;
    
    [super tearDown];
}

- (void)testClassInitialization {
    
    XCTAssertNotNil(self.categList, @"Category List should not be nil");
}

- (void)testCollectionView {
    
    [self.categList loadCollectionView];
    
    XCTAssertNotNil(self.categList.categ_collection, @"Category Collection view should not be nil");
}

- (void)testTitleViewLabel {
    
    [self.categList setUpTitleView];
    
    XCTAssertEqualObjects(self.categList.navigationItem.titleView, self.categList.titleView, @"Navigation item should have a title view");
    XCTAssertEqualObjects(self.categList.titleView.text, JSLocalizedString(@"Categories", @""), @"Navigation item should be %@",JSLocalizedString(@"Categories", @""));
}

- (void)testSearchView {
    
    [self.categList setUpSearchButton];
    
    XCTAssertNotNil(self.categList.searchButton, @"Search button shouldn't be nil");
    XCTAssertEqualObjects(self.categList.navigationItem.rightBarButtonItem, self.categList.searchItem, @"Navigation right item should have a search button");
}

- (void)testFetchedCategoryList {
    
    [self.categList fetchCategoriesList];
    
    XCTAssertNotNil(self.categList.categories_list, @"Categories' list is nil");
    
}

@end
