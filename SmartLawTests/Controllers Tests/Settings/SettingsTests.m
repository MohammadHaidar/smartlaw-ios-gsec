//
//  SettingsTests.m
//  SmartLaw
//
//  Created by Krystel Chaccour on 6/15/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "SettingsPage.h"
#import "JSLocalizedString.h"
#import "FontManager.h"
#import <OCMock/OCMock.h>
#import <STTimeSlider/STTimeSlider.h>
#import "AppDelegate.h"

@interface SettingsTests : XCTestCase
@property SettingsPage *settingsPage;
@property UISwitch *temp_switch;
@property UISegmentedControl *temp_segment;
@property STTimeSlider *temp_slider;
@property AppDelegate *temp_delegate;

@end

@implementation SettingsTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    self.settingsPage = [[SettingsPage alloc] init];
    self.temp_switch = [[UISwitch alloc] init];
    self.temp_slider = [[STTimeSlider alloc] init];
    self.temp_segment = [[UISegmentedControl alloc] init];
    self.temp_delegate = ((AppDelegate *)[[UIApplication sharedApplication] delegate]);

    [self.settingsPage loadInterface];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    self.settingsPage = nil;
    self.temp_switch = nil;
    self.temp_slider = nil;
    self.temp_segment = nil;
    self.temp_delegate = nil;
    
    [super tearDown];
}

- (void)testInitialization {
    XCTAssertNotNil(self.settingsPage, @"Settings page should not be nil");
}

- (void)testTitleViewLabel {
    XCTAssertEqualObjects(self.settingsPage.navigationItem.titleView, self.settingsPage.titleView, @"Navigation item should have a title view");
    XCTAssertEqualObjects(self.settingsPage.titleView.text, JSLocalizedString(@"Settings", @""), @"Navigation item should be %@",JSLocalizedString(@"Settings", @""));
}

- (void)testLanguageIsSet {
    
    XCTAssertNotNil([[NSUserDefaults standardUserDefaults] objectForKey:@"language"], @"Language should not be nil");
}

- (void)testSelectLangLabel {
    
    XCTAssertNotNil(self.settingsPage.select_lang, @"Select lang label should not be nil");
    XCTAssertEqualObjects(self.settingsPage.select_lang.text, JSLocalizedString(@"Select Language", @""), @"Settings Lang != %@",JSLocalizedString(@"Select Language", @""));
}

- (void)testDefaultFontLabel {
    
    XCTAssertNotNil(self.settingsPage.default_font, @"Default font should not be nil");
    XCTAssertEqualObjects(self.settingsPage.default_font.text, JSLocalizedString(@"Default font size", @""), @"Default font != %@",JSLocalizedString(@"Default font size", @""));
}

- (void)testGlobalPaginationLabel {
    
    XCTAssertNotNil(self.settingsPage.global_pagination, @"Global Pagination label should not be nil");
    XCTAssertEqualObjects(self.settingsPage.global_pagination.text, JSLocalizedString(@"Allow global pagination", @""), @"Global Pagination != %@",JSLocalizedString(@"Allow global pagination", @""));
}

- (void)testSmallFontLabel {
    
    XCTAssertNotNil(self.settingsPage.small_font, @"Small font label is not nil");
    XCTAssertEqualObjects(self.settingsPage.small_font.text, JSLocalizedString(@"Small", @""), @"Small font != %@",JSLocalizedString(@"Small", @""));
}

- (void)testNormalFontLabel {
    
    XCTAssertNotNil(self.settingsPage.normal_font, @"Normal font label is not nil");
    XCTAssertEqualObjects(self.settingsPage.normal_font.text, JSLocalizedString(@"Normal", @""), @"Normal font != %@",JSLocalizedString(@"Normal", @""));
}

- (void)testLargeFontLabel {
    
    XCTAssertNotNil(self.settingsPage.large_font, @"Large font label should not be nil");
    XCTAssertEqualObjects(self.settingsPage.large_font.text, JSLocalizedString(@"Large", @""), @"Large font != %@", JSLocalizedString(@"Large", @""));
}

- (void)testContentLabel {
    
    XCTAssertNotNil(self.settingsPage.content, @"Content label should not be nil");
    XCTAssertEqualObjects(self.settingsPage.content.text, JSLocalizedString(@"Show content", @""), @"Content != %@", JSLocalizedString(@"Show content", @""));
}

- (void)testScrollView {
    
    XCTAssertNotNil(self.settingsPage.scroll_view, @"Scroll view should not be nil");
}

- (void)testSegmentController {
    
    XCTAssertNotNil(self.settingsPage.language_segment, @"Segment controller should not be nil");
}

- (void)testFontSlider {
    
    XCTAssertNotNil(self.settingsPage.fontSlider, @"Font slider should not be nil");
}

- (void)testSetLangEnglish {
    
    [self.temp_segment setSelectedSegmentIndex:0];
    [self.settingsPage segmentUpdated:self.temp_segment];
    NSString *lang = JSGetLanguage();
    XCTAssertEqualObjects(lang, @"en", @"Language should be 'en'");
}

- (void)testSetLangArabic {
    
    [self.temp_segment setSelectedSegmentIndex:1];
    [self.settingsPage segmentUpdated:self.temp_segment];
    NSString *lang = JSGetLanguage();
    XCTAssertEqualObjects(lang, @"ar", @"Language should be 'ar'");
}


- (void)testShowContent {
    
    [self.temp_switch setTag:03];
    [self.temp_switch setOn:YES];
    [self.settingsPage switchAction:self.temp_switch];
    XCTAssertTrue([[NSUserDefaults standardUserDefaults] boolForKey:@"switchContent"], @"Content should be true");
}

- (void)testHideContent {
    
    [self.temp_switch setTag:03];
    [self.temp_switch setOn:NO];
    [self.settingsPage switchAction:self.temp_switch];
    XCTAssertFalse([[NSUserDefaults standardUserDefaults] boolForKey:@"switchContent"], @"Content should be false");
}

- (void)testPaginationIsOn {
    
    [self.temp_switch setTag:02];
    [self.temp_switch setOn:YES];
    
    id mockConnection = OCMClassMock([Setting class]);
    self.temp_delegate.setting = mockConnection;
    
    [self.settingsPage switchAction:self.temp_switch];
    
    OCMVerify([mockConnection setGlobalPagination:YES]);
}

- (void)testPaginationIsOff {
    
    [self.temp_switch setTag:02];
    [self.temp_switch setOn:NO];
    
    id mockConnection = OCMClassMock([Setting class]);
    self.temp_delegate.setting = mockConnection;
    
    [self.settingsPage switchAction:self.temp_switch];
    
    OCMVerify([mockConnection setGlobalPagination:NO]);
}


- (void)testSmallSliderFuncEn {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"en" forKey:@"language"];
    
    id mockConnection = OCMClassMock([Setting class]);
    self.temp_delegate.setting = mockConnection;
    
    [self.settingsPage timeSlider:self.temp_slider didMoveToPointAtIndex:0];
    
    OCMVerify([mockConnection setFontSizeS:0]);
}

- (void)testSmallSliderFuncAr {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"ar" forKey:@"language"];
    
    id mockConnection = OCMClassMock([Setting class]);
    self.temp_delegate.setting = mockConnection;
    
    [self.settingsPage timeSlider:self.temp_slider didMoveToPointAtIndex:0];

    OCMVerify([mockConnection setFontSizeS:4]);
}

- (void)testNormalSliderFunc {
    
    id mockConnection = OCMClassMock([Setting class]);
    self.temp_delegate.setting = mockConnection;

    [self.settingsPage timeSlider:self.temp_slider didMoveToPointAtIndex:1];
//    [[[mockConnection expect] andReturnValue:OCMOCK_VALUE(1)] fontSizeS];

    OCMVerify([mockConnection setFontSizeS:1]);
}


- (void)testLargeSliderFuncEn {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"en" forKey:@"language"];

    id mockConnection = OCMClassMock([Setting class]);
    self.temp_delegate.setting = mockConnection;
    
    [self.settingsPage timeSlider:self.temp_slider didMoveToPointAtIndex:2];
    
    OCMVerify([mockConnection setFontSizeS:4]);
}

- (void)testLargeSliderFuncAr {
    
    [[NSUserDefaults standardUserDefaults] setObject:@"ar" forKey:@"language"];
    
    id mockConnection = OCMClassMock([Setting class]);
    self.temp_delegate.setting = mockConnection;
    
    [self.settingsPage timeSlider:self.temp_slider didMoveToPointAtIndex:2];
    
    OCMVerify([mockConnection setFontSizeS:0]);
}

@end
