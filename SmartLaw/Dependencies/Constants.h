//
//  Constants.h
//  SmartLaw
//
//  Created by Krystel Chaccour on 3/1/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#ifndef Constants_h
#define Constants_h

//#define stagingURL @"http://62.84.75.75:8585/iCodeService.asmx" //soap
//#define stagingURL @"http://77.235.135.51:8085/iCodeService.aspx"
#define stagingURL @"https://abudhabilegislations.ecouncil.ae/FunctionalPages/Ajax.aspx"
#define htmlURL @"https://abudhabilegislations.ecouncil.ae"
// public ip:  77.235.135.51

#define IS_PORTRAIT     UIInterfaceOrientationIsPortrait([[UIApplication sharedApplication] statusBarOrientation])
#define IS_LANDSCAPE    UIInterfaceOrientationIsLandscape([[UIApplication sharedApplication] statusBarOrientation])

static NSString *LANGUAGE_KEY = @"AppleLanguages";

#endif /* Constants_h */
