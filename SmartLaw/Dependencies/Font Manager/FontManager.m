//
//  FontManager.m
//  SmartLaw
//
//  Created by Krystel Chaccour on 3/1/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import "FontManager.h"

@implementation FontManager

#pragma mark - Default Fonts

+(UIFont *)regularFontOfSize:(CGFloat)pointSize
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"en"])
        return [UIFont fontWithName:@"CorporateS-Regular" size:pointSize];
    else
        return [UIFont fontWithName:@"GESSTextLight-Light" size:pointSize];
}

+(UIFont *)boldFontOfSize:(CGFloat)pointSize
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"en"])
        return [UIFont fontWithName:@"CorporateS-Bold" size:pointSize];
    else
        return [UIFont fontWithName:@"GESSTextBold-Bold" size:pointSize];
}


+(UIFont *) mediumFontOfSize:(CGFloat)pointSize
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"en"])
        return [UIFont fontWithName:@"CorporateS-Bold" size:pointSize];
    else
        return [UIFont fontWithName:@"GESSTextMedium-Medium" size:pointSize];

}

+(UIFont *) lightFontOfSize:(CGFloat)pointSize
{
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"en"])
        return [UIFont fontWithName:@"CorporateS-Light" size:pointSize];
    else
        return [UIFont fontWithName:@"GESSTextLight-Light" size:pointSize];

}

@end
