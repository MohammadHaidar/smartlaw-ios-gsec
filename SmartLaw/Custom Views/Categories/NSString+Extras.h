//
//  NSString+Extras.h
//  DubizzleHorizontal
//
//  Created by Joseph Boston on 02/07/2014.
//  Copyright (c) 2014 dubizzle. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Extras)
- (NSTextAlignment)naturalTextAligment;
@end
