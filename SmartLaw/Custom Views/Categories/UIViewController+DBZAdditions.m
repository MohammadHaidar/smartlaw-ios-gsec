//
//  UIViewController+DBZAdditions.m
//  DubizzleHorizontal
//
//  Created by Robert Dimitrov on 5/7/14.
//  Copyright (c) 2014 Dubizzle. All rights reserved.
//

#import "UIViewController+DBZAdditions.h"

@implementation UIViewController (DBZAdditions)

- (void)dbz_pushViewController:(UIViewController *)viewController animated:(BOOL)animated {
//    viewController.navigationItem.leftBarButtonItem = [[UIBarButtonItem alloc] initWithTitle:
//                                                                                       style:UIBarButtonItemStylePlain
//                                                                                      target:self action:@selector(backButtonTapped:)];
    
    [self.navigationController pushViewController:viewController animated:animated];
}

- (void)backButtonTapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
