//
//  BooksListTests.m
//  SmartLaw
//
//  Created by Krystel Chaccour on 6/15/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "BooksList.h"
#import "JSLocalizedString.h"

@interface BooksListTests : XCTestCase
@property BooksList *bookList;
@end

@implementation BooksListTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    self.bookList = [[BooksList alloc] init];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    
    self.bookList = nil;
    
    [super tearDown];
}

- (void)testClassInitialization {
    
    XCTAssertNotNil(self.bookList, @"Books list class is nil");
}

- (void)testTitleView {
    
    [self.bookList setUpTitleView];
    
    XCTAssertEqualObjects(self.bookList.navigationItem.titleView, self.bookList.titleView, @"Navigation item should have a title view");
    XCTAssertEqualObjects(self.bookList.titleView.text, JSLocalizedString(@"My Books", @""), @"Navigation item should be %@",JSLocalizedString(@"My Books", @""));
}

- (void)testBarView {
    
    [self.bookList loadBar];
    
    XCTAssertNotNil(self.bookList.barView, @"Bar view is nil");
    XCTAssertNotNil(self.bookList.gridView, @"Grid view is nil");
    XCTAssertNotNil(self.bookList.fakeG, @"Fake grid button is nil");
    XCTAssertNotNil(self.bookList.listView, @"List view is nil");
    XCTAssertNotNil(self.bookList.fakeL, @"Fake list button is nil");
    XCTAssertNotNil(self.bookList.sortA, @"Sort Asc is nil");
    XCTAssertNotNil(self.bookList.fakeSA, @"Fake sort asc is nil");
    XCTAssertNotNil(self.bookList.sortZ, @"Sort Desc is nil");
    XCTAssertNotNil(self.bookList.fakeSZ, @"Fake sort desc is nil");
    XCTAssertTrue(self.bookList.gridView.selected, @"Grid view is not selected");
}

- (void)testCollectionView {
    
    [self.bookList loadCollection];
    
    XCTAssertNotNil(self.bookList.grid_collection, @"Collecton is nil");
}

- (void)testTableView {
    
    [self.bookList loadBar];
    [self.bookList loadTable];
    
    XCTAssertNotNil(self.bookList.list_table, @"Table is nil");
}

- (void)testFooterView {
    
    [self.bookList initFooterView];
    
    XCTAssertNotNil(self.bookList.footerView, @"Footer is nil");
}

- (void)testButtonSelectionAsc {
    
    UIButton *btn = [UIButton new];
    [btn setTag:100];
    [self.bookList buttonSelected:btn];
    
}

- (void)testButtonSelectionDesc {
    
    UIButton *btn = [UIButton new];
    [btn setTag:110];
    [self.bookList buttonSelected:btn];
    
}

-(void)testEmptyLabelLib {
    
    [self.bookList emptyLabel];
    
    XCTAssertNotNil(self.bookList.empty_label, @"Empty label is nil");
    XCTAssertEqualObjects(self.bookList.empty_label.text, JSLocalizedString(@"Empty Books", @""), @"Empty label should be %@",JSLocalizedString(@"Empty Books", @""));
}

@end
