//
//  UIView+DBZAdditions.m
//  DubizzleHorizontal
//
//  Created by Robert Dimitrov on 4/29/14.
//  Copyright (c) 2014 Dubizzle. All rights reserved.
//

#import "UIView+DBZAdditions.h"

@implementation UIView (DBZAdditions)

- (UIImage *)dbz_imageWithView:(UIView *)view {
    UIGraphicsBeginImageContextWithOptions(view.bounds.size, view.opaque, 0.0f);
    [view drawViewHierarchyInRect:view.bounds afterScreenUpdates:NO];
    UIImage * snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return snapshotImage;
}

- (UIView *)dbz_getSnapShot {
    return [[UIScreen mainScreen] snapshotViewAfterScreenUpdates:NO];
}

@end
