//
//  LibraryList.m
//  SmartLaw
//
//  Created by Krystel Chaccour on 3/1/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import "LibraryList.h"
#import "ListViewCell.h"
#import "FooterReusableView.h"
#import <TOWebViewController/TOWebViewController.h>

#import "RowObject.h"

@interface LibraryList () <UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate, UIWebViewDelegate>
{
    UIView *uiDisableViewOverlay;
    UIView *viewForSearchBar;
}

@property (nonatomic, assign) float progress;
@property (nonatomic, retain) NSString *viewType, *search_str;
@property (nonatomic, retain) NSString *lawHtml_ID;
@property (nonatomic, retain) NSMutableArray *booksList_arr;
@property (nonatomic, retain) IBOutlet RTSpinKitView *spinner;

@end

#define tableIdentifier @"tableCell"

@implementation LibraryList
@synthesize nodeID;

#pragma mark - View Life Cycle
#pragma mark

-(void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]]];

    [self setUpDismissButton];
    
    // remove notification observers to avoid duplicate calls
    [[NSNotificationCenter defaultCenter] removeObserver:self name:kReachabilityChangedNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"saveFavorite" object:nil];
    
    // call function when favorie is added in law webview
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(favoriteAction:) name:@"saveFavorite" object:nil];

    // call function when connection status changes
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object: nil];

    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    if ([self respondsToSelector:@selector(automaticallyAdjustsScrollViewInsets)])
        self.automaticallyAdjustsScrollViewInsets = NO;

    // set the navigation bar style (color, tint, etc)
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [ColorTheme barBlack];
    self.navigationController.navigationBar.barTintColor = [ColorTheme barBlack];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;


    [self setUpTitleView];
    [self setUpSearchButton];
    
    [self.empty_label setHidden:YES];
    
    [self.booksList_arr removeAllObjects];
    self.booksList_arr = nil;
    self.booksList_arr = [NSMutableArray array];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        if (self.isFavorite == NO)
        {
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"mode"] isEqualToString:@"on"])
            {
                    // function to create the top bar that contains the sort and view type buttons
                    //        [self loadBar];
                    if (self.isSearching == YES)
                    {
                        self.isSearching = YES;
                        [self searchButtonTapped:_searchBar];
                    }
                    else if (self.isSearching == NO)
                    {
                        self.isSearching = NO;
                        
                        [Appdelegate hideLoadingIndicator];
                        [Appdelegate showLoadingIndicatorWithView:self.view andText:JSLocalizedString(@"Loading", @"")];
                        
                        [self retrieveLibraryListWithID:nodeID andDocumentType:self.documentType];
                    }
            }
            else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"mode"] isEqualToString:@"off"])
            {
                if (self.isSearching == YES)
                {
                    self.isSearching = YES;
                    [self searchButtonTapped:_searchBar];
                }
                else if (self.isSearching == NO)
                {
                    self.isSearching = NO;
                    
                    [Appdelegate hideLoadingIndicator];
                    [Appdelegate showLoadingIndicatorWithView:self.view andText:JSLocalizedString(@"Loading", @"")];
                    
                    self.booksList_arr = [NSMutableArray arrayWithArray:[Appdelegate retrieveFromTableWithCondition:(NSString *)nodeID andDocumentType:[self.documentType integerValue]]];
                    
                    if (self.list_table)
                    {
                        [self.list_table reloadData];
                        
                        [self.empty_label setHidden:YES];
                    }
                    else
                    {
                        [self loadTable];
                        [self.list_table reloadData];
                    }
                    
                    
                    double delayInSeconds = 1.3;
                    dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                    dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                        //code to be executed on the main queue after delay
                        [Appdelegate hideLoadingIndicator];
                    });
                }
            }
        }
    });
}

-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    // make sure the status bar is shown
    [self showStatusBar];

    // set the search bar cancel button style and text - set the navigation bar title
    id barButtonAppearanceInSearchBar = [UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil];
    [self.titleView setText:self.navigationTitle];
    [self.titleView setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]+4]];
    [barButtonAppearanceInSearchBar setTitleTextAttributes:@{NSFontAttributeName : [FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]], NSForegroundColorAttributeName : [UIColor whiteColor]} forState:UIControlStateNormal];
    [barButtonAppearanceInSearchBar setTitle:JSLocalizedString(@"Cancel", @"")];
    
    // set the text field in search bar style and text
    for (id object in [[[self.searchBar subviews] objectAtIndex:0] subviews])
    {
        if ([object isKindOfClass:[UITextField class]])
        {
            UITextField *textFieldObject = (UITextField *)object;
            
            textFieldObject.textColor = [UIColor whiteColor];
            [textFieldObject setTextAlignment:NSTextAlignmentRight];
            [textFieldObject setFont:[FontManager lightFontOfSize:16]];
            break;
        }
    }

    dispatch_async(dispatch_get_main_queue(), ^{
        
        if (self.isFavorite == YES)
        {
            self.isSearching = NO;
            
            [Appdelegate hideLoadingIndicator];
            [Appdelegate showLoadingIndicatorWithView:self.view andText:JSLocalizedString(@"Loading", @"")];
            
            NSMutableArray *array = [NSMutableArray array];
            
            if ([[Appdelegate retrieveFavorites] count] > 0)
            {
                for (id lawid in [Appdelegate retrieveFavorites])
                {
                    [array addObject:[lawid objectForKey:@"lawID"]];
                }
            }
            
            if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"mode"] isEqualToString:@"on"])
                [self retrieveFavoritesWithIds:array];
            else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"mode"] isEqualToString:@"off"])
                [self retrieveFavoritesWithIdsFromDB:array];
        }
    });
}

-(void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];
    
    [Appdelegate hideLoadingIndicator];
}

#pragma mark - Dismiss button
#pragma mark -

/**
 Add custom back button to navigation bar
 */
-(void)setUpDismissButton
{
    UIImage *dismiss_image= [UIImage imageNamed:@"Left-Arrow"];
    self.dismiss_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.dismiss_btn.bounds = CGRectMake( 10, 0, dismiss_image.size.width, dismiss_image.size.height);
    [self.dismiss_btn addTarget:self action:@selector(dismissPage) forControlEvents:UIControlEventTouchUpInside];
    [self.dismiss_btn setImage:dismiss_image forState:UIControlStateNormal];
    
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:self.dismiss_btn];
    self.navigationItem.leftBarButtonItem = leftButton;
}

/**
 Function called to go back to the previous/parent view controller
 */
-(void)dismissPage
{
    [self.searchBar resignFirstResponder];
    
    [self.navigationController popViewControllerAnimated:YES];
}

/**
 Function called to go back to the root view controller
 */
-(void)dismissToRoot
{
    [self.navigationController popToRootViewControllerAnimated:YES];
}

#pragma mark - Title View
#pragma mark -

/**
 Create label for custom navigation bar title
 */
-(void)setUpTitleView
{
    self.titleView = [[UILabel alloc] init];
    self.titleView.frame = CGRectMake(0, 0, 0, 44);
    [self.titleView setTextAlignment:NSTextAlignmentCenter];
    [self.titleView setTextColor:[UIColor whiteColor]];
    [self.titleView setBackgroundColor:[UIColor clearColor]];
    [self.titleView setText:self.navigationTitle];
    [self.titleView setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]+4]];
    [self.navigationItem setTitleView:self.titleView];
}

#pragma mark - Search button
#pragma mark -

/**
 Add searchbar in navigation bar
 */
-(void)setUpSearchButton
{
    // create the magnifying glass button
    self.searchButton = [[UIButton alloc] init];
    [self.searchButton setFrame:CGRectMake(10, 0, 20, 20)];
    UIImage *image =[UIImage imageNamed:@"Home-Bar"];
    [self.searchButton setImage:image forState:UIControlStateNormal];
    [self.searchButton setTintColor:[UIColor whiteColor]];
    [_searchButton addTarget:self action:@selector(dismissToRoot) forControlEvents:UIControlEventTouchUpInside];
    
    self.searchItem = [[UIBarButtonItem alloc] initWithCustomView:_searchButton];
    self.navigationItem.rightBarButtonItem = _searchItem;
    
    self.searchBar = [[UISearchBar alloc] init];
    _searchBar.delegate = self;
    [_searchBar setShowsCancelButton:YES];
    [_searchBar setTintColor:[UIColor whiteColor]];
    [_searchBar setBackgroundColor:[UIColor clearColor]];
    [_searchBar setSearchBarStyle:UISearchBarStyleMinimal];
    [self.searchBar sizeToFit];
    _searchBar.autoresizingMask = UIViewAutoresizingFlexibleWidth |UIViewAutoresizingFlexibleBottomMargin;
    
    // create overlay when search is tapped
    uiDisableViewOverlay = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    uiDisableViewOverlay.backgroundColor=[UIColor blackColor];
    uiDisableViewOverlay.alpha = 0;
    uiDisableViewOverlay.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    uiDisableViewOverlay.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    [uiDisableViewOverlay setUserInteractionEnabled:YES];
    UITapGestureRecognizer *tapOverlay = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(dismissSearchBar)];
    [uiDisableViewOverlay addGestureRecognizer:tapOverlay];
    
    // set the search bar cancel button style and text
    id barButtonAppearanceInSearchBar = [UIBarButtonItem appearanceWhenContainedIn:[UISearchBar class], nil];
    [barButtonAppearanceInSearchBar setTitleTextAttributes:@{NSFontAttributeName : [FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]], NSForegroundColorAttributeName : [UIColor whiteColor] } forState:UIControlStateNormal];
    [barButtonAppearanceInSearchBar setTitle:JSLocalizedString(@"Cancel", @"")];
    
    // set the text field in search bar style and text
    for (id object in [[[self.searchBar subviews] objectAtIndex:0] subviews])
    {
        if ([object isKindOfClass:[UITextField class]])
        {
            UITextField *textFieldObject = (UITextField *)object;
            
            textFieldObject.textColor = [UIColor whiteColor];
            [textFieldObject setTextAlignment:NSTextAlignmentRight];
            [textFieldObject setFont:[FontManager lightFontOfSize:16]];
            break;
        }
    }
    
}

#pragma mark - Status Bar
#pragma mark -

/**
 Make sure status bar is not hidden

 @return NO
 */
- (BOOL)prefersStatusBarHidden
{
    return NO;
}

-(void)showStatusBar {
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}

#pragma mark - Retrieve List of books
#pragma mark

/**
 Retrieve list of law online

 @param nodeId different Id to differentiate the origin/result (legistations, cirulars)
 @param documentType 15 for Legistations / 16 for Circulars
 */
-(void)retrieveLibraryListWithID:(NSString *)nodeId andDocumentType:(NSString *)documentType
{
    // check if the internet connection is valid
    if ([Appdelegate.reach currentReachabilityStatus] == NotReachable)
    {
        [Appdelegate hideLoadingIndicator];
        [Appdelegate showConnectionAlertWithView:self.view];
    }
    else
    {
        TacoShell *ts = [[TacoShell alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/GetMobileNodeChildren",stagingURL]]];
        ts.method = BKTSRequestMethodPOST;
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setObject:nodeId forKey:@"ID"];
        [dict setObject:documentType forKey:@"DocumentTypeID"];
        [dict setObject:@"-1" forKey:@"SortOrder"];
        
        ts.POSTDictionary = dict;
        ts.contentType = BKTSRequestContentTypeApplicationJSON;
        
        ts.completionBlock = ^(NSDictionary *dictionary, NSInteger httpResponseStatusCode, id response){
            
            if ([[[[response objectForKey:@"d"] objectForKey:@"Status"] objectForKey:@"StatusCode"] intValue] == 0)
            {
                [self.booksList_arr removeAllObjects];
                self.booksList_arr = nil;
                self.booksList_arr = [NSMutableArray new];

                NSArray *tempList = [[response objectForKey:@"d"] objectForKey:@"TreeStructure"];

                NSString *fileName, *fileID, *parentID, *htmlLink, *masterID;
                NSArray *childNodes;
                NSInteger nodeType, childNodesCount;
                BOOL isSelectable;
                BOOL shouldExpand;
                // Retrieve and Save all parent tasks
                for (int d = 0; d < [tempList count]; d++)
                {
                    fileName = [[tempList objectAtIndex:d] objectForKey:@"Name"];
                    fileID = [[tempList objectAtIndex:d] objectForKey:@"ID"];
                    masterID = [[tempList objectAtIndex:d] objectForKey:@"DocumentID"];
                    parentID = [[tempList objectAtIndex:d] objectForKey:@"ParentID"];
                    htmlLink = [[tempList objectAtIndex:d] objectForKey:@"HTMLLink"];
                    childNodes = [[tempList objectAtIndex:d] objectForKey:@"ChildNodes"];
                    nodeType = [[[tempList objectAtIndex:d] objectForKey:@"NodeType"] integerValue];
                    childNodesCount = [[[tempList objectAtIndex:d] objectForKey:@"ChildNodesCount"] integerValue];
                    isSelectable = [[[tempList objectAtIndex:d] objectForKey:@"Selectable"] boolValue];
                    
                    if (childNodesCount > 0 && childNodes > 0 && isSelectable == YES)
                        shouldExpand = NO;
                    else shouldExpand = NO;

                    [self.booksList_arr addObject:[RowObject fileWithName:fileName fileId:fileID masterId:masterID parentId:parentID nodeType:nodeType selectable:isSelectable childNodesCount:childNodesCount childNodes:childNodes htmlLink:htmlLink expand:shouldExpand isChild:NO isParent:YES depth:0]];
                }
                
                if (self.list_table)
                {
                    [self.list_table reloadData];
                    
                    [self.empty_label setHidden:YES];
                    
                }
                else
                {
                    [self loadTable];
                    [self.list_table reloadData];
                }

                
                double delayInSeconds = 1.3;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    //code to be executed on the main queue after delay
                    [Appdelegate hideLoadingIndicator];
                });
            }
            else
            {

            }
        };
        
        ts.errorBlock = ^(NSError *error){
            [Appdelegate hideLoadingIndicator];
            if (error)
            {
                [self.spinner stopAnimating];
                
                [Appdelegate hideLoadingIndicator];
                [Appdelegate showConnectionAlertWithView:self.view];
                NSLog(@"error: %@", error);
            }
        };
        
        [ts start];
    }
}

#pragma mark - Retrieve Search Results
#pragma mark -

/**
 Retrieve list of laws that confine with the search string entered

 @param searchStr string entered in the search bar/field
 */
-(void)retrieveSearchResultsWithString:(NSString *)searchStr
{
    // check if the internet connection is valid
    if ([Appdelegate.reach currentReachabilityStatus] == NotReachable)
    {
        [Appdelegate hideLoadingIndicator];
        [Appdelegate showConnectionAlertWithView:self.view];
    }
    else
    {
        TacoShell *ts = [[TacoShell alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/SearchMobileData",stagingURL]]];
        ts.method = BKTSRequestMethodPOST;
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setObject:searchStr forKey:@"searchStr"];
        
        ts.POSTDictionary = dict;
        ts.contentType = BKTSRequestContentTypeApplicationJSON;
        
        ts.completionBlock = ^(NSDictionary *dictionary, NSInteger httpResponseStatusCode, id response){
            
            if ([[[[response objectForKey:@"d"] objectForKey:@"Status"] objectForKey:@"StatusCode"] intValue] == 0)
            {
                double delayInSeconds = 1.0;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    //code to be executed on the main queue after delay
                    [Appdelegate hideLoadingIndicator];
                });
                
                [self.booksList_arr removeAllObjects];
                self.booksList_arr = nil;
                self.booksList_arr = [NSMutableArray new];
                
                NSArray *tempList = [[response objectForKey:@"d"] objectForKey:@"TreeStructure"];
                
                NSString *fileName, *fileID, *parentID, *htmlLink, *masterID;
                NSArray *childNodes;
                NSInteger nodeType, childNodesCount;
                BOOL isSelectable;
                
                // Retrieve and Save all parent tasks
                for (int d = 0; d < [tempList count]; d++)
                {
                    fileName = [[tempList objectAtIndex:d] objectForKey:@"Name"];
                    fileID = [[tempList objectAtIndex:d] objectForKey:@"ID"];
                    masterID = [[tempList objectAtIndex:d] objectForKey:@"DocumentID"];
                    parentID = [[tempList objectAtIndex:d] objectForKey:@"ParentID"];
                    htmlLink = [[tempList objectAtIndex:d] objectForKey:@"HTMLLink"];
                    childNodes = [[tempList objectAtIndex:d] objectForKey:@"ChildNodes"];
                    nodeType = [[[tempList objectAtIndex:d] objectForKey:@"NodeType"] integerValue];
                    childNodesCount = [[[tempList objectAtIndex:d] objectForKey:@"ChildNodesCount"] integerValue];
                    isSelectable = [[[tempList objectAtIndex:d] objectForKey:@"Selectable"] boolValue];
                    
                    [self.booksList_arr addObject:[RowObject fileWithName:fileName fileId:fileID masterId:masterID parentId:parentID nodeType:nodeType selectable:isSelectable childNodesCount:childNodesCount childNodes:childNodes htmlLink:htmlLink expand:NO isChild:NO isParent:YES depth:0]];

                }
                
                if (self.list_table)
                {
                    [self.list_table reloadData];
                    
                    if ([self.booksList_arr count] == 0)
                        [self emptyWithType:[self.documentType intValue]];
                    else
                        [self.empty_label setHidden:YES];
                }
                else
                {
                    [self loadTable];
                    [self.list_table reloadData];
                }
                
                [UIView animateWithDuration:0.5f animations:^{
                    [uiDisableViewOverlay setAlpha:0.0]; } completion:nil];
            }
            else
            {
                
            }
        };
        
        ts.errorBlock = ^(NSError *error){
            [Appdelegate hideLoadingIndicator];
            if (error)
            {
                [self.spinner stopAnimating];
                
                [Appdelegate hideLoadingIndicator];
                [Appdelegate showConnectionAlertWithView:self.view];
                NSLog(@"error: %@", error);
            }
        };
        
        [ts start];
    }
}

#pragma mark - Retrieve List of Favorites
#pragma mark -

/**
 Retrieve online list of laws that confine witht the list of ids

 @param favoriteIds array of ids that are saved locally as 'favorites'
 */
-(void)retrieveFavoritesWithIds:(NSArray *)favoriteIds
{
    // check if the internet connection is valid
    if ([Appdelegate.reach currentReachabilityStatus] == NotReachable)
    {
        [Appdelegate hideLoadingIndicator];
        [Appdelegate showConnectionAlertWithView:self.view];
    }
    else
    {
        TacoShell *ts = [[TacoShell alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/GetFavoritDocuments",stagingURL]]];
        ts.method = BKTSRequestMethodPOST;
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setObject:favoriteIds forKey:@"docIds"];
        
        ts.POSTDictionary = dict;
        ts.contentType = BKTSRequestContentTypeApplicationJSON;
        
        ts.completionBlock = ^(NSDictionary *dictionary, NSInteger httpResponseStatusCode, id response){
            
            if ([[[[response objectForKey:@"d"] objectForKey:@"Status"] objectForKey:@"StatusCode"] intValue] == 0)
            {
                double delayInSeconds = 1.0;
                dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, delayInSeconds * NSEC_PER_SEC);
                dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
                    //code to be executed on the main queue after delay
                    [Appdelegate hideLoadingIndicator];
                });
                
                [self.booksList_arr removeAllObjects];
                self.booksList_arr = nil;
                self.booksList_arr = [NSMutableArray new];
                
                NSArray *tempList = [[response objectForKey:@"d"] objectForKey:@"TreeStructure"];
                
                NSString *fileName, *fileID, *parentID, *htmlLink, *masterID;
                NSArray *childNodes;
                NSInteger nodeType, childNodesCount;
                BOOL isSelectable;
                
                // Retrieve and Save all parent tasks
                for (int d = 0; d < [tempList count]; d++)
                {
                    fileName = [[tempList objectAtIndex:d] objectForKey:@"Name"];
                    fileID = [[tempList objectAtIndex:d] objectForKey:@"ID"];
                    masterID = [[tempList objectAtIndex:d] objectForKey:@"DocumentID"];
                    parentID = [[tempList objectAtIndex:d] objectForKey:@"ParentID"];
                    htmlLink = [[tempList objectAtIndex:d] objectForKey:@"HTMLLink"];
                    childNodes = [[tempList objectAtIndex:d] objectForKey:@"ChildNodes"];
                    nodeType = [[[tempList objectAtIndex:d] objectForKey:@"NodeType"] integerValue];
                    childNodesCount = [[[tempList objectAtIndex:d] objectForKey:@"ChildNodesCount"] integerValue];
                    isSelectable = [[[tempList objectAtIndex:d] objectForKey:@"Selectable"] boolValue];
                    
                    [self.booksList_arr addObject:[RowObject fileWithName:fileName fileId:fileID masterId:masterID parentId:parentID nodeType:nodeType selectable:isSelectable childNodesCount:childNodesCount childNodes:childNodes htmlLink:htmlLink expand:NO isChild:NO isParent:YES depth:0]];
                    
                }
                
                if (self.list_table)
                {
                    [self.list_table reloadData];

                    if ([self.booksList_arr count] == 0)
                        [self emptyWithType:[self.documentType intValue]];
                    else
                        [self.empty_label setHidden:YES];
                }
                else
                {
                    [self loadTable];
                    [self.list_table reloadData];
                }
                
                [UIView animateWithDuration:0.5f animations:^{
                    [uiDisableViewOverlay setAlpha:0.0]; } completion:nil];
            }
            else
            {
                
            }
        };
        
        ts.errorBlock = ^(NSError *error){
            [Appdelegate hideLoadingIndicator];
            if (error)
            {
                [self.spinner stopAnimating];
                
                
                [Appdelegate hideLoadingIndicator];
                [Appdelegate showConnectionAlertWithView:self.view];
                NSLog(@"error: %@", error);
            }
        };
        
        [ts start];
    }
}

/**
 Retrieve list of laws from Database that confine witht the list of ids
 
 @param favoriteIds array of ids that are saved locally as 'favorites'
 */
-(void)retrieveFavoritesWithIdsFromDB:(NSArray *)favoriteIds
{
    NSString *strTest =  [[favoriteIds valueForKey:@"description"] componentsJoinedByString:@","];
    
    [self.booksList_arr removeAllObjects];
    self.booksList_arr = nil;
    self.booksList_arr = [NSMutableArray arrayWithArray:[Appdelegate retrieveFromTableWithCondition:strTest andDocumentType:-1]];
    
    if (self.list_table)
    {
        [self.list_table reloadData];
        
        if ([self.booksList_arr count] == 0)
            [self emptyWithType:[self.documentType intValue]];
        else
            [self.empty_label setHidden:YES];
    }
    else
    {
        [self loadTable];
        [self.list_table reloadData];
    }
    
    [UIView animateWithDuration:0.5f animations:^{
        [uiDisableViewOverlay setAlpha:0.0]; } completion:nil];
}

#pragma mark - Favorite
#pragma mark -

/**
 Save favorite law - save the law id
 Delete law - remove the law id
 */
-(void) favoriteAction:(NSNotification *)notification
{
    NSDictionary *dict = [notification userInfo];
    if ([[dict objectForKey:@"status"] isEqualToString:@"delete"])
    {
        if (self.lawHtml_ID)
            [self removeFavoriteToPlistWithID:self.lawHtml_ID];

    }
    else if ([[dict objectForKey:@"status"] isEqualToString:@"add"])
    {
        if (self.lawHtml_ID)
            [self saveFavoriteToPlistWithID:self.lawHtml_ID];
    }
}

/**
 Save law id to local plist (Favorites.plist)

 @param lawID Id of the law to be saved
 */
-(void)saveFavoriteToPlistWithID:(NSString *)lawID
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"Favorites.plist"];
    
    NSMutableArray *favorites = [NSMutableArray arrayWithContentsOfFile:plistPath];
    
    if (favorites == nil) {
        favorites = [[NSMutableArray alloc] initWithCapacity:0];
    }
    
    NSMutableDictionary *array = [[NSMutableDictionary alloc]init];
    [array setObject:lawID forKey:@"lawID"];
    [favorites addObject:array];
    [favorites writeToFile:plistPath atomically: TRUE];
}

/**
 Remove law id to local plist (Favorites.plist)
 
 @param lawID Id of the law to be removed
 */
-(void)removeFavoriteToPlistWithID:(NSString *)lawID
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"Favorites.plist"];
    
    NSMutableArray *favorites = [NSMutableArray arrayWithContentsOfFile:plistPath];
    
    if (favorites != nil && [favorites count] > 0) {
    {
        for (id lawid in favorites)
        {
            if ([[lawid objectForKey:@"lawID"] integerValue] == [lawID integerValue])
            {
                [favorites removeObject:lawid];
                break;
            }
        }
    }
     
        [favorites writeToFile:plistPath atomically: TRUE];

    }
}

#pragma mark - Table
#pragma mark

/**
 Create the table view (list view)
 */
-(void)loadTable
{
    if (!self.list_table) // if table view is not yet initialized and added to view
    {
        self.list_table = [[UITableView alloc] init];
        [self.list_table setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.list_table setDelegate:self];
        [self.list_table setDataSource:self];
        [self.list_table setScrollEnabled:YES];
        [self.list_table setScrollsToTop:YES];
        [self.list_table setShowsHorizontalScrollIndicator:NO];
        [self.list_table setShowsVerticalScrollIndicator:NO];
        [self.list_table setSeparatorStyle:UITableViewCellSeparatorStyleSingleLine];
        [self.list_table setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]]];
        [self.view addSubview:self.list_table];
        
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[_list_table]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_list_table)]];
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[_list_table]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_list_table)]];
        
        [self.list_table registerClass:[ListViewCell class] forCellReuseIdentifier:tableIdentifier];
        
        if ([self.booksList_arr count] == 0)
            [self emptyWithType:[self.documentType intValue]];
        else
            [self.empty_label setHidden:YES];
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.booksList_arr count];
}

/**
 Calculate the height of each row depending on the content
 */
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    RowObject *law = self.booksList_arr[indexPath.row];

    CGSize maximumLabelSize = CGSizeMake(tableView.frame.size.width -20, 10000);
    
    int height = 0;
    
    UIFont *fontDate = [FontManager mediumFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]];
    CGRect expectedLabelSizeDate = [law.file_name boundingRectWithSize:maximumLabelSize  options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:fontDate} context:nil];
    
    height = expectedLabelSizeDate.size.height + 40;

    return height;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ListViewCell *cell = (ListViewCell *)[tableView dequeueReusableCellWithIdentifier:tableIdentifier];
    RowObject *law = self.booksList_arr[indexPath.row];

    if (cell)
    {
        [cell.book_title setText:law.file_name];
        
        switch (law.nodeType) {
            case 1:
                [cell.rowType setImage:[UIImage imageNamed:@"Law_Folder"]];
                break;
            case 2:
                [cell.rowType setImage:[UIImage imageNamed:@"Law_Balance"]];
                break;
            case 3:
                [cell.rowType setImage:[UIImage imageNamed:@"Law_Film"]];
                break;
            case 4:
                [cell.rowType setImage:[UIImage imageNamed:@"Law_File"]];
                break;
            default:
                [cell.rowType setImage:[UIImage imageNamed:@"Law_File"]];
                break;
        }
        
        if (law.selectable == true && law.childNodesCount > 0)
        {
            [cell.accessoryBtn setHidden:NO];
            [cell.fakeView setHidden:NO];

            [cell.accessoryBtn setTag:indexPath.row];
            [cell.fakeView setTag:indexPath.row];
            
            [cell.fakeView addTarget:self action:@selector(expand:) forControlEvents:UIControlEventTouchUpInside];
            [cell.accessoryBtn addTarget:self action:@selector(expand:) forControlEvents:UIControlEventTouchUpInside];
        }
        else
        {
            [cell.accessoryBtn setHidden:YES];
            [cell.fakeView setHidden:YES];
        }
        
        if (self.isSearching == YES)
        {
            cell.indentationLevel = 0;
            cell.indentationWidth = 0;
        }
        else
        {
            if (law.isChild == YES)
            {
                cell.indentationLevel = law.depth;
                cell.indentationWidth = 10.0;
            }
            else
            {
                cell.indentationLevel = 0;
                cell.indentationWidth = 0;
            }
        }
        
        if (law.expand == YES)
            [cell.accessoryBtn setStyle:kFRDLivelyButtonStyleCaretUp animated:NO];
        else if (law.expand == NO)
            [cell.accessoryBtn setStyle:kFRDLivelyButtonStyleCaretDown animated:NO];
    }
    
    cell.tag = indexPath.row;
    cell.backgroundColor = [UIColor whiteColor];
//    cell.layer.masksToBounds = YES;
    [cell setSelectionStyle:UITableViewCellSelectionStyleGray];
    
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    RowObject *law = self.booksList_arr[indexPath.row];

    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (law.selectable == false)
    {
        LibraryList *new_self = [[LibraryList alloc] init];
        new_self.nodeID = law.file_id;
        new_self.documentType = self.documentType;
        new_self.navigationTitle = law.file_name;
        [self.navigationController pushViewController:new_self animated:YES];

    }
    else if (law.selectable == true && (law.childNodesCount > 0 || law.childNodesCount == 0))
    {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"mode"] isEqualToString:@"on"])
            [self loadWebViewWithHtmlString:[NSString stringWithFormat:@"%@%@",htmlURL,law.htmlLink] withID:law.master_id];
        else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"mode"] isEqualToString:@"off"])
        {
            NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
            NSString *indexPath = [[paths objectAtIndex:0] stringByAppendingString:law.htmlLink];

            [self loadWebViewWithHtmlString:indexPath withID:law.master_id];
        }
    }
}

/*
 ** Function to handle show/hide of task children: expand - collapse table tree
 */
-(IBAction)expand:(FRDLivelyButton*)sender
{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
    RowObject *law = self.booksList_arr[indexPath.row];
    NSArray *getChildren;
    NSMutableArray *children = [NSMutableArray array];
    
    NSInteger newDepth = law.depth + 1;
    
    BOOL shouldExpand;
    

    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"mode"] isEqualToString:@"off"])
    {
        children = [NSMutableArray arrayWithArray:[Appdelegate retrieveChildrenFromTableWithCondition:law.file_id andDepth:newDepth]];
    }
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"mode"] isEqualToString:@"on"])
    {
        getChildren = [NSArray arrayWithArray:law.childNodes];
   
        for (int i =0; i< [getChildren count]; i++)
        {
            if ([[[getChildren objectAtIndex:i] objectForKey:@"ChildNodesCount"] integerValue] > 0 && [[[getChildren objectAtIndex:i] objectForKey:@"ChildNodes"] count] > 0 && [[[getChildren objectAtIndex:i] objectForKey:@"Selectable"] boolValue] == YES)
                shouldExpand = YES;
            else shouldExpand = NO;
            
            [children insertObject:[RowObject fileWithName:[[getChildren objectAtIndex:i] objectForKey:@"Name"] fileId:[[getChildren objectAtIndex:i] objectForKey:@"ID"] masterId:[[getChildren objectAtIndex:i] objectForKey:@"DocumentID"] parentId:[[getChildren objectAtIndex:i] objectForKey:@"ParentID"] nodeType:[[[getChildren objectAtIndex:i] objectForKey:@"NodeType"]integerValue] selectable:[[[getChildren objectAtIndex:i] objectForKey:@"Selectable"] boolValue] childNodesCount:[[[getChildren objectAtIndex:i] objectForKey:@"ChildNodesCount"]integerValue] childNodes:[[getChildren objectAtIndex:i] objectForKey:@"ChildNodes"] htmlLink:[[getChildren objectAtIndex:i] objectForKey:@"HTMLLink"] expand:NO isChild:YES isParent:NO depth:newDepth] atIndex:i];
        }
    }
    
  
    ListViewCell *cell = (ListViewCell *)[self.list_table cellForRowAtIndexPath:indexPath];
    
    NSUInteger startPosition = [sender tag]+1;
    NSUInteger endPosition = startPosition;
    BOOL expand = NO;
    
    law.expand = !law.expand;
   
    for (int i=0; i<[children count];i ++)
    {
        if (law.expand)
        {
            [self.booksList_arr insertObject:[children objectAtIndex:i] atIndex:endPosition];
            
            expand = YES;
            endPosition++;
        }
        else
        {
            expand = NO;
            endPosition = [self removeAllNodesAtParentNode:law];
            break;
        }
    }
    
    NSMutableArray *indexPathArray = [NSMutableArray array];
    for (NSUInteger i=startPosition; i<endPosition; i++) {
        NSIndexPath *tempIndexPath = [NSIndexPath indexPathForRow:i inSection:0];
        [indexPathArray addObject:tempIndexPath];
    }
        
    //Insert or delete the relevant node
    if ([indexPathArray count] >0)
    {
        if (expand) {
            law.expand = YES;
            [cell.accessoryBtn setStyle:kFRDLivelyButtonStyleCaretUp animated:NO];
            [self.list_table beginUpdates];
            [self.list_table insertRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationFade];
            [self.list_table endUpdates];
        }else{
            law.expand = NO;
            [cell.accessoryBtn setStyle:kFRDLivelyButtonStyleCaretDown animated:NO];
            [self.list_table beginUpdates];
            [self.list_table deleteRowsAtIndexPaths:indexPathArray withRowAnimation:UITableViewRowAnimationFade];
            [self.list_table endUpdates];
        }
    }

    [self.list_table reloadData];
}

/**
 ** Remove all child nodes of the parent node (including the grandson node)
 */
-(NSUInteger)removeAllNodesAtParentNode : (RowObject *)parentNode{
    
    NSUInteger startPosition = [self.booksList_arr indexOfObject:parentNode];
    NSUInteger endPosition = startPosition;
    
    for (NSUInteger i = startPosition+1 ; i < self.booksList_arr.count; i++) {
        RowObject *node = [self.booksList_arr objectAtIndex:i];
        endPosition++;
        
        if (node.depth <= parentNode.depth) {
            break;
        }
        
        if(endPosition == self.booksList_arr.count-1){
            endPosition++;
            node.expand = NO;
            //            parentNode.expand = NO;
            break;
        }
        node.expand = NO;
        
    }
    
    if (endPosition>startPosition) {
        [self.booksList_arr removeObjectsInRange:NSMakeRange(startPosition+1, endPosition-startPosition-1)];
    }
    
    return endPosition;
}

-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return 0.01f;
}

-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return [UIView new];
}

#pragma mark - Load Web View
#pragma mark -

/**
 Create custom web view (TOWebViewController) to display the html files
 */
-(void)loadWebViewWithHtmlString:(NSString *)htmlString withID:(NSString *)lawID
{
    self.lawHtml_ID = lawID;
    TOWebViewController *webView;
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"mode"] isEqualToString:@"on"])
        webView = [[TOWebViewController alloc] initWithURLString:htmlString];
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"mode"] isEqualToString:@"off"])
    {
        if ([self.documentType integerValue] == 0)
            webView = [[TOWebViewController alloc] initWithLocalURL:[NSString stringWithFormat:@"%@?%@", htmlString, self.search_str]];
        else
            webView = [[TOWebViewController alloc] initWithLocalURL:htmlString];
    }
    
    [webView.webView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
    [webView setLoadingBarTintColor:[ColorTheme slawLightRed]];
    [webView setButtonTintColor:[ColorTheme slawLightRed]];
    [webView setShowDoneButton:YES];
    [webView setShowLoadingBar:YES];
    [webView setShowPageTitles:YES];
    [webView setHideWebViewBoundaries:YES];
    [webView setEnableFavorite:YES];
    [webView setShowActionButton:YES];
    [webView setShowUrlWhileLoading:NO];
    [webView.webView setClipsToBounds:YES];

    if ([[Appdelegate retrieveFavorites] count] > 0)
    {
        for (id lawid in [Appdelegate retrieveFavorites])
        {
            if ([[lawid objectForKey:@"lawID"] integerValue] == [lawID integerValue])
            {
                [webView setIsFavStatus:YES];
                break;
            }
            else
                [webView setIsFavStatus:NO];
        }
    }

    [webView.navigationController.navigationBar setTintColor:[ColorTheme slawRed]];
    [self.navigationController pushViewController:webView animated:YES];
}


#pragma mark - Search
#pragma mark

/**
 Called when text in search field change
 */
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText
{
    if (searchText.length == 0)
    {
        if (uiDisableViewOverlay.superview)
        {
            [uiDisableViewOverlay setFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height)];
            uiDisableViewOverlay.alpha = 0.6;
            [self.view bringSubviewToFront:uiDisableViewOverlay];
        }
    }
}

/**
 Called when magnifying glass button pressed
 Update the UI
 */
- (void)searchButtonTapped:(id)sender {
    
    [UIView animateWithDuration:0.3 animations:^{
        _searchButton.alpha = 0.0f;
  
    } completion:^(BOOL finished) {
        
        // remove the search button
        self.navigationItem.rightBarButtonItem = nil;
        // set the frame of search bar
        [_searchBar setFrame:CGRectMake(_searchBar.bounds.origin.x, _searchBar.bounds.origin.x, [UIScreen mainScreen].bounds.size.width -10, _searchBar.bounds.size.height)];

        // add the search bar (which will start out hidden)
        viewForSearchBar = [[UIView alloc]initWithFrame:_searchBar.bounds];
        [viewForSearchBar addSubview:_searchBar];
        self.navigationItem.titleView = viewForSearchBar;
        _searchBar.alpha = 0.0;
        
        [UIView animateWithDuration:0.2
                         animations:^{
                             // show search bar and overlay
                             _searchBar.alpha = 1.0;
                             uiDisableViewOverlay.alpha = 0.6;
                         } completion:^(BOOL finished) {
                             [_searchBar becomeFirstResponder];
                             if (!uiDisableViewOverlay.superview)
                                 [self.view addSubview:uiDisableViewOverlay];
                         }];
        
    }];
}

#pragma mark UISearchBarDelegate methods

-(void)dismissSearchBar
{
    [UIView animateWithDuration:0.2
                     animations:^{
                         uiDisableViewOverlay.alpha = 0;
                     } completion:^(BOOL finished) {
                         [self.searchBar resignFirstResponder];
                     }];
}

/**
 Called when cancel button pressed
 */
- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    [self dismissPage];
}


/**
 Show search cancel button when search field first responder

 */
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
    searchBar.showsCancelButton = YES;
}

/**
 Called when search button pressed
 Call function to retrieve results
 */
-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar
{
    [self.empty_label setHidden:YES];
    [self.searchBar resignFirstResponder];
    
    self.isSearching = YES;
    self.search_str = searchBar.text;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [Appdelegate hideLoadingIndicator];
        [Appdelegate showLoadingIndicatorWithView:self.view andText:JSLocalizedString(@"Loading", @"")];

        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"mode"] isEqualToString:@"on"])
            [self retrieveSearchResultsWithString:self.search_str];
        else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"mode"] isEqualToString:@"off"])
        {
            [self.booksList_arr removeAllObjects];
            self.booksList_arr = nil;
            self.booksList_arr = [NSMutableArray arrayWithArray:[Appdelegate searchDatabaseWithKeywords:self.search_str]];
            
            if (self.list_table)
            {
                [self.list_table reloadData];
                
                if ([self.booksList_arr count] == 0)
                    [self emptyWithType:[self.documentType intValue]];
                else
                    [self.empty_label setHidden:YES];
            }
            else
            {
                [self loadTable];
                [self.list_table reloadData];
            }
            
            [UIView animateWithDuration:0.5f animations:^{
                [uiDisableViewOverlay setAlpha:0.0]; } completion:nil];
        }
    });
    
}

#pragma mark - Empty results
#pragma mark

/**
 Called when the result array is empty: display label with the corresponding text to avoid an empty screen/view

 @param nsType integer value to specify the origin/result
 */
-(void)emptyWithType:(int)nsType
{
    [self.empty_label removeFromSuperview];
    self.empty_label = [[UILabel alloc] init];
    [self.empty_label setTranslatesAutoresizingMaskIntoConstraints:NO];
    if (nsType == 15 || nsType == 16) // library array
        [self.empty_label setText:[NSString stringWithFormat:JSLocalizedString(@"Empty Library", @""), self.navigationTitle]];
    else if (nsType == 0) // search results
        [self.empty_label setText:JSLocalizedString(@"Empty Search", @"")];
    else if (nsType == -1) // my books
        [self.empty_label setText:JSLocalizedString(@"Empty Books", @"")];

    [self.empty_label setFont:[FontManager mediumFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]+4]];
    [self.empty_label setTextColor:[ColorTheme textGrey]];
    [self.empty_label setTextAlignment:NSTextAlignmentCenter];
    [self.empty_label setNumberOfLines:0];
    [self.empty_label setLineBreakMode:NSLineBreakByWordWrapping];
    [self.empty_label setHidden:NO];
    [self.empty_label setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:self.empty_label];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-30-[_empty_label]-30-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(_empty_label)]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.empty_label attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
}

#pragma mark - Reachability
#pragma mark

/**
 Handle connection reachability and change
 */
-(void)reachabilityChanged:(NSNotification *)notification {
    
    Reachability *reach = [notification object];
    if (reach.currentReachabilityStatus == NotReachable)
    {
    }
    else
    {
        // if connection is reachable after being not reachable, then fetch and load the books again
        
            [self.booksList_arr removeAllObjects];
            self.booksList_arr = nil;
        
            dispatch_async(dispatch_get_main_queue(), ^{
                [Appdelegate hideLoadingIndicator];
                [Appdelegate showLoadingIndicatorWithView:self.view andText:JSLocalizedString(@"Loading", @"")];
                
                if (self.isSearching == NO)
                    [self retrieveLibraryListWithID:nodeID andDocumentType:self.documentType];
                else if (self.isSearching == YES)
                    [self retrieveSearchResultsWithString:self.search_str];
            });
    }
}


/**
 Allow only portrait device orientation

 @return UIInterfaceOrientationMaskPortrait
 */
-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

/**
 Make sure the interface doesn't rotate

 @return NO
 */
-(BOOL)shouldAutorotate
{
    return NO;
}

-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
