//
//  LandingPage.m
//  SmartLaw
//
//  Created by Krystel Chaccour on 3/1/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import "LandingPage.h"

#import "LibraryList.h"
#import "SettingsPage.h"
#import "PortalPage.h"
#import "FAQPage.h"
#import "ContactPage.h"
#import <TOWebViewController/TOWebViewController.h>

#define ROUND_BUTTON_WIDTH_HEIGHT_iPhone 60
#define ROUND_BUTTON_WIDTH_HEIGHT_iPad 85
#define Logo_Scale_iPhone 0.3
#define Logo_Scale_iPad 0.01

@interface LandingPage ()
{
    NSMutableDictionary *dict;
}
@end

@implementation LandingPage
@synthesize nc1, nc2, nc3;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]]];

//     Check if language is already saved and set the localization language accordingly
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"en"])
        JSSetLanguage(@"en");
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"ar"])
        JSSetLanguage(@"ar");
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"ar" forKey:@"language"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        JSSetLanguage(@"ar");
    }

    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    if ([self respondsToSelector:@selector(automaticallyAdjustsScrollViewInsets)])
        self.automaticallyAdjustsScrollViewInsets = NO;
    
    // set the navigation bar style (color, tint, etc)
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [ColorTheme barBlack];
    self.navigationController.navigationBar.barTintColor = [ColorTheme barBlack];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;

    dict = [NSMutableDictionary dictionary];
    
    [self setUpTitleView];
    [self showStatusBar];
    [self loadInterface];
}

/**
 Update text and font for 'menu' title labels - when language is changed, view should reload, viewwillappear will get called everytime we go back to landing page therefore title labels UI should be updated here
 */
-(void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];

    [self.titleView setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]+4]];
    [self.titleView setText:JSLocalizedString(@"Home", @"")];

    [self.category_lbl setText:JSLocalizedString(@"Legislations", @"")];
    [self.category_lbl setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [self.ta3mim_lbl setText:JSLocalizedString(@"Circulars", @"")];
    [self.ta3mim_lbl setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [self.myBooks_lbl setText:JSLocalizedString(@"My Books", @"")];
    [self.myBooks_lbl setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [self.portal_lbl setText:JSLocalizedString(@"Portal", @"")];
    [self.portal_lbl setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [self.settings_lbl setText:JSLocalizedString(@"Settings", @"")];
    [self.settings_lbl setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [self.contact_lbl setText:JSLocalizedString(@"Contact", @"")];
    [self.contact_lbl setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [self.faq_lbl setText:JSLocalizedString(@"FAQ", @"")];
    [self.faq_lbl setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [self.search_lbl setText:JSLocalizedString(@"External Search", @"")];
    [self.search_lbl setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
}

-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    // calculate the distance between the logo in the middle and the menu item/button around it
    int distance = self.myBooks_btn.frame.origin.x - (self.logo.frame.origin.x + self.logo.frame.size.width);
    
    [dict setObject:[NSNumber numberWithInt:distance] forKey:@"distance"];
    [dict setObject:[NSNumber numberWithInt:distance + (self.contact_lbl.frame.size.height + 5)] forKey:@"bdistance"];
    [dict setObject:[NSNumber numberWithInt:distance/2] forKey:@"middistance"];

    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[cat_lbl]-(distance)-[logo]" options:0 metrics:dict views:dict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[cat_btn(metric_round)]-5-[cat_lbl]" options:0 metrics:dict views:dict]];

    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[logo]-(bdistance)-[sett_btn(metric_round)]" options:0 metrics:dict views:dict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[sett_btn]-5-[sett_lbl]" options:0 metrics:nil views:dict]];

    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[ta_btn(metric_round)]-(middistance)-[logo]" options:0 metrics:dict views:dict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[ta_btn]-5-[ta_lbl]" options:0 metrics:nil views:dict]];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[logo]-(middistance)-[port_btn(metric_round)]" options:0 metrics:dict views:dict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[port_btn]-5-[port_lbl]" options:0 metrics:nil views:dict]];

    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[logo]-(middistance)-[cont_btn(metric_round)]" options:0 metrics:dict views:dict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[cont_btn]-5-[cont_lbl]" options:0 metrics:nil views:dict]];

    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[logo]-(middistance)-[cont_btn(metric_round)]" options:0 metrics:dict views:dict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[cont_btn]-5-[cont_lbl]" options:0 metrics:nil views:dict]];

    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[sear_btn(metric_round)]-(middistance)-[logo]" options:0 metrics:dict views:dict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[sear_btn]-5-[sear_lbl]" options:0 metrics:nil views:dict]];
}

#pragma mark - Load Interface
#pragma mark -

/**
 Create all the UI of the landing page (buttons and labels)
 */
-(void)loadInterface
{
    UIImage *logo_img = [UIImage imageNamed:@"Logo"];
    self.logo = [[UIImageView alloc] init];
    [self.logo setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.logo setImage:logo_img];
    [self.logo setBackgroundColor:[UIColor clearColor]];
    [self.logo setContentMode:UIViewContentModeScaleAspectFit];
    [self.view addSubview:self.logo];
    [self.logo layoutIfNeeded];
    
    self.category_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.category_btn setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.category_btn setBackgroundColor:[ColorTheme categGrey]];
    self.category_btn.layer.masksToBounds = YES;
    [self.category_btn setImage:[UIImage imageNamed:@"Library"] forState:UIControlStateNormal];
    self.category_btn.layer.cornerRadius = [self isPad] ? ROUND_BUTTON_WIDTH_HEIGHT_iPad/2.0 : ROUND_BUTTON_WIDTH_HEIGHT_iPhone/2.0f;
    [self.category_btn setTag:1];
    [self.category_btn addTarget:self action:@selector(goToView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.category_btn];
    [self.category_btn layoutIfNeeded];
    
    self.category_lbl = [[UILabel alloc] init];
    [self.category_lbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.category_lbl setBackgroundColor:[UIColor clearColor]];
    [self.category_lbl setText:JSLocalizedString(@"Legislations", @"")];
    [self.category_lbl setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [self.category_lbl setTextColor:[ColorTheme textGrey]];
    [self.category_lbl setNumberOfLines:0];
    [self.category_lbl setLineBreakMode:NSLineBreakByWordWrapping];
    [self.view addSubview:self.category_lbl];
    [self.category_lbl layoutIfNeeded];
    
    self.ta3mim_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.ta3mim_btn setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.ta3mim_btn setBackgroundColor:[ColorTheme categGrey]];
    self.ta3mim_btn.layer.masksToBounds = YES;
    self.ta3mim_btn.layer.cornerRadius = [self isPad] ? ROUND_BUTTON_WIDTH_HEIGHT_iPad/2.0 : ROUND_BUTTON_WIDTH_HEIGHT_iPhone/2.0f;
    [self.ta3mim_btn setImage:[UIImage imageNamed:@"Ta3mim"] forState:UIControlStateNormal];
    [self.ta3mim_btn setTag:2];
    [self.ta3mim_btn addTarget:self action:@selector(goToView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.ta3mim_btn];
    [self.ta3mim_btn layoutIfNeeded];
    
    self.ta3mim_lbl = [[UILabel alloc] init];
    [self.ta3mim_lbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.ta3mim_lbl setBackgroundColor:[UIColor clearColor]];
    [self.ta3mim_lbl setText:JSLocalizedString(@"Circulars", @"")];
    [self.ta3mim_lbl setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [self.ta3mim_lbl setTextColor:[ColorTheme textGrey]];
    [self.ta3mim_lbl setNumberOfLines:0];
    [self.ta3mim_lbl setLineBreakMode:NSLineBreakByWordWrapping];
    [self.view addSubview:self.ta3mim_lbl];
    [self.ta3mim_lbl layoutIfNeeded];
    
    self.myBooks_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.myBooks_btn setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.myBooks_btn setBackgroundColor:[ColorTheme categGrey]];
    self.myBooks_btn.layer.masksToBounds = YES;
    self.myBooks_btn.layer.cornerRadius = [self isPad] ? ROUND_BUTTON_WIDTH_HEIGHT_iPad/2.0 : ROUND_BUTTON_WIDTH_HEIGHT_iPhone/2.0f;
    [self.myBooks_btn setImage:[UIImage imageNamed:@"Book"] forState:UIControlStateNormal];
    [self.myBooks_btn setTag:3];
    [self.myBooks_btn addTarget:self action:@selector(goToView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.myBooks_btn];
    [self.myBooks_btn layoutIfNeeded];

    self.myBooks_lbl = [[UILabel alloc] init];
    [self.myBooks_lbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.myBooks_lbl setBackgroundColor:[UIColor clearColor]];
    [self.myBooks_lbl setText:JSLocalizedString(@"My Books", @"")];
    [self.myBooks_lbl setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [self.myBooks_lbl setTextColor:[ColorTheme textGrey]];
    [self.myBooks_lbl setNumberOfLines:0];
    [self.myBooks_lbl setLineBreakMode:NSLineBreakByWordWrapping];
    [self.view addSubview:self.myBooks_lbl];
    [self.myBooks_lbl layoutIfNeeded];
    
    self.portal_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.portal_btn setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.portal_btn setBackgroundColor:[ColorTheme categGrey]];
    self.portal_btn.layer.masksToBounds = YES;
    self.portal_btn.layer.cornerRadius = [self isPad] ? ROUND_BUTTON_WIDTH_HEIGHT_iPad/2.0 : ROUND_BUTTON_WIDTH_HEIGHT_iPhone/2.0f;
    [self.portal_btn setImage:[UIImage imageNamed:@"Portal"] forState:UIControlStateNormal];
    [self.portal_btn setTag:4];
    [self.portal_btn addTarget:self action:@selector(goToView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.portal_btn];
    [self.portal_btn layoutIfNeeded];
    
    self.portal_lbl = [[UILabel alloc] init];
    [self.portal_lbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.portal_lbl setBackgroundColor:[UIColor clearColor]];
    [self.portal_lbl setText:JSLocalizedString(@"Portal", @"")];
    [self.portal_lbl setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [self.portal_lbl setTextColor:[ColorTheme textGrey]];
    [self.portal_lbl setNumberOfLines:0];
    [self.portal_lbl setLineBreakMode:NSLineBreakByWordWrapping];
    [self.view addSubview:self.portal_lbl];
    [self.portal_lbl layoutIfNeeded];
    
    self.settings_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.settings_btn setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.settings_btn setBackgroundColor:[ColorTheme categGrey]];
    [self.settings_btn setImage:[UIImage imageNamed:@"SettingsW"] forState:UIControlStateNormal];
    self.settings_btn.layer.masksToBounds = YES;
    self.settings_btn.layer.cornerRadius = [self isPad] ? ROUND_BUTTON_WIDTH_HEIGHT_iPad/2.0 : ROUND_BUTTON_WIDTH_HEIGHT_iPhone/2.0f;
    [self.settings_btn setTag:5];
    [self.settings_btn addTarget:self action:@selector(goToView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.settings_btn];
    [self.settings_btn layoutIfNeeded];
    
    self.settings_lbl = [[UILabel alloc] init];
    [self.settings_lbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.settings_lbl setBackgroundColor:[UIColor clearColor]];
    [self.settings_lbl setText:JSLocalizedString(@"Settings", @"")];
    [self.settings_lbl setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [self.settings_lbl setTextColor:[ColorTheme textGrey]];
    [self.settings_lbl setNumberOfLines:0];
    [self.settings_lbl setLineBreakMode:NSLineBreakByWordWrapping];
    [self.view addSubview:self.settings_lbl];
    [self.settings_lbl layoutIfNeeded];
    
    self.contact_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.contact_btn setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.contact_btn setBackgroundColor:[ColorTheme categGrey]];
    self.contact_btn.layer.masksToBounds = YES;
    self.contact_btn.layer.cornerRadius = [self isPad] ? ROUND_BUTTON_WIDTH_HEIGHT_iPad/2.0 : ROUND_BUTTON_WIDTH_HEIGHT_iPhone/2.0f;
    [self.contact_btn setImage:[UIImage imageNamed:@"Contact"] forState:UIControlStateNormal];
    [self.contact_btn setTag:6];
    [self.contact_btn addTarget:self action:@selector(goToView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.contact_btn];
    [self.contact_btn layoutIfNeeded];
    
    self.contact_lbl = [[UILabel alloc] init];
    [self.contact_lbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.contact_lbl setBackgroundColor:[UIColor clearColor]];
    [self.contact_lbl setText:JSLocalizedString(@"Contact", @"")];
    [self.contact_lbl setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [self.contact_lbl setTextColor:[ColorTheme textGrey]];
    [self.contact_lbl setNumberOfLines:0];
    [self.contact_lbl setLineBreakMode:NSLineBreakByWordWrapping];
    [self.view addSubview:self.contact_lbl];
    [self.contact_lbl layoutIfNeeded];
    
    self.faq_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.faq_btn setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.faq_btn setBackgroundColor:[ColorTheme categGrey]];
    self.faq_btn.layer.masksToBounds = YES;
    self.faq_btn.layer.cornerRadius = [self isPad] ? ROUND_BUTTON_WIDTH_HEIGHT_iPad/2.0 : ROUND_BUTTON_WIDTH_HEIGHT_iPhone/2.0f;
    [self.faq_btn setImage:[UIImage imageNamed:@"FAQ"] forState:UIControlStateNormal];
    [self.faq_btn setTag:7];
    [self.faq_btn addTarget:self action:@selector(goToView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.faq_btn];
    [self.faq_btn layoutIfNeeded];
    
    self.faq_lbl = [[UILabel alloc] init];
    [self.faq_lbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.faq_lbl setBackgroundColor:[UIColor clearColor]];
    [self.faq_lbl setText:JSLocalizedString(@"FAQ", @"")];
    [self.faq_lbl setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [self.faq_lbl setTextColor:[ColorTheme textGrey]];
    [self.faq_lbl setNumberOfLines:0];
    [self.faq_lbl setLineBreakMode:NSLineBreakByWordWrapping];
    [self.view addSubview:self.faq_lbl];
    [self.faq_lbl layoutIfNeeded];
    
    self.search_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.search_btn setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.search_btn setBackgroundColor:[ColorTheme categGrey]];
    self.search_btn.layer.masksToBounds = YES;
    self.search_btn.layer.cornerRadius = [self isPad] ? ROUND_BUTTON_WIDTH_HEIGHT_iPad/2.0 : ROUND_BUTTON_WIDTH_HEIGHT_iPhone/2.0f;
    [self.search_btn setImage:[UIImage imageNamed:@"Search-White"] forState:UIControlStateNormal];
    [self.search_btn setTag:8];
    [self.search_btn addTarget:self action:@selector(goToView:) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:self.search_btn];
    [self.search_btn layoutIfNeeded];
    
    self.search_lbl = [[UILabel alloc] init];
    [self.search_lbl setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.search_lbl setBackgroundColor:[UIColor clearColor]];
    [self.search_lbl setText:JSLocalizedString(@"External Search", @"")];
    [self.search_lbl setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [self.search_lbl setTextColor:[ColorTheme textGrey]];
    [self.search_lbl setNumberOfLines:0];
    [self.search_lbl setLineBreakMode:NSLineBreakByWordWrapping];
    [self.view addSubview:self.search_lbl];
    [self.search_lbl layoutIfNeeded];
    
    [dict setObject:self.logo forKey:@"logo"];
    [dict setObject:self.category_btn forKey:@"cat_btn"];
    [dict setObject:self.category_lbl forKey:@"cat_lbl"];
    [dict setObject:self.myBooks_btn forKey:@"book_btn"];
    [dict setObject:self.myBooks_lbl forKey:@"book_lbl"];
    [dict setObject:self.portal_btn forKey:@"port_btn"];
    [dict setObject:self.portal_lbl forKey:@"port_lbl"];
    [dict setObject:self.settings_btn forKey:@"sett_btn"];
    [dict setObject:self.settings_lbl forKey:@"sett_lbl"];
    [dict setObject:self.search_btn forKey:@"sear_btn"];
    [dict setObject:self.search_lbl forKey:@"sear_lbl"];
    [dict setObject:self.ta3mim_btn forKey:@"ta_btn"];
    [dict setObject:self.ta3mim_lbl forKey:@"ta_lbl"];
    [dict setObject:self.contact_btn forKey:@"cont_btn"];
    [dict setObject:self.contact_lbl forKey:@"cont_lbl"];
    [dict setObject:self.faq_btn forKey:@"faq_btn"];
    [dict setObject:self.faq_lbl forKey:@"faq_lbl"];
    
    if ([self isPad])
    {
        [dict setObject:[NSNumber numberWithInt:ROUND_BUTTON_WIDTH_HEIGHT_iPad] forKey:@"metric_round"];

        [dict setObject:[NSNumber numberWithInt:logo_img.size.width - (logo_img.size.width*Logo_Scale_iPad)] forKey:@"wlogo"];
        [dict setObject:[NSNumber numberWithInt:logo_img.size.height - (logo_img.size.height*Logo_Scale_iPad)] forKey:@"hlogo"];
    }
    else
    {
        [dict setObject:[NSNumber numberWithInt:ROUND_BUTTON_WIDTH_HEIGHT_iPhone] forKey:@"metric_round"];

        [dict setObject:[NSNumber numberWithInt:logo_img.size.width - (logo_img.size.width*Logo_Scale_iPhone)] forKey:@"wlogo"];
        [dict setObject:[NSNumber numberWithInt:logo_img.size.height - (logo_img.size.height*Logo_Scale_iPhone)] forKey:@"hlogo"];
    }
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[logo(wlogo)]" options:0 metrics:dict views:dict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[logo(hlogo)]" options:0 metrics:dict views:dict]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.logo attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.logo attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.category_lbl attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];

    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[cat_btn(metric_round)]" options:0 metrics:dict views:dict]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.category_btn attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];

    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[ta_btn(metric_round)]" options:0 metrics:dict views:dict]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.ta3mim_btn attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.5 constant:0.0]];
    
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.ta3mim_lbl attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.ta3mim_btn attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];

    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[book_btn(metric_round)]" options:0 metrics:dict views:dict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[book_btn(metric_round)]" options:0 metrics:dict views:dict]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.myBooks_btn attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.myBooks_btn attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.75 constant:0.0]];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.myBooks_lbl attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.myBooks_btn attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[book_btn]-5-[book_lbl]" options:0 metrics:nil views:dict]];

    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[port_btn(metric_round)]" options:0 metrics:dict views:dict]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.portal_btn attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.5 constant:0.0]];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.portal_lbl attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.portal_btn attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];

    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[sett_btn(metric_round)]" options:0 metrics:dict views:dict]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.settings_btn attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.settings_lbl attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.settings_btn attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];

    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[cont_btn(metric_round)]" options:0 metrics:dict views:dict]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.contact_btn attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:0.5 constant:0.0]];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.contact_lbl attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.contact_btn attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];

    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[faq_btn(metric_round)]" options:0 metrics:dict views:dict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[faq_btn(metric_round)]" options:0 metrics:dict views:dict]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.faq_btn attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.faq_btn attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:0.25 constant:0.0]];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.faq_lbl attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.faq_btn attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[faq_btn]-5-[faq_lbl]" options:0 metrics:nil views:dict]];

    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[sear_btn(metric_round)]" options:0 metrics:dict views:dict]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.search_btn attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterX multiplier:0.5 constant:0.0]];

    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:self.search_lbl attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:self.search_btn attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
}


#pragma mark - Title View
#pragma mark -

/**
 Create label for custom navigation bar title
 */
-(void)setUpTitleView
{
    self.titleView = [[UILabel alloc] init];
    self.titleView.frame = CGRectMake(0, 0, 0, 44);
    [self.titleView setTextAlignment:NSTextAlignmentCenter];
    [self.titleView setTextColor:[UIColor whiteColor]];
    [self.titleView setBackgroundColor:[UIColor clearColor]];
    [self.titleView setText:JSLocalizedString(@"Home", @"")];
    [self.titleView setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]+4]];
    [self.navigationItem setTitleView:self.titleView];
}

#pragma mark - Navigate to Controller
#pragma mark -

/**
 Function to handle the event when user clicks on a specific menu item/button
 */
-(IBAction)goToView:(id)sender
{
    UIViewController *controller;
    
    switch ([sender tag]) {
        case 1: //categories
        {
            LibraryList *list = [[LibraryList alloc] init];
            [list setNodeID:@"25280"];
            [list setDocumentType:@"15"];
            [list setIsSearching:NO];
            [list setIsFavorite:NO];
            [list setNavigationTitle:JSLocalizedString(@"Legislations", @"")];
            controller = list;
        }
            break;
        case 2: //ta3amim
        {
            LibraryList *list = [[LibraryList alloc] init];
            [list setNodeID:@"25281"];
            [list setDocumentType:@"16"];
            [list setIsSearching:NO];
            [list setIsFavorite:NO];
            [list setNavigationTitle:JSLocalizedString(@"Circulars", @"")];
            controller = list;
        }
            break;
        case 3: //my books
        {
            LibraryList *list = [[LibraryList alloc] init];
            [list setNodeID:@""];
            [list setDocumentType:@"-1"];
            [list setIsSearching:NO];
            [list setIsFavorite:YES];
            [list setNavigationTitle:JSLocalizedString(@"My Books", @"")];
            controller = list;
        }
            break;
        case 4: //portal
        {
            NSString *urlAddress = @"http://85.112.95.35:8081/Default.aspx?view=2";

            TOWebViewController *webView = [[TOWebViewController alloc] initWithURLString:urlAddress];
            [webView.webView setFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height)];
            [webView setLoadingBarTintColor:[ColorTheme slawRed]];
            [webView setButtonTintColor:[ColorTheme slawLightRed]];
            [webView setShowDoneButton:YES];
            [webView setShowLoadingBar:YES];
            [webView setShowPageTitles:YES];
            [webView setHideWebViewBoundaries:YES];
            [webView setShowActionButton:YES];
            [webView setShowUrlWhileLoading:NO];
            [webView.webView setClipsToBounds:YES];
            [webView.navigationController.navigationBar setTintColor:[ColorTheme slawRed]];

            // add custom back button to navigation bar
            UIImage *dismiss_image= [UIImage imageNamed:@"Left-Arrow"];
            UIButton *dismiss_btn = [UIButton buttonWithType:UIButtonTypeCustom];
            dismiss_btn.bounds = CGRectMake( 10, 0, dismiss_image.size.width, dismiss_image.size.height);
            [dismiss_btn addTarget:self action:@selector(dismissPage) forControlEvents:UIControlEventTouchUpInside];
            [dismiss_btn setImage:dismiss_image forState:UIControlStateNormal];
            
            controller = webView;
//            controller = [[PortalPage alloc] init];
        }   break;
        case 5: //settings
            controller = [[SettingsPage alloc] init];
            break;
        case 6: //contact us
            controller = [[ContactPage alloc] init];
            break;
        case 7: //faq
            controller = [[FAQPage alloc] init];
            break;
        case 8: //search
        {
            LibraryList *list = [[LibraryList alloc] init];
            [list setIsSearching:YES];
            [list setNodeID:@""];
            [list setDocumentType:@"0"];
            [list setIsFavorite:NO];
            controller = list;
        }
            break;
            
        default:
            break;
    }
    
    [self.navigationController pushViewController:controller animated:YES];
    
}

/**
 Function called to go back to the previous/parent view controller
 */
-(void)dismissPage
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Status Bar
#pragma mark -

/**
 Make sure status bar is not hidden

 @return NO
 */
- (BOOL)prefersStatusBarHidden
{
    return NO;
}

-(void)showStatusBar {
    if ([self respondsToSelector:@selector(setNeedsStatusBarAppearanceUpdate)]) {
        [self prefersStatusBarHidden];
        [self performSelector:@selector(setNeedsStatusBarAppearanceUpdate)];
    }
    
    [[UIApplication sharedApplication] setStatusBarHidden:NO withAnimation:UIStatusBarAnimationFade];
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
}


#pragma mark - Interface Orientation
#pragma mark -


/**
 Allow only portrait device orientation

 @return UIInterfaceOrientationMaskPortrait
 */
-(UIInterfaceOrientationMask)supportedInterfaceOrientations
{
    return UIInterfaceOrientationMaskPortrait;
}

/**
 Make sure the device interface cannot rotate

 @return NO
 */
- (BOOL)shouldAutorotate
{
    return NO;
}

/**
 Make sure the device interface cannot rotate

 @return NO
 */
- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return NO;
}


/**
 Check if the device is an iPad or iPhone

 */
-(BOOL)isPad {
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return YES;
    }else {
        return NO;
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
