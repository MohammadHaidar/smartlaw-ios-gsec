//
//  FAQPage.h
//  SmartLaw
//
//  Created by Krystel Chaccour on 9/27/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KMAccordionTableViewController.h"

@interface FAQPage : KMAccordionTableViewController
@property (nonatomic, retain) UIButton *dismiss_btn;
@property (nonatomic, retain) UILabel *titleView;

@end
