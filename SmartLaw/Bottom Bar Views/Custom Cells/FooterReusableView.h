//
//  FooterReusableView.h
//  SmartLaw
//
//  Created by Krystel Chaccour on 4/8/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FooterReusableView : UICollectionReusableView
@property (nonatomic, retain) RTSpinKitView *spinner;

@end
