//
//  UIFont+DBZAdditions.m
//  DubizzleHorizontal
//
//  Created by Robert Dimitrov on 4/29/14.
//  Copyright (c) 2014 Dubizzle. All rights reserved.
//

#import "UIFont+DBZAdditions.h"

@implementation UIFont (DBZAdditions)

+ (void)dbz_printAllFonts {
    // print all available fonts
    NSArray *familyNames = [[NSArray alloc] initWithArray:[UIFont familyNames]];
    NSArray *fontNames;
    NSInteger indFamily, indFont;
    for (indFamily=0; indFamily<[familyNames count]; ++indFamily)
    {
        //NSLog(@"Family name: %@", [familyNames HGObjectAtIndex:indFamily]);
        fontNames = [[NSArray alloc] initWithArray:
                     [UIFont fontNamesForFamilyName:
                      [familyNames HGObjectAtIndex:indFamily]]];
        for (indFont=0; indFont<[fontNames count]; ++indFont)
        {
            //NSLog(@"    Font name: %@", [fontNames HGObjectAtIndex:indFont]);
        }
    }
}

#pragma mark - Proxima Nova font

+ (UIFont *)dbz_proximaNovaBoldWithSize:(CGFloat)size {
    if ([Globals isDeviceLanguageRTL]) {
        return [UIFont fontWithName:@"ProximaNova-Bold" size:size];
    }

    return [UIFont fontWithName:@"ProximaNova-Bold" size:size];
}

+ (UIFont *)dbz_proximaNovaLightWithSize:(CGFloat)size {
    if ([Globals isDeviceLanguageRTL]) {
        return [UIFont fontWithName:@"ProximaNova-Light" size:size];
    }

    return [UIFont fontWithName:@"ProximaNova-Light" size:size];
}

+ (UIFont *)dbz_proximaNovaRegularItalicWithSize:(CGFloat)size {
    if ([Globals isDeviceLanguageRTL]) {
        return [UIFont fontWithName:@"ProximaNova-RegularIt" size:size];
    }

    return [UIFont fontWithName:@"ProximaNova-RegularIt" size:size];
}

+ (UIFont *)dbz_proximaNovaRegularWithSize:(CGFloat)size {
    if ([Globals isDeviceLanguageRTL]) {
        return [UIFont fontWithName:@"ProximaNova-Regular" size:size];
    }


    return [UIFont fontWithName:@"ProximaNova-Regular" size:size];
}

+ (UIFont *)dbz_proximaNovaSemiboldWithSize:(CGFloat)size {
    if ([Globals isDeviceLanguageRTL]) {
        return [UIFont fontWithName:@"ProximaNova-Semibold" size:size];
    }

    return [UIFont fontWithName:@"ProximaNova-Semibold" size:size];
}

+ (UIFont *)dbz_proximaNovaThinItalicWithSize:(CGFloat)size {
    if ([Globals isDeviceLanguageRTL]) {
        return [UIFont fontWithName:@"ProximaNova-ThinIt" size:size];
    }


    return [UIFont fontWithName:@"ProximaNova-ThinIt" size:size];
}

+ (UIFont *)dbz_proximaNovaThinWithSize:(CGFloat)size {
    if ([Globals isDeviceLanguageRTL]) {
        return [UIFont fontWithName:@"ProximaNova-Thin" size:size];
    }


    return [UIFont fontWithName:@"ProximaNova-Thin" size:size];
}

#pragma mark - Ubuntu font

+ (UIFont *)dbz_ubuntuBoldWithSize:(CGFloat)size {
    if ([Globals isDeviceLanguageRTL]) {
        return [UIFont fontWithName:@"ProximaNova-Bold" size:size];
    }

    return [UIFont fontWithName:@"Ubuntu-Bold" size:size];
}

+ (UIFont *)dbz_ubuntuBoldItalicWithSize:(CGFloat)size {
    if ([Globals isDeviceLanguageRTL]) {
        return [UIFont fontWithName:@"ProximaNova-BoldItalic" size:size];
    }

    return [UIFont fontWithName:@"Ubuntu-BoldItalic" size:size];
}

+ (UIFont *)dbz_ubuntuItalicWithSize:(CGFloat)size {
    if ([Globals isDeviceLanguageRTL]) {
        return [UIFont fontWithName:@"ProximaNova-Italic" size:size];
    }

    return [UIFont fontWithName:@"Ubuntu-Italic" size:size];
}

+ (UIFont *)dbz_ubuntuLightWithSize:(CGFloat)size {
    if ([Globals isDeviceLanguageRTL]) {
        return [UIFont fontWithName:@"ProximaNova-Light" size:size];
    }


    return [UIFont fontWithName:@"Ubuntu-Light" size:size];
}

+ (UIFont *)dbz_ubuntuLightItalicWithSize:(CGFloat)size {
    if ([Globals isDeviceLanguageRTL]) {
        return [UIFont fontWithName:@"ProximaNova-LightItalic" size:size];
    }

    return [UIFont fontWithName:@"Ubuntu-LightItalic" size:size];
}

+ (UIFont *)dbz_ubuntuMediumWithSize:(CGFloat)size {
    if ([Globals isDeviceLanguageRTL]) {
        return [UIFont fontWithName:@"ProximaNova-Medium" size:size];
    }

    return [UIFont fontWithName:@"Ubuntu-Medium" size:size];
}

+ (UIFont *)dbz_ubuntuMediumItalicWithSize:(CGFloat)size {
    if ([Globals isDeviceLanguageRTL]) {
        return [UIFont fontWithName:@"ProximaNova-MediumItalic" size:size];
    }

    return [UIFont fontWithName:@"Ubuntu-MediumItalic" size:size];
}

+ (UIFont *)dbz_ubuntuRegularWithSize:(CGFloat)size {
    if ([Globals isDeviceLanguageRTL]) {
        return [UIFont fontWithName:@"ProximaNova-Light" size:size];
    }

    return [UIFont fontWithName:@"Ubuntu" size:size];
}

@end
