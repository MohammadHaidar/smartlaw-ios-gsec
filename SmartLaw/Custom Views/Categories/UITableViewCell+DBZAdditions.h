//
//  UITableViewCell+DBZAdditions.h
//  DubizzleHorizontal
//
//  Created by Robert Dimitrov on 5/9/14.
//  Copyright (c) 2014 Dubizzle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UITableViewCell (DBZAdditions)

- (void)dbz_setSelectedBackgroundColor:(UIColor *)color;

@end
