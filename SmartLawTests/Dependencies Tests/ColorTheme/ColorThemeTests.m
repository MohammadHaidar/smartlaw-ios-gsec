//
//  ColorThemeTests.m
//  SmartLaw
//
//  Created by Krystel Chaccour on 6/15/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "ColorTheme.h"

@interface ColorThemeTests : XCTestCase
@end

@implementation ColorThemeTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testBackgroundGrey {
    
    XCTAssertEqualObjects([ColorTheme backgroundGrey], [ColorTheme colorFromHexString:@"#d0d2d3"], @"Background grey != #d0d2d3");
}

- (void)testBackgroundBeige {
    
    XCTAssertEqualObjects([ColorTheme backgroundBeige], [ColorTheme colorFromHexString:@"#f1e8d9"], @"Background beige != #f1e8d9");
}

- (void)testSlawRed {
    
    XCTAssertEqualObjects([ColorTheme slawRed], [ColorTheme colorFromHexString:@"#891e04"], @"Slaw red != #891e04");
}

- (void)testSlawLightRed {
    
    XCTAssertEqualObjects([ColorTheme slawLightRed], [ColorTheme colorFromHexString:@"#ee3e43"], @"Slaw light red != #ee3e43");
}

- (void)testPantoneGrey {
    
    XCTAssertEqualObjects([ColorTheme pantoneGrey], [ColorTheme colorFromHexString:@"#adafb1"], @"Pantone grey != #adafb1");
}

- (void)testTextGrey {
    
    XCTAssertEqualObjects([ColorTheme textGrey], [ColorTheme colorFromHexString:@"#4d4d4d"], @"Text grey != #4d4d4d");
}

- (void)testLightGrey {
    
    XCTAssertEqualObjects([ColorTheme lightGrey], [ColorTheme colorFromHexString:@"#d2d2d2"], @"Light grey != #d2d2d2");
}

- (void)testBarGrey {
    
    XCTAssertEqualObjects([ColorTheme barGrey], [ColorTheme colorFromHexString:@"#c5c5c5"], @"Bar grey != #c5c5c5");
}

- (void)testHighlightBlue {
    
    XCTAssertEqualObjects([ColorTheme highlightBlue], [ColorTheme colorFromHexString:@"#BDE7F6"], @"Highlight Blue != #BDE7F6");
}

- (void)testHighlightRed {
    
    XCTAssertEqualObjects([ColorTheme highlightRed], [ColorTheme colorFromHexString:@"#F33042"], @"Highlight red != #F33042");
}

- (void)testHighlightGreen {
    
    XCTAssertEqualObjects([ColorTheme highlightGreen], [ColorTheme colorFromHexString:@"#AFD6A3"], @"Highlight Green != #AFD6A3");
}

- (void)testHighlightYellow {
    
    XCTAssertEqualObjects([ColorTheme highlightYellow], [ColorTheme colorFromHexString:@"#FFE5CA"], @"Highlight Yellow != #FFE5CA");
}

- (void)testBarBlack {
    
    XCTAssertEqualObjects([ColorTheme barBlack], [ColorTheme colorFromHexString:@"#323232"], @"Bar black != #323232");
}

- (void)testCategGrey {
    
    XCTAssertEqualObjects([ColorTheme categGrey], [ColorTheme colorFromHexString:@"#6d6d6d"], @"Categ Grey != #6d6d6d");
}

@end
