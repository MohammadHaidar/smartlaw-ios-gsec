//
//  PortalPage.m
//  SmartLaw
//
//  Created by Krystel Chaccour on 9/27/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import "PortalPage.h"
#import <TOWebViewController/TOWebViewController.h>

@interface PortalPage () <UIWebViewDelegate>

@end

@implementation PortalPage
@synthesize titleView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]]];

    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    if ([self respondsToSelector:@selector(automaticallyAdjustsScrollViewInsets)])
        self.automaticallyAdjustsScrollViewInsets = NO;
    
    // set the navigation bar style (color, tint, etc)
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [ColorTheme barBlack];
    self.navigationController.navigationBar.barTintColor = [ColorTheme barBlack];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;

    // create label for custom navigation bar title
    titleView = [[UILabel alloc] init];
    titleView.frame = CGRectMake(0, 0, 0, 44);
    [titleView setTextAlignment:NSTextAlignmentCenter];
    [titleView setTextColor:[UIColor whiteColor]];
    [titleView setText:JSLocalizedString(@"PortalT", @"")];
    [titleView setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]+4]];
    [titleView setBackgroundColor:[UIColor clearColor]];
    [self.navigationItem setTitleView:titleView];
    
    [Appdelegate showLoadingIndicatorWithView:self.view andText:nil];

    [self setUpDismissButton];
    
//    [self setUpLabel];
    
//    [self performSelector:@selector(setUpInterface) withObject:nil afterDelay:1.0];
}

-(void)setUpInterface
{
    UIWebView *webView = [[UIWebView alloc] init];
    [webView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [webView setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]]];
    [webView setDelegate:self];
    [webView setScalesPageToFit:NO];
    NSString *urlAddress = @"https://abudhabilegislations.ecouncil.ae/Default.aspx?view=2";
    NSURL *url = [NSURL URLWithString:urlAddress];
    [webView loadRequest:[NSURLRequest requestWithURL:url]];
    [self.view addSubview:webView];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[webView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(webView)]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[webView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(webView)]];
}

-(void)setUpLabel
{
    UILabel *comingSoon = [[UILabel alloc] init];
    [comingSoon setTranslatesAutoresizingMaskIntoConstraints:NO];
    [comingSoon setText:JSLocalizedString(@"Coming Soon", @"")];
    [comingSoon setTextColor:[ColorTheme textGrey]];
    [comingSoon setNumberOfLines:0];
    [comingSoon setLineBreakMode: NSLineBreakByWordWrapping];
    [comingSoon setBackgroundColor:[UIColor clearColor]];
    [comingSoon setFont:[FontManager mediumFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]+4]];
    [comingSoon setTextAlignment:NSTextAlignmentCenter];
    [self.view addSubview:comingSoon];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[comingSoon]-20-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(comingSoon)]];
    [self.view addConstraint:[NSLayoutConstraint constraintWithItem:comingSoon attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.view attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
}

-(void)setUpDismissButton
{
    // add custom back button to navigation bar
    UIImage *dismiss_image= [UIImage imageNamed:@"Left-Arrow"];
    self.dismiss_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.dismiss_btn.bounds = CGRectMake( 10, 0, dismiss_image.size.width, dismiss_image.size.height);
    [self.dismiss_btn addTarget:self action:@selector(dismissPage) forControlEvents:UIControlEventTouchUpInside];
    [self.dismiss_btn setImage:dismiss_image forState:UIControlStateNormal];
    
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:self.dismiss_btn];
    self.navigationItem.leftBarButtonItem = leftButton;
    
}

#pragma mark - UIWebview delegates
#pragma mark -

-(void)webViewDidFinishLoad:(UIWebView *)webView
{
    [Appdelegate hideLoadingIndicator];
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

-(void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    [Appdelegate hideLoadingIndicator];
}


- (void)webViewDidStartLoad:(UIWebView *)webView {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}


// Function called to go back to the previous/parent view controller
-(void)dismissPage
{
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
