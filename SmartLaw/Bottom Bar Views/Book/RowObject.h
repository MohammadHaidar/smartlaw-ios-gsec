//
//  RowObject.h
//  SmartLaw
//
//  Created by Krystel Chaccour on 11/25/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RowObject : NSObject

@property (nonatomic, retain) NSString *file_name;
@property (nonatomic, retain) NSString *file_id;
@property (nonatomic, retain) NSString *master_id;
@property (nonatomic, retain) NSString *file_parentId;
@property (nonatomic, assign) NSInteger nodeType;
@property (nonatomic, assign) BOOL selectable;
@property (nonatomic, assign) NSInteger childNodesCount;
@property (nonatomic, retain) NSArray *childNodes;
@property (nonatomic, retain) NSString *htmlLink;

@property (nonatomic, assign) NSInteger depth;
@property (nonatomic, assign) BOOL expand; // if the node is expanded
@property (nonatomic, assign) BOOL isChild; // if the node is a child
@property (nonatomic, assign) BOOL isParent; // if the node is parent


// Class methods
/*
 ** A class method to returns a Task created with the given parameters.
 */
+(instancetype)fileWithName:(NSString *)fileName fileId:(NSString *)fileId masterId:(NSString *)masterId parentId:(NSString *)parentId nodeType:(NSInteger)nodeType selectable:(BOOL)selectable childNodesCount:(NSInteger)childNodesCount childNodes:(NSArray *)childNodes htmlLink:(NSString *)htmlLink expand:(BOOL)expand isChild:(BOOL)isChild isParent:(BOOL)isParent depth:(NSInteger)depth;

+ (instancetype)law;

// Instance methods
/*
 ** Returns a Task object initialized with the given parameters.
 */
-(instancetype)initWithFileWithName:(NSString *)fileName fileId:(NSString *)fileId masterId:(NSString *)masterId parentId:(NSString *)parentId nodeType:(NSInteger)nodeType selectable:(BOOL)selectable childNodesCount:(NSInteger)childNodesCount childNodes:(NSArray *)childNodes htmlLink:(NSString *)htmlLink expand:(BOOL)expand isChild:(BOOL)isChild isParent:(BOOL)isParent depth:(NSInteger)depth;

@end
