//
//  NSString+DecimalNumbers.h
//  DubizzleHorizontal
//
//  Created by Nikolay Yanev on 8/27/14.
//  Copyright (c) 2014 dubizzle. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (DecimalNumbers)

- (NSString *)dbz_replaceArabicNumbers;

@end
