//
//  LibraryList.h
//  SmartLaw
//
//  Created by Krystel Chaccour on 3/1/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LibraryList : UIViewController
@property (nonatomic, assign) BOOL isSearching;
@property (nonatomic, assign) BOOL isFavorite;

@property (nonatomic, retain) UIButton *dismiss_btn;
@property (nonatomic, retain) UITableView *list_table;
@property (nonatomic, retain) UIView *footerView;
@property (nonatomic, retain) UIButton *searchButton;
@property (nonatomic, retain) UIBarButtonItem *searchItem;
@property (nonatomic, retain) UISearchBar *searchBar;
@property (nonatomic, retain) UILabel *titleView;
@property (nonatomic, retain) UILabel *empty_label;
@property (nonatomic, retain) NSString *nodeID, *documentType;
@property (nonatomic, retain) NSString *navigationTitle;

-(void)setUpDismissButton;
-(void)setUpTitleView;
-(void)setUpSearchButton;
-(void)loadTable;
-(void)emptyWithType:(int)nsType;

@end
