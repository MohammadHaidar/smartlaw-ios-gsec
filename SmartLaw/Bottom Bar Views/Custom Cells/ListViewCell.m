//
//  ListViewCell.m
//  SmartLaw
//
//  Created by Krystel Chaccour on 3/3/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import "ListViewCell.h"

@interface ListViewCell()
{
    NSMutableDictionary *dict;
}

@end

@implementation ListViewCell

- (void)awakeFromNib {
    // Initialization code
    [super awakeFromNib];
}

-(instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    
    if (self)
    {
        [self.contentView.layer setCornerRadius:7.0];

        self.fakeView = [[UIButton alloc] init];
        [self.fakeView setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.fakeView setBackgroundColor:[UIColor clearColor]];
        self.fakeView.layer.masksToBounds = YES;
        [self.fakeView setUserInteractionEnabled:YES];
        [self.contentView bringSubviewToFront:self.fakeView];
        [self.contentView addSubview:self.fakeView];
        
        self.book_title = [[UILabel alloc] init];
        [self.book_title setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.book_title setTextColor:[ColorTheme textGrey]];
        [self.book_title setTextAlignment:NSTextAlignmentRight];
        [self.book_title setFont:[FontManager mediumFontOfSize:14]];
        [self.book_title setNumberOfLines:0];
        [self.book_title setLineBreakMode:NSLineBreakByWordWrapping];
        [self.contentView addSubview:self.book_title];
    
        self.rowType = [[UIImageView alloc] init];
        [self.rowType setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.rowType setBackgroundColor:[UIColor clearColor]];
        [self.rowType setContentMode:UIViewContentModeScaleAspectFit];
        [self.contentView addSubview:self.rowType];
        
        self.accessoryBtn = [[FRDLivelyButton alloc] initWithFrame:CGRectMake(7, self.contentView.frame.size.height/2 - 13, 26, 26)];
        [self.accessoryBtn setStyle:kFRDLivelyButtonStyleCaretDown animated:NO];
        [self.accessoryBtn setBackgroundColor:[UIColor clearColor]];
        [self.accessoryBtn setUserInteractionEnabled:YES];
        [self.accessoryBtn setHidden:YES];
        [self.fakeView addSubview:self.accessoryBtn];

        dict = [NSMutableDictionary dictionary];
        [dict setObject:self.fakeView forKey:@"fakeV"];
        [dict setObject:self.book_title forKey:@"bookTitle"];
        [dict setObject:self.rowType forKey:@"rowimg"];
        
//        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"en"])
//        {
//            [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[fakeV(40)]-0-|" options:0 metrics:nil views:dict]];
//            [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[fakeV]-0-|" options:0 metrics:nil views:dict]];
//            
//            [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[rowimg(20)]" options:0 metrics:nil views:dict]];
//            
//            [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[rowimg]-10-[bookTitle]-10-[fakeV]" options:0 metrics:nil views:dict]];
//            
//        }
//        else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"ar"])
//        {
            [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[fakeV(40)]" options:0 metrics:nil views:dict]];
            [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[fakeV]-0-|" options:0 metrics:nil views:dict]];
            
            [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[rowimg(20)]-10-|" options:0 metrics:nil views:dict]];
            
            [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[fakeV]-10-[bookTitle]-10-[rowimg]" options:0 metrics:nil views:dict]];
//        }
        
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[rowimg(20)]" options:0 metrics:nil views:dict]];
        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.rowType attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];

        [self.contentView addConstraint:[NSLayoutConstraint constraintWithItem:self.book_title attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.contentView attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    }
    
    return self;
}


//-(void)viewDidLayoutSubviews
//{
//    [self layoutSubviews];
//}

- (void)layoutSubviews
{
    [super layoutSubviews];
    
    [self.contentView layoutIfNeeded];
    [self.fakeView layoutIfNeeded];

    float indentPoints = self.indentationLevel * self.indentationWidth;
    
//    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"en"])
//    {
//        // move contentview frame according to indentation
//        self.contentView.frame = CGRectMake(indentPoints,
//                                            self.contentView.frame.origin.y,
//                                            self.contentView.frame.size.width - indentPoints,
//                                            self.contentView.frame.size.height);
//        
//   
//        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[fakeV(40)]-0-|" options:0 metrics:nil views:dict]];
//    }
//    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"ar"])
//    {
        if (indentPoints > 0)
        {
            self.contentView.frame = CGRectMake(0,
                                                self.contentView.frame.origin.y,
                                                self.contentView.frame.size.width - indentPoints,
                                                self.contentView.frame.size.height);

        }
        // move contentview frame according to indentation
       
        [self.contentView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[fakeV(40)]" options:0 metrics:nil views:dict]];

//    }
    
    [self.fakeView layoutIfNeeded];
    [self.accessoryBtn setFrame:CGRectMake(7, self.contentView.frame.size.height/2 - 13, 26, 26)];

}


- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
    
    // Configure the view for the selected state
}

@end
