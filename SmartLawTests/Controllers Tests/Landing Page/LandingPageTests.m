//
//  LandingPageTests.m
//  SmartLaw
//
//  Created by Krystel Chaccour on 6/15/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "LandingPage.h"

@interface LandingPageTests : XCTestCase
@property LandingPage *landindPage;
@end

@implementation LandingPageTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.

    self.landindPage = [[LandingPage alloc] init];
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    
    self.landindPage = nil;
    
    [super tearDown];
}

- (void)testLanguageIsSet {
    
    XCTAssertNotNil([[NSUserDefaults standardUserDefaults] objectForKey:@"language"], @"Language should not be nil");
}

- (void)testClassInitialization {
    
    XCTAssertNotNil(self.landindPage, @"Landing Page should not be nil");
}

- (void)testNavigationInitialization {
    
    [self.landindPage loadBottomBar];
    
    XCTAssertNotNil(self.landindPage.nc1, @"Navigation 1 should not be nil");
    XCTAssertNotNil(self.landindPage.nc2, @"Navigation 2 should not be nil");
    XCTAssertNotNil(self.landindPage.nc3, @"Navigation 3 should not be nil");
}

- (void)testBottomBarInitialized {
    
    [self.landindPage loadBottomBar];
    
    XCTAssertNotNil(self.landindPage.es_tabBar, @"Tab Bar controller should not be nil");
}


@end
