//
//  NSDate+DBZAdditions.h
//  DubizzleHorizontal
//
//  Created by Robert Dimitrov on 6/4/14.
//  Copyright (c) 2014 Dubizzle. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (DBZAdditions)

- (NSDate *)dbz_dateAtBeginningOfDayForDate:(NSDate *)inputDate;
- (NSDate *)dbz_dateByAddingYears:(NSInteger)numberOfYears toDate:(NSDate *)inputDate;
+ (NSInteger)dbz_daysBetweenDate:(NSDate*)fromDateTime andDate:(NSDate*)toDateTime;

@end
