//
//  ContactPage.h
//  SmartLaw
//
//  Created by Krystel Chaccour on 9/27/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ContactPage : UIViewController
@property (nonatomic, retain) UIView *background_view;
@property (nonatomic, retain) UILabel *titleView;
@property (nonatomic, retain) UIButton *dismiss_btn;

@end
