//
//  NSString+DecimalNumbers.m
//  DubizzleHorizontal
//
//  Created by Nikolay Yanev on 8/27/14.
//  Copyright (c) 2014 dubizzle. All rights reserved.
//

#import "NSString+DecimalNumbers.h"

@implementation NSString (DecimalNumbers)

- (NSString *)dbz_replaceArabicNumbers {
    NSMutableString *string = [self mutableCopy];

    NSArray *arabicNumbers = @[@"٠", @"١", @"٢", @"٣", @"٤", @"٥", @"٦", @"٧", @"٨", @"٩"];
    NSArray *decimalNumbers = @[@"0", @"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9"];

    for (int i = 0; i < arabicNumbers.count; i++) {
        [string replaceOccurrencesOfString:arabicNumbers[i] withString:decimalNumbers[i] options:NSCaseInsensitiveSearch range:NSMakeRange(0, string.length)];
    }

    return string;
}

@end
