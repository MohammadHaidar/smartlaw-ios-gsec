//
//  SettingsPage.m
//  SmartLaw
//
//  Created by Krystel Chaccour on 3/1/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import "SettingsPage.h"
#import <KSToastView/KSToastView.h>

@interface SettingsPage () <UIScrollViewDelegate>
{
    SIAlertView *alertView;
    SIAlertView *p_alertView;
    NSDateFormatter *dateFormatter;
}
@end

@implementation SettingsPage
@synthesize  select_lang, titleView, background_view, language_segment, select_mode, mode_segment, update_button, update_label;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]]];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"downloadProgress" object:nil];
    // call function to update download progress
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateProgress:) name:@"downloadProgress" object:nil];

    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"startProgress" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(startProgress) name:@"startProgress" object:nil];
    
    [[NSNotificationCenter defaultCenter] removeObserver:self name:@"updateToast" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateToast) name:@"updateToast" object:nil];


    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    if ([self respondsToSelector:@selector(automaticallyAdjustsScrollViewInsets)])
        self.automaticallyAdjustsScrollViewInsets = NO;
    
    // set the navigation bar style (color, tint, etc)
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [ColorTheme barBlack];
    self.navigationController.navigationBar.barTintColor = [ColorTheme barBlack];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;
    
    // check and set the language used for localization
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"ar"])
        JSSetLanguage(@"ar");
    else
        JSSetLanguage(@"en");
    
    // create label for custom navigation bar title
    titleView = [[UILabel alloc] init];
    titleView.frame = CGRectMake(0, 0, 0, 44);
    [titleView setTextAlignment:NSTextAlignmentCenter];
    [titleView setTextColor:[UIColor whiteColor]];
    [titleView setText:JSLocalizedString(@"Settings", @"")];
    [titleView setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]+4]];
    [titleView setBackgroundColor:[UIColor clearColor]];
    
    [self.navigationItem setTitleView:titleView];

    [self setUpDismissButton];

    [self loadInterface];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewWillAppear:animated];
}

-(void)setUpDismissButton
{
    // add custom back button to navigation bar
    UIImage *dismiss_image= [UIImage imageNamed:@"Left-Arrow"];
    self.dismiss_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.dismiss_btn.bounds = CGRectMake( 10, 0, dismiss_image.size.width, dismiss_image.size.height);
    [self.dismiss_btn addTarget:self action:@selector(dismissPage) forControlEvents:UIControlEventTouchUpInside];
    [self.dismiss_btn setImage:dismiss_image forState:UIControlStateNormal];
    
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:self.dismiss_btn];
    self.navigationItem.leftBarButtonItem = leftButton;
    
}

/**
 Function called to go back to the previous/parent view controller
 */
-(void)dismissPage
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Create UI
#pragma mark

/**
 Function to load/create the settings UI
 */
-(void)loadInterface
{
    [background_view removeFromSuperview];
    background_view = [[UIView alloc] init];
    [background_view setTranslatesAutoresizingMaskIntoConstraints:NO];
    [background_view setBackgroundColor:[UIColor whiteColor]];
    [background_view.layer setCornerRadius:7.0];
    [background_view setUserInteractionEnabled:YES];
    [self.view addSubview:background_view];
    [background_view layoutIfNeeded];
    [background_view setNeedsLayout];
    
    [select_lang removeFromSuperview];
    select_lang = [[UILabel alloc] init];
    [select_lang setTranslatesAutoresizingMaskIntoConstraints:NO];
    [select_lang setTextColor:[ColorTheme textGrey]];
    [select_lang setBackgroundColor:[UIColor clearColor]];
    [select_lang sizeToFit];
    [select_lang setClipsToBounds:YES];
    [background_view addSubview:select_lang];
    [select_lang layoutIfNeeded];
    [select_lang setNeedsLayout];
    
    [language_segment removeFromSuperview];
    language_segment = [[UISegmentedControl alloc] initWithItems:@[JSLocalizedString(@"English", @""), JSLocalizedString(@"Arabic", @"")]];
    [language_segment setTranslatesAutoresizingMaskIntoConstraints:NO];
    [language_segment setTintColor:[ColorTheme slawLightRed]];
    UIFont *font = [FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]+1];
    UIColor *grey_color = [ColorTheme textGrey];
    UIColor *white_color = [UIColor whiteColor];
    NSDictionary *attributes_normal = [NSDictionary dictionaryWithObjects:@[font, grey_color] forKeys:@[NSFontAttributeName, NSForegroundColorAttributeName]];
    NSDictionary *attributes_selected = [NSDictionary dictionaryWithObjects:@[font, white_color] forKeys:@[NSFontAttributeName, NSForegroundColorAttributeName]];
    
    [language_segment setTitleTextAttributes:attributes_normal forState:UIControlStateNormal];
    [language_segment setTitleTextAttributes:attributes_selected forState:UIControlStateSelected];
    [language_segment setTag:10];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"language"] isEqualToString:@"en"])
        [language_segment setSelectedSegmentIndex:0];
    else
        [language_segment setSelectedSegmentIndex:1];
    [language_segment addTarget:self action:@selector(segmentUpdated:) forControlEvents:UIControlEventValueChanged];
    [background_view addSubview:language_segment];
    
    [select_mode removeFromSuperview];
    select_mode = [[UILabel alloc] init];
    [select_mode setTranslatesAutoresizingMaskIntoConstraints:NO];
    [select_mode setTextColor:[ColorTheme textGrey]];
    [select_mode setBackgroundColor:[UIColor clearColor]];
    [select_mode sizeToFit];
    [select_mode setClipsToBounds:YES];
    [background_view addSubview:select_mode];
    [select_mode layoutIfNeeded];
    [select_mode setNeedsLayout];
    
    [mode_segment removeFromSuperview];
    mode_segment = [[UISegmentedControl alloc] initWithItems:@[JSLocalizedString(@"Online", @""), JSLocalizedString(@"Offline", @"")]];
    [mode_segment setTranslatesAutoresizingMaskIntoConstraints:NO];
    [mode_segment setTintColor:[ColorTheme slawLightRed]];
    [mode_segment setTitleTextAttributes:attributes_normal forState:UIControlStateNormal];
    [mode_segment setTitleTextAttributes:attributes_selected forState:UIControlStateSelected];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"mode"] isEqualToString:@"on"])
        [mode_segment setSelectedSegmentIndex:0];
    else if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"mode"] isEqualToString:@"off"])
        [mode_segment setSelectedSegmentIndex:1];
    else
        [mode_segment setSelectedSegmentIndex:0];
    [mode_segment setTag:20];
    [mode_segment addTarget:self action:@selector(segmentUpdated:) forControlEvents:UIControlEventValueChanged];
    [background_view addSubview:mode_segment];

    dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"dd MMMM yyyy - hh:mm a"];

    update_label = [[UILabel alloc] init];
    [update_label setTranslatesAutoresizingMaskIntoConstraints:NO];
    [update_label setTextColor:[ColorTheme textGrey]];
    [update_label setBackgroundColor:[UIColor clearColor]];
    [update_label setText:[NSString stringWithFormat:JSLocalizedString(@"Last Updated", @""), [dateFormatter stringFromDate:[Appdelegate deserializeJsonDateString:[[NSUserDefaults standardUserDefaults] objectForKey:@"LastUpdated"]]]]];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"mode"] isEqualToString:@"off"])
        [update_label setHidden:NO];
    else
        [update_label setHidden:YES];
    [update_label setNumberOfLines:0];
    [update_label setLineBreakMode:NSLineBreakByWordWrapping];
    [update_label sizeToFit];
    [update_label setClipsToBounds:YES];
    [background_view addSubview:update_label];
    [update_label layoutIfNeeded];
    [update_label setNeedsLayout];

    update_button = [UIButton buttonWithType:UIButtonTypeCustom];
    [update_button setTranslatesAutoresizingMaskIntoConstraints:NO];
    [update_button setTitle:JSLocalizedString(@"Check for updates", @"") forState:UIControlStateNormal];
    [update_button.titleLabel setFont:[FontManager mediumFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]+1]];
    [update_button setTitleColor:[ColorTheme slawRed] forState:UIControlStateNormal];
    [update_button setTitleColor:[ColorTheme textGrey] forState:UIControlStateSelected | UIControlStateHighlighted];
    [update_button setUserInteractionEnabled:YES];
    [update_button setEnabled:YES];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"mode"] isEqualToString:@"off"])
        [update_button setHidden:NO];
    else
        [update_button setHidden:YES];
    [update_button addTarget:self action:@selector(clicked:) forControlEvents:UIControlEventTouchUpInside];
    [background_view addSubview:update_button];
    [update_button layoutIfNeeded];
    [update_button setNeedsLayout];
    
    [select_lang setTextAlignment:[JSLocalizedString(@"Align", @"") intValue]];
    [select_lang setText:JSLocalizedString(@"Select Language", @"")];
    [select_lang setFont:[FontManager mediumFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]+1]];

    [select_mode setTextAlignment:[JSLocalizedString(@"Align", @"") intValue]];
    [select_mode setText:JSLocalizedString(@"Select Mode", @"")];
    [select_mode setFont:[FontManager mediumFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]+1]];

    [titleView setText:JSLocalizedString(@"Settings", @"")];
    [titleView setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]+4]];
    [titleView sizeToFit];

    [update_label setTextAlignment:[JSLocalizedString(@"Align", @"") intValue]];
    [update_label setFont:[UIFont fontWithName:@"CorporateS-Light" size:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [update_label sizeToFit];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:self.view forKey:@"view"];
    [dict setObject:background_view forKey:@"bView"];
    [dict setObject:language_segment forKey:@"Lsegment"];
    [dict setObject:select_lang forKey:@"Ltitle"];
    [dict setObject:mode_segment forKey:@"Msegment"];
    [dict setObject:select_mode forKey:@"Mtitle"];
    [dict setObject:update_label forKey:@"Lupdate"];
    [dict setObject:update_button forKey:@"Bupdate"];
    float width = self.view.frame.size.width - 40;
    [dict setObject:[NSNumber numberWithFloat:width] forKey:@"width"];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[bView(width)]" options:0 metrics:dict views:dict]];
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"mode"] isEqualToString:@"off"])
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[bView(280)]" options:0 metrics:nil views:dict]];
    else
        [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[bView(200)]" options:0 metrics:nil views:dict]];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"ar"])
    {
        [background_view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[Ltitle]-20-|" options:0 metrics:nil views:dict]];
        [background_view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[Ltitle]" options:0 metrics:nil views:dict]];

        [background_view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[Lsegment]-20-|" options:0 metrics:nil views:dict]];
        [background_view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[Ltitle]-20-[Lsegment]" options:0 metrics:nil views:dict]];
        
        [background_view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[Mtitle]-20-|" options:0 metrics:nil views:dict]];
        [background_view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[Lsegment]-20-[Mtitle]" options:0 metrics:nil views:dict]];
        
        [background_view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[Msegment]-20-|" options:0 metrics:nil views:dict]];
        [background_view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[Mtitle]-20-[Msegment]" options:0 metrics:nil views:dict]];

        [background_view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[Lupdate]-20-|" options:0 metrics:nil views:dict]];
        [background_view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[Msegment]-30-[Lupdate]" options:0 metrics:nil views:dict]];
        
        [background_view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[Bupdate]-20-|" options:0 metrics:nil views:dict]];
        [background_view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[Lupdate]-10-[Bupdate]" options:0 metrics:nil views:dict]];
    }
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"en"])
    {
        [background_view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[Ltitle]" options:0 metrics:nil views:dict]];
        [background_view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[Ltitle]" options:0 metrics:nil views:dict]];
        
        [background_view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[Lsegment]-20-|" options:0 metrics:nil views:dict]];
        [background_view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[Ltitle]-20-[Lsegment]" options:0 metrics:nil views:dict]];
    
        [background_view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[Mtitle]" options:0 metrics:nil views:dict]];
        [background_view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[Lsegment]-20-[Mtitle]" options:0 metrics:nil views:dict]];
        
        [background_view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[Msegment]-20-|" options:0 metrics:nil views:dict]];
        [background_view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[Mtitle]-20-[Msegment]" options:0 metrics:nil views:dict]];

        [background_view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[Lupdate]-20-|" options:0 metrics:nil views:dict]];
        [background_view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[Msegment]-30-[Lupdate]" options:0 metrics:nil views:dict]];
        
        [background_view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[Bupdate]" options:0 metrics:nil views:dict]];
        [background_view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[Lupdate]-10-[Bupdate]" options:0 metrics:nil views:dict]];

    }
}

#pragma mark - Segment Control
#pragma mark -

-(void)segmentUpdated:(UISegmentedControl *)sender
{
    if ([sender tag] == 10) // language segment
    {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"ar"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"en" forKey:@"language"];
            JSSetLanguage(@"en");
        }
        else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"en"])
        {
            [[NSUserDefaults standardUserDefaults] setObject:@"ar" forKey:@"language"];
            JSSetLanguage(@"ar");
        }
        
        // send notification to update/reload the bottom bar and library/books views
        [[NSNotificationCenter defaultCenter] postNotificationName:@"bottomBar" object:self];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"reloadListing" object:self];
        
        // reset and relaod the UI
        [self loadInterface];
        
    }
    else if ([sender tag] == 20) // mode segment
    {
        __weak typeof(self) weakSelf = self;

        if ([sender selectedSegmentIndex] == 1)
        {
            alertView = [[SIAlertView alloc] initWithTitle:JSLocalizedString(@"Offline", @"") andMessage:JSLocalizedString(@"Switch Offline", @"")];
            
            [alertView addButtonWithTitle:JSLocalizedString(@"No", @"")
                                     type:SIAlertViewButtonTypeDefault
                                  handler:^(SIAlertView *alert) {
                                      [sender setSelectedSegmentIndex:0];
                                      NSLog(@"Button1 Clicked");
                                  }];
            [alertView addButtonWithTitle:JSLocalizedString(@"Yes", @"")
                                     type:SIAlertViewButtonTypeDefault
                                  handler:^(SIAlertView *alert) {
                                      NSLog(@"Button2 Clicked");
                                      [Appdelegate performSelectorOnMainThread:@selector(hideLoadingIndicator) withObject:nil waitUntilDone:YES];
                                      [Appdelegate showLoadingIndicatorWithView:weakSelf.view andText:JSLocalizedString(@"Loading", @"")];
                                      [Appdelegate downloadOfflineContent];
                                      [[NSUserDefaults standardUserDefaults] setObject:@"off" forKey:@"mode"];
                                  }];
            [alertView setShowProgress:NO];
            [alertView setTransitionStyle:SIAlertViewTransitionStyleFade];
            [alertView setMessageFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
            [alertView setTitleFont:[FontManager mediumFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]+2]];
            [alertView setMessageColor:[ColorTheme textGrey]];
            [alertView setTitleColor:[ColorTheme textGrey]];
            [alertView setButtonFont:[FontManager mediumFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
            [alertView setButtonColor:[ColorTheme slawLightRed]];
            [alertView show];
        }
        else
        {
            alertView = [[SIAlertView alloc] initWithTitle:JSLocalizedString(@"Online", @"") andMessage:JSLocalizedString(@"Switch Online", @"")];
            
            [alertView addButtonWithTitle:JSLocalizedString(@"No", @"")
                                     type:SIAlertViewButtonTypeDefault
                                  handler:^(SIAlertView *alert) {
                                      [sender setSelectedSegmentIndex:1];
                                      NSLog(@"Button1 Clicked");
                                  }];
            [alertView addButtonWithTitle:JSLocalizedString(@"Yes", @"")
                                     type:SIAlertViewButtonTypeDefault
                                  handler:^(SIAlertView *alert) {
                                      [sender setSelectedSegmentIndex:0];
                                      [[NSUserDefaults standardUserDefaults] setObject:@"on" forKey:@"mode"];
                                      [weakSelf performSelectorOnMainThread:@selector(viewDidLayoutSubviews) withObject:nil waitUntilDone:YES];
                                  }];
            [alertView setShowProgress:NO];
            [alertView setTransitionStyle:SIAlertViewTransitionStyleFade];
            [alertView setMessageFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
            [alertView setTitleFont:[FontManager mediumFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]+2]];
            [alertView setMessageColor:[ColorTheme textGrey]];
            [alertView setTitleColor:[ColorTheme textGrey]];
            [alertView setButtonFont:[FontManager mediumFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
            [alertView setButtonColor:[ColorTheme slawLightRed]];
            [alertView show];

        }
    }

    [[NSUserDefaults standardUserDefaults] synchronize];

}

-(void)viewDidLayoutSubviews
{
    NSLog(@"layout subviews");
    
    // Adjust segment tint color:
    // After alert view is dismissed, segment tint is dimmed (gray)
    // Bring it back to red
    [language_segment setTintAdjustmentMode:UIViewTintAdjustmentModeNormal];
    [mode_segment setTintAdjustmentMode:UIViewTintAdjustmentModeNormal];
    
    [background_view layoutIfNeeded];
    [update_label layoutIfNeeded];
    [update_button layoutIfNeeded];
    
    [update_label setText:[NSString stringWithFormat:JSLocalizedString(@"Last Updated", @""), [dateFormatter stringFromDate:[Appdelegate deserializeJsonDateString:[[NSUserDefaults standardUserDefaults] objectForKey:@"LastUpdated"]]]]];

    
    if ([[[NSUserDefaults standardUserDefaults]objectForKey:@"mode"] isEqualToString:@"off"])
    {
        [update_label setHidden:NO];
        [update_button setHidden:NO];
        [background_view setFrame:CGRectMake(background_view.frame.origin.x, background_view.frame.origin.y, background_view.frame.size.width, 280)];
        [update_label setFrame:CGRectMake(update_label.frame.origin.x, update_label.frame.origin.y, update_label.frame.size.width, update_label.frame.size.height)];
        [update_button setFrame:CGRectMake(update_button.frame.origin.x, update_button.frame.origin.y, update_button.frame.size.width, update_button.frame.size.height)];
    }
    else
    {
        [update_label setHidden:YES];
        [update_button setHidden:YES];
        [background_view setFrame:CGRectMake(background_view.frame.origin.x, background_view.frame.origin.y, background_view.frame.size.width, 200)];
        [update_label setFrame:CGRectMake(update_label.frame.origin.x, update_label.frame.origin.y, update_label.frame.size.width, update_label.frame.size.height)];
        [update_button setFrame:CGRectMake(update_button.frame.origin.x, update_button.frame.origin.y, update_button.frame.size.width, update_button.frame.size.height)];
    }
    
}

#pragma mark - Progress View
#pragma mark -

-(void)startProgress
{
    [Appdelegate performSelectorOnMainThread:@selector(hideLoadingIndicator) withObject:nil waitUntilDone:YES];

    [self performSelectorOnMainThread:@selector(showProgressAlert) withObject:nil waitUntilDone:YES];
}

/**
 Show alertview containing progress bar
 */
-(void)showProgressAlert
{
    [alertView dismissAnimated:YES];
    [p_alertView dismissAnimated:YES];
    [alertView removeFromSuperview];
    [p_alertView removeFromSuperview];
    p_alertView = [[SIAlertView alloc] initWithTitle:@"" andMessage:JSLocalizedString(@"Download data",@"")];
    [p_alertView setShowProgress:YES];
    [p_alertView setTransitionStyle:SIAlertViewTransitionStyleFade];
    [p_alertView setMessageFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [p_alertView setTitleFont:[FontManager mediumFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]+2]];
    [p_alertView setMessageColor:[ColorTheme textGrey]];
    [p_alertView setTitleColor:[ColorTheme textGrey]];
    [p_alertView setButtonFont:[FontManager mediumFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [p_alertView setButtonColor:[ColorTheme slawLightRed]];
    [p_alertView show];
}

#pragma mark - Download Progress
#pragma mark

int counter = 0;

/**
 Animate progressview with the download progress percentage
 */
-(void) updateProgress:(NSNotification *)notification
{
    NSLog(@"\n------>P %@", [notification userInfo]);
    
    //make sure it's running on main thread
    dispatch_async(dispatch_get_main_queue(), ^{
        [p_alertView.download_progress setProgress:[[[notification userInfo] objectForKey:@"progress"] floatValue] animated:YES];
        [p_alertView.label_progress setText:[NSString stringWithFormat:@"%%%.f",[[[notification userInfo] objectForKey:@"progress"] floatValue]*100]];
        
        if ([[[notification userInfo] objectForKey:@"progress"] floatValue] == 1)
        {
            if (counter ==0) // when zip is downloaded and decompressed
            {
                [p_alertView.download_progress setProgress:0 animated:NO];
                [p_alertView.label_progress setText:@""];
            }
            else // when processing the laws
            {
                [p_alertView dismissAnimated:YES];
                [p_alertView removeFromSuperview];
                [self performSelectorOnMainThread:@selector(viewDidLayoutSubviews) withObject:nil waitUntilDone:YES];
            }
            counter++;
        }
    });
}

-(void)updateToast
{
    [Appdelegate performSelectorOnMainThread:@selector(hideLoadingIndicator) withObject:nil waitUntilDone:YES];

    [KSToastView ks_setAppearanceMaxLines:0];
    [KSToastView ks_setAppearanceMaxWidth:[UIScreen mainScreen].bounds.size.width - 60];
    [KSToastView ks_setAppearanceTextFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]+2]];
    [KSToastView ks_setAppearanceTextColor:[UIColor whiteColor]];
    [KSToastView ks_setAppearanceTextAligment:NSTextAlignmentCenter];
    [KSToastView ks_setAppearanceBackgroundColor:[ColorTheme textGrey]];
    [KSToastView ks_setAppearanceCornerRadius:5.0];
    [KSToastView ks_setAppearanceOffsetBottom:60.0];
    [KSToastView ks_setAppearanceTextInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
    [KSToastView ks_showToast:JSLocalizedString(@"Latest Version", @"") duration:4.0 delay:0.5];
    
    [self performSelectorOnMainThread:@selector(viewDidLayoutSubviews) withObject:nil waitUntilDone:YES];
}

#pragma mark - Check for updates
#pragma mark -

/**
 "Check for updates" button clicked

 */
-(IBAction) clicked:(UIButton*)sender{
    
    // animate the button when clicked: zoom in/out effect
    
    [UIView animateWithDuration:0.5f animations:^{
        
        sender.transform = CGAffineTransformMakeScale(1.5, 1.5);
    } completion:^(BOOL finished){}];
    // for zoom out
    [UIView animateWithDuration:0.5f animations:^{
        
        sender.transform = CGAffineTransformMakeScale(1, 1);
    }completion:^(BOOL finished){

        [Appdelegate performSelectorOnMainThread:@selector(hideLoadingIndicator) withObject:nil waitUntilDone:YES];
        [Appdelegate showLoadingIndicatorWithView:self.view andText:JSLocalizedString(@"Loading", @"")];

        [Appdelegate downloadOfflineContent];
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
