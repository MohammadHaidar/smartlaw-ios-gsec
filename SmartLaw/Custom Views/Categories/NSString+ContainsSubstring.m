//
//  NSString+ContainsSubstring.m
//  DubizzleHorizontal
//
//  Created by Fawad Suhail on 11/19/14.
//  Copyright (c) 2014 dubizzle. All rights reserved.
//

#import "NSString+ContainsSubstring.h"

@implementation NSString (ContainsSubstring)

-(BOOL) containsSubstring:(NSString *)aString {
    
    if ([self rangeOfString:aString].location == NSNotFound) {
        
        return NO;
    }
    
    else {
        
        return YES;
    }
}

@end
