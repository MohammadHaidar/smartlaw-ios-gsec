//
//  LandingPage.h
//  SmartLaw
//
//  Created by Krystel Chaccour on 3/1/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LandingPage : UIViewController
@property (nonatomic, retain) UINavigationController *nc1;
@property (nonatomic, retain) UINavigationController *nc2;
@property (nonatomic, retain) UINavigationController *nc3;

@property (nonatomic, retain) UIImageView *logo;

@property (nonatomic, retain) UIButton *category_btn;
@property (nonatomic, retain) UIButton *ta3mim_btn;
@property (nonatomic, retain) UIButton *myBooks_btn;
@property (nonatomic, retain) UIButton *portal_btn;
@property (nonatomic, retain) UIButton *settings_btn;
@property (nonatomic, retain) UIButton *contact_btn;
@property (nonatomic, retain) UIButton *faq_btn;
@property (nonatomic, retain) UIButton *search_btn;

@property (nonatomic, retain) UILabel *titleView;
@property (nonatomic, retain) UILabel *category_lbl;
@property (nonatomic, retain) UILabel *ta3mim_lbl;
@property (nonatomic, retain) UILabel *myBooks_lbl;
@property (nonatomic, retain) UILabel *portal_lbl;
@property (nonatomic, retain) UILabel *settings_lbl;
@property (nonatomic, retain) UILabel *contact_lbl;
@property (nonatomic, retain) UILabel *faq_lbl;
@property (nonatomic, retain) UILabel *search_lbl;

-(void)setUpTitleView;

@end
