//
//  SettingsPage.h
//  SmartLaw
//
//  Created by Krystel Chaccour on 3/1/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import <UIKit/UIKit.h>
@interface SettingsPage : UIViewController

@property (nonatomic, retain) UILabel *select_lang;
@property (nonatomic, retain) UILabel *select_mode;
@property (nonatomic, retain) UILabel *titleView;
@property (nonatomic, retain) UISegmentedControl *language_segment;
@property (nonatomic, retain) UISegmentedControl *mode_segment;
@property (nonatomic, retain) UIView *background_view;
@property (nonatomic, retain) UIButton *dismiss_btn;
@property (nonatomic, retain) UIButton *update_button;
@property (nonatomic, retain) UILabel *update_label;
-(void)loadInterface;
-(void)segmentUpdated:(UISegmentedControl *)sender;

@end
