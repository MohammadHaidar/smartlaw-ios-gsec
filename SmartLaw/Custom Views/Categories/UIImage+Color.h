//
//  UIImage+Color.h
//  DubizzleHorizontal
//
//  Created by Robert Dimitrov on 5/8/14.
//  Copyright (c) 2014 Dubizzle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (Color)

- (UIImage *)clr_tintWithColor:(UIColor *)tintColor;

@end
