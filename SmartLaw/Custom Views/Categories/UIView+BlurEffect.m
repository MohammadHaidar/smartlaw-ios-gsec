//
//  UIView+BlurEffect.m
//  DubizzleHorizontal
//
//  Created by Robert Dimitrov on 5/16/14.
//  Copyright (c) 2014 Dubizzle. All rights reserved.
//

#import "UIView+BlurEffect.h"
#import "UIImage+ImageEffects.h"

@implementation UIView (BlurEffect)

- (UIImage *)blurredSnapshot {
    // Create the image context
    UIGraphicsBeginImageContextWithOptions(self.bounds.size,
                                           YES, self.window.screen.scale);
    
    // There he is! The new API method
    [self drawViewHierarchyInRect:CGRectMake(0, -20, CGRectGetWidth(self.frame), CGRectGetHeight(self.frame) + 20) afterScreenUpdates:NO];
    
    // Get the snapshot
    UIImage *snapshotImage = UIGraphicsGetImageFromCurrentImageContext();
    
    // Now apply the blur effect using Apple's UIImageEffect category
    
    snapshotImage = [snapshotImage applyBlurWithRadius:3
                                             tintColor:[UIColor colorWithRed:1.0 green:1.0 blue:1.0 alpha:0.3]
                                 saturationDeltaFactor:1.8
                                             maskImage:nil];
    
    // Be nice and clean your mess up
    UIGraphicsEndImageContext();
    
    return snapshotImage;
}

@end
