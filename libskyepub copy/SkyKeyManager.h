//
//  SkyDRM.h
//  SkyDemo
//
//  Created by 하늘나무 on 2015. 11. 6..
//  Copyright © 2015년 Skytree Corporation. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SkyKeyManager : NSObject {
    NSString* clientId;
    NSString* clientSecret;
    
    NSString* accessToken;
    NSMutableDictionary* keys;
}

-(id)initWithClientId:(NSString*)clientId clientSecret:(NSString*)clientSecret;
-(NSString*)getKey:(NSString*)uuidForEpub uuidForContent:(NSString*)uuidForContent;

@property (nonatomic, retain) NSString* clientId;
@property (nonatomic, retain) NSString* clientSecret;
@property (nonatomic, retain) NSString* accessToken;
@property (nonatomic, retain) NSMutableDictionary* keys;

@end
