//
//  UITableViewCell+DBZAdditions.m
//  DubizzleHorizontal
//
//  Created by Robert Dimitrov on 5/9/14.
//  Copyright (c) 2014 Dubizzle. All rights reserved.
//

#import "UITableViewCell+DBZAdditions.h"

@implementation UITableViewCell (DBZAdditions)

- (void)dbz_setSelectedBackgroundColor:(UIColor *)color {
    self.selectedBackgroundView = [[UIView alloc] initWithFrame:self.bounds];
    self.selectedBackgroundView.autoresizingMask = UIViewAutoresizingFlexibleHeight;
    self.selectedBackgroundView.backgroundColor = color;
}

@end
