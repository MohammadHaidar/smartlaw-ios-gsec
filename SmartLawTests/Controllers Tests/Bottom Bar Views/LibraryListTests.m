//
//  LibraryListTests.m
//  SmartLaw
//
//  Created by Krystel Chaccour on 6/15/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "LibraryList.h"
#import "JSLocalizedString.h"
#import "AppDelegate.h"

@interface LibraryListTests : XCTestCase
@property LibraryList *library;
@property AppDelegate *temp_delegate;

@end

@implementation LibraryListTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    self.library = [[LibraryList alloc] init];
    self.temp_delegate = ((AppDelegate *)[[UIApplication sharedApplication] delegate]);
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    
    self.library = nil;
    self.temp_delegate = nil;

    [super tearDown];
}

- (void)testClassInitialization {
    
    XCTAssertNotNil(self.library, @"Class is nil");
}

- (void)testDismissButton {
    
    [self.library setUpDismissButton];
    
    XCTAssertNotNil(self.library.dismiss_btn, @"Dismiss button is nil");
    XCTAssertNotNil(self.library.navigationItem.leftBarButtonItem, @"Navigation left item is nil");

}

- (void)testInitializeVariables {
    
    [self.library initializeVariables];
    
    XCTAssertEqual(self.library.loadMore, 0, @"Load more initially is %d", self.library.loadMore);
    XCTAssertEqual(self.library.startIndex, 0, @"Start index initially is %d", self.library.startIndex);
    if ([self.temp_delegate isPad])
        XCTAssertEqual(self.library.endIndex, 9, @"End index initially is %d", self.library.endIndex);
    else
        XCTAssertEqual(self.library.endIndex, 6, @"End index initially is %d", self.library.endIndex);
    
    XCTAssertFalse(self.library.isLoadingMore, @"isLoadingMore is true");
    XCTAssertFalse(self.library.is_downloading, @"is_downloading is true");
    
}

- (void)testTitleView {
    
    [self.library setUpTitleView];

    XCTAssertEqualObjects(self.library.navigationItem.titleView, self.library.titleView, @"Navigation item should have a title view");
    XCTAssertEqualObjects(self.library.titleView.text, self.library.categ_Title, @"Navigation item should be %@",self.library.categ_Title);
}

- (void)testSearchButton {
    
    [self.library setUpSearchButton];

    XCTAssertNotNil(self.library.searchButton, @"Search button is nil");
    XCTAssertEqualObjects(self.library.navigationItem.rightBarButtonItem, self.library.searchItem, @"Navigation right item should have a search button");

    XCTAssertNotNil(self.library.searchBar, @"Search bar is nil");
}

- (void)testBarView {

    [self.library loadBar];
    
    XCTAssertNotNil(self.library.barView, @"Bar view is nil");
    XCTAssertNotNil(self.library.gridView, @"Grid view is nil");
    XCTAssertNotNil(self.library.fakeG, @"Fake grid button is nil");
    XCTAssertNotNil(self.library.listView, @"List view is nil");
    XCTAssertNotNil(self.library.fakeL, @"Fake list button is nil");
    XCTAssertNotNil(self.library.sortA, @"Sort Asc is nil");
    XCTAssertNotNil(self.library.fakeSA, @"Fake sort asc is nil");
    XCTAssertNotNil(self.library.sortZ, @"Sort Desc is nil");
    XCTAssertNotNil(self.library.fakeSZ, @"Fake sort desc is nil");
    XCTAssertTrue(self.library.gridView.selected, @"Grid view is not selected");
}

- (void)testCollectionView {
    
    [self.library loadCollection];
    
    XCTAssertNotNil(self.library.grid_collection, @"Collecton is nil");
}

- (void)testTableView {
    
    [self.library loadBar];
    [self.library loadTable];
    
    XCTAssertNotNil(self.library.list_table, @"Table is nil");
}

- (void)testFooterView {
    
    [self.library initFooterView];
    
    XCTAssertNotNil(self.library.footerView, @"Footer is nil");
}

- (void)testButtonSelectionAsc {
    
    UIButton *btn = [UIButton new];
    [btn setTag:100];
    [self.library buttonSelected:btn];
    
}

-(void)testEmptyLabelLib {
    
    [self.library emptyWithType:2];
    
    XCTAssertNotNil(self.library.empty_label, @"Empty label is nil");
    XCTAssertEqualObjects(self.library.empty_label.text, JSLocalizedString(@"Empty Library", @""), @"Empty label should be %@",JSLocalizedString(@"Empty Library", @""));
}

-(void)testEmptyLabelSearch {
    
    [self.library emptyWithType:3];
    
    XCTAssertNotNil(self.library.empty_label, @"Empty label is nil");
    XCTAssertEqualObjects(self.library.empty_label.text, JSLocalizedString(@"Empty Search", @""), @"Empty label should be %@",JSLocalizedString(@"Empty Search", @""));
}

@end

