//
//  BookDetailsTests.m
//  SmartLaw
//
//  Created by Krystel Chaccour on 6/15/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import <XCTest/XCTest.h>
#import <OCMock/OCMock.h>
#import "AppDelegate.h"
#import "BookDetails.h"
#import <TacoShell/TacoShell.h>
#import "Constants.h"

@interface BookDetailsTests : XCTestCase
{
    NSDateFormatter *dateTimeformatter;
}
@property BookDetails *bookDetails;
@property NSMutableDictionary *temp_dict;
@property AppDelegate *temp_delegate;

@end

@implementation BookDetailsTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
    
    self.bookDetails = [[BookDetails alloc] init];
   
    self.temp_dict = [NSMutableDictionary dictionary];
    [self.temp_dict setObject:@"Sader" forKey:@"Author"];
    [self.temp_dict setObject:@"AUHEC_Book1" forKey:@"Code"];
    [self.temp_dict setObject:@"http://62.84.75.75:8585/Books/EPUB/AUHEC_Book1/AUHEC_Book1.epub" forKey:@"EPUB"];
    [self.temp_dict setObject:[NSNumber numberWithInt:95] forKey:@"ID"];
    [self.temp_dict setObject:@"http://62.84.75.75:8585/Books/Cover/AUHEC_Book1.png" forKey:@"Image"];
    [self.temp_dict setObject:[NSNumber numberWithInt:1] forKey:@"Language"];
    [self.temp_dict setObject:@"/Date(1466773861083)/" forKey:@"PublishedDate"];
    [self.temp_dict setObject:@"Book summary" forKey:@"Summary"];
    [self.temp_dict setObject:@"Book title" forKey:@"Title"];
   
    self.temp_delegate = ((AppDelegate *)[[UIApplication sharedApplication] delegate]);

    
    dateTimeformatter=[[NSDateFormatter alloc]init];
    [dateTimeformatter setLocale:[NSLocale currentLocale]];
    [dateTimeformatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [dateTimeformatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];

    
    [self.bookDetails loadInterface];
    
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    
    self.bookDetails = nil;
    self.temp_dict = nil;
    self.temp_delegate = nil;
    [super tearDown];
}

- (void)testInitialization {
    XCTAssertNotNil(self.bookDetails, @"Settings page should not be nil");
}

- (void)testBookDetailsDict {

    XCTAssertNotNil(self.temp_dict, @"Temp dict should not be nil");
}

- (void)testScrollView {
    
    XCTAssertNotNil(self.bookDetails.scroll_view, @"Scroll view should not be nil");
}

- (void)testBookImage {
    
    XCTAssertNotNil(self.bookDetails.book_image, @"Book image should not be nil");
}

- (void)testDeserializedString {
    
    XCTAssertNotNil([dateTimeformatter stringFromDate:[self.bookDetails deserializeJsonDateString:[self.temp_dict objectForKey:@"PublishedDate"]]]);
    XCTAssertEqualObjects(@"2016-06-24 16:11:01", [dateTimeformatter stringFromDate:[self.bookDetails deserializeJsonDateString:[self.temp_dict objectForKey:@"PublishedDate"]]], @"should be equal");
}

- (void)testEpubInstall {
    
    NSFileManager *fm = [NSFileManager defaultManager];
    NSString *downloadPath = [self.temp_delegate getDownloadPath:[NSString stringWithFormat:@"%@.epub",[self.temp_dict objectForKey:@"Code"]]];
    
    BOOL success = [self.temp_delegate installEPubWithLink:[self.temp_dict objectForKey:@"EPUB"] andfilName:[NSString stringWithFormat:@"%@.epub",[self.temp_dict objectForKey:@"Code"]] andBookId:[self.temp_dict objectForKey:@"ID"] andIndex:nil andLastUpdateDate:[dateTimeformatter stringFromDate:[self.temp_dict objectForKey:@"PublishedDate"]] withType:3];

    
    BOOL isExists = [fm fileExistsAtPath:downloadPath];
    if (isExists)
        XCTAssertFalse(success, @"Book already installed in db");
    else
        XCTAssertTrue(success, @"Book not installed in db");
}

- (void)testIfBookIsDownloaded {
    
    [self.bookDetails bookDownloaded:nil];
    
    XCTAssertTrue(self.bookDetails.bookDownloaded, @"bookDownloaded is false");
    XCTAssertFalse(self.bookDetails.bookIsDownloading, @"bookIsDownloading is true");
    XCTAssertTrue(self.bookDetails.isDownloaded, @"isDownloaded is false");
    XCTAssertFalse(self.bookDetails.delete_Btn.hidden, @"delete_btn is hidden");
    XCTAssertFalse(self.bookDetails.open_Btn.hidden, @"open_Btn is hidden");
    XCTAssertTrue(self.bookDetails.download_Btn.hidden, @"download_Btn is not hidden");
}


- (void)testDeleteBook {
    
    BOOL success = [self.temp_delegate deleteBookByCode:[self.temp_dict objectForKey:@"ID"] withName:[self.temp_dict objectForKey:@"Title"] andFileName:[NSString stringWithFormat:@"%@.epub",[self.temp_dict objectForKey:@"Code"]]];
    
    XCTAssertTrue(success, @"Book is not deleted");
}

- (void)testReloadAfterDelete {
    
    XCTAssertFalse(self.bookDetails.bookIsDownloading, @"bookIsDownloading is true");
    XCTAssertFalse(self.bookDetails.bookDownloaded, @"bookDownloaded is true");
    XCTAssertFalse(self.bookDetails.isDownloaded, @"isDownloaded is true");

    XCTAssertTrue(self.bookDetails.delete_Btn.hidden, @"delete_btn is not hidden");
    XCTAssertTrue(self.bookDetails.open_Btn.hidden, @"open_Btn is not hidden");

}


@end
