//
//  ListViewCell.h
//  SmartLaw
//
//  Created by Krystel Chaccour on 3/3/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FRDLivelyButton/FRDLivelyButton.h>

@interface ListViewCell : UITableViewCell

@property (nonatomic, retain) IBOutlet UIButton *fakeView;
@property (nonatomic, retain) IBOutlet UIImageView *rowType;
@property (nonatomic, retain) IBOutlet UILabel *book_title;
@property (nonatomic, strong) IBOutlet FRDLivelyButton *accessoryBtn;

@end
