//
//  FooterReusableView.m
//  SmartLaw
//
//  Created by Krystel Chaccour on 4/8/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import "FooterReusableView.h"

@implementation FooterReusableView

-(id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self)
    {
        self.spinner = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleArcAlt color:[ColorTheme textGrey]];
        [self.spinner setHidesWhenStopped:YES];
        [self.spinner setFrame:CGRectMake([UIScreen mainScreen].bounds.size.width/2 -10, 10, 20, 20)];
        [self addSubview:self.spinner];
    }
    
    return self;
}
@end
