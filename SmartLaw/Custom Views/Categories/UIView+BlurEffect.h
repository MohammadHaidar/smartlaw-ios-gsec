//
//  UIView+BlurEffect.h
//  DubizzleHorizontal
//
//  Created by Robert Dimitrov on 5/16/14.
//  Copyright (c) 2014 Dubizzle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (BlurEffect)

-(UIImage *)blurredSnapshot;

@end
