//
//  UIViewController+DBZAdditions.h
//  DubizzleHorizontal
//
//  Created by Robert Dimitrov on 5/7/14.
//  Copyright (c) 2014 Dubizzle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIViewController (DBZAdditions)

- (void)dbz_pushViewController:(UIViewController *)viewController animated:(BOOL)animated;

@end
