//
//  AppDelegate.h
//  SmartLaw
//
//  Created by Krystel Chaccour on 3/1/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Reachability.h"

@class Highlight;
@class Setting;
@class PagingInformation;
@interface AppDelegate : UIResponder <UIApplicationDelegate>
@property (nonatomic, retain) IBOutlet UIWindow *window;
@property BOOL isRotationLocked;
@property (nonatomic, retain) Reachability *reach;
@property (nonatomic, retain) NSArray *favorites_list;
@property (nonatomic, retain) NSMutableArray *booksListArray;

-(BOOL)isAbove5;
-(BOOL)isPad;

-(NSArray *)retrieveFavorites;
-(void)downloadOfflineContent;
-(NSArray *) retrieveFromTableWithCondition:(NSString *)conditionStr andDocumentType:(NSInteger)docType;
-(NSArray *) retrieveChildrenFromTableWithCondition:(NSString *)conditionStr andDepth:(NSInteger)newDepth;
-(NSArray *)searchDatabaseWithKeywords:(NSString *)keywords;

-(void)showLoadingIndicatorWithView:(UIView *)on_view andText:(NSString *)loadText;
-(void)hideLoadingIndicator;

- (NSDate *)deserializeJsonDateString: (NSString *)jsonDateString;

-(void)reachabilityChanged:(NSNotification*)note;
-(void)showConnectionAlertWithView:(UIView *)on_View;

@end

