//
//  RowObject.m
//  SmartLaw
//
//  Created by Krystel Chaccour on 11/25/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import "RowObject.h"

@implementation RowObject


+(instancetype)fileWithName:(NSString *)fileName fileId:(NSString *)fileId masterId:(NSString *)masterId parentId:(NSString *)parentId nodeType:(NSInteger)nodeType selectable:(BOOL)selectable childNodesCount:(NSInteger)childNodesCount childNodes:(NSArray *)childNodes htmlLink:(NSString *)htmlLink expand:(BOOL)expand isChild:(BOOL)isChild isParent:(BOOL)isParent depth:(NSInteger)depth;
{
    RowObject *aLaw = [[RowObject alloc] initWithFileWithName:fileName fileId:fileId masterId:masterId parentId:parentId nodeType:nodeType selectable:selectable childNodesCount:childNodesCount childNodes:childNodes htmlLink:htmlLink expand:expand isChild:isChild isParent:isParent depth:depth];
    
    return aLaw;
}

+(instancetype)law
{
    RowObject *aLaw = [[RowObject alloc] initWithFileWithName:@"" fileId:@"" masterId:@"" parentId:@"" nodeType:0 selectable:NO childNodesCount:0 childNodes:nil htmlLink:@"" expand:NO isChild:NO isParent:NO depth:0];
    
    return aLaw;
}

-(instancetype)initWithFileWithName:(NSString *)fileName fileId:(NSString *)fileId masterId:(NSString *)masterId parentId:(NSString *)parentId nodeType:(NSInteger)nodeType selectable:(BOOL)selectable childNodesCount:(NSInteger)childNodesCount childNodes:(NSArray *)childNodes htmlLink:(NSString *)htmlLink expand:(BOOL)expand isChild:(BOOL)isChild isParent:(BOOL)isParent depth:(NSInteger)depth;
{
    if (self = [super init])
    {
        self.file_name = fileName;
        self.file_id = fileId;
        self.master_id = masterId;
        self.file_parentId = parentId;
        self.nodeType = nodeType;
        self.selectable = selectable;
        self.childNodesCount = childNodesCount;
        self.childNodes = childNodes;
        self.htmlLink = htmlLink;
        self.expand = expand;
        self.isChild = isChild;
        self.isParent = isParent;
        self.depth = depth;
    }
    return self;
}


@end
