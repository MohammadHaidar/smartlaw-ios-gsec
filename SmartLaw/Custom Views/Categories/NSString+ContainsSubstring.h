//
//  NSString+ContainsSubstring.h
//  DubizzleHorizontal
//
//  Created by Fawad Suhail on 11/19/14.
//  Copyright (c) 2014 dubizzle. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (ContainsSubstring)
-(BOOL) containsSubstring:(NSString *)aString;
@end
