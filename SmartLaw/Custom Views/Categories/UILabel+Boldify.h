//
//  UILabel+Boldify.h
//  DubizzleHorizontal
//
//  Created by Fawad Suhail on 4/28/14.
//  Copyright (c) 2014 Dubizzle. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface UILabel (Boldify)
- (void) boldSubstring: (NSString*) substring;
- (void) boldRange: (NSRange) range;
@end