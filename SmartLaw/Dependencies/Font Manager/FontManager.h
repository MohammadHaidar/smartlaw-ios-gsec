//
//  FontManager.h
//  SmartLaw
//
//  Created by Krystel Chaccour on 3/1/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface FontManager : NSObject

+(UIFont *) regularFontOfSize:(CGFloat)pointSize;
+(UIFont *) mediumFontOfSize:(CGFloat)pointSize;
+(UIFont *) lightFontOfSize:(CGFloat)pointSize;
+(UIFont *) boldFontOfSize:(CGFloat)pointSize;

@end
