//
//  BookDetails.m
//  SmartLaw
//
//  Created by Krystel Chaccour on 3/3/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import "BookDetails.h"
#import <Canvas/Canvas.h>
#import "GBKUIButtonProgressView.h"
#import "BookViewController.h"

@interface BookDetails ()
{
    CSAnimationView *detailsView;
    NSDateFormatter *dateTimeformatter;
    
    UILabel *book_summary_txt;
    UILabel *book_summary;
    
    UIView *summaryView;
    UIView *bookinfoView;
    UILabel *book_update;
}
@property (retain, nonatomic) IBOutlet UIProgressView *progressView;
@property (strong, nonatomic) NSOperationQueue *fakeDownloadRequest;
@property (nonatomic, assign) BOOL updateUI;

@end

@implementation BookDetails
@synthesize scroll_view, book_image;

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.updateUI = YES;
    
    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]]];

    // call function when connection status changes
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object: nil];

    [Appdelegate hideLoadingIndicator];
    [Appdelegate showLoadingIndicatorWithView:self.view andText:JSLocalizedString(@"Loading Book", @"")];

    // call function when book is done downloading to reload book cell
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(bookDownloaded:) name:@"detailsFileSaved" object:nil];
    // call function to update download progress of book
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(updateProgress:) name:@"downloadProgress" object:nil];
    // call function after book was deleted successfully
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reloadAfterDelete) name:@"bookDeleted" object:nil];

    // add custom back button to navigation bar
    UIImage *dismiss_image= [UIImage imageNamed:@"Left-Arrow"];
    UIButton *dismiss_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    dismiss_btn.bounds = CGRectMake( 10, 0, dismiss_image.size.width, dismiss_image.size.height);
    [dismiss_btn addTarget:self action:@selector(dismissPage) forControlEvents:UIControlEventTouchUpInside];
    [dismiss_btn setImage:dismiss_image forState:UIControlStateNormal];
    
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:dismiss_btn];
    self.navigationItem.leftBarButtonItem = leftButton;
   
    dispatch_async(dispatch_get_main_queue(), ^{
        [self retrieveDetails];
    });
}

-(void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    
    [self dismissPage];
}

// Function called to go back to the previous/parent view controller
-(void)dismissPage
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Get Details
#pragma mark

// Function called to retrieve a book details
-(void)retrieveDetails
{
    if ([Appdelegate.reach currentReachabilityStatus] == NotReachable)
    {
        [Appdelegate hideLoadingIndicator];
        [Appdelegate showConnectionAlertWithView:self.view];
    }
    else
    {
        TacoShell *ts = [[TacoShell alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/GetOnlineBookDetail",stagingURL]]];
        ts.method = BKTSRequestMethodPOST;
        
        NSMutableDictionary *dict = [[NSMutableDictionary alloc] init];
        [dict setObject:[NSNumber numberWithInt:self.book_id] forKey:@"BookID"];
        
        ts.POSTDictionary = dict;
        ts.contentType = BKTSRequestContentTypeApplicationJSON;
        
        ts.completionBlock = ^(NSDictionary *dictionary, NSInteger httpResponseStatusCode, id response){
            
            if ([[[[response objectForKey:@"d"] objectForKey:@"Status"] objectForKey:@"StatusCode"] intValue] == 0)
            {
                [Appdelegate hideLoadingIndicator];
                
                self.bookDetails = [NSMutableDictionary dictionaryWithDictionary:[[response objectForKey:@"d"] objectForKey:@"BookDetails"]];
                NSLog(@"\n details > %@", self.bookDetails);
                [self loadInterface];
            }
            else
                [Appdelegate hideLoadingIndicator];
            
        };
        
        ts.errorBlock = ^(NSError *error){
            if (error)
                NSLog(@"error: %@", error);
            [Appdelegate hideLoadingIndicator];
            [Appdelegate showConnectionAlertWithView:self.view];
        };
        
        [ts start];
    }
}


#pragma mark - Load Interface
#pragma mark-

// Funciton to create all view subviews - UI
-(void)loadInterface
{
    [scroll_view removeFromSuperview];
    scroll_view = [[UIScrollView alloc] init];
    [scroll_view setTranslatesAutoresizingMaskIntoConstraints:NO];
    [scroll_view setScrollEnabled:YES];
    [scroll_view setScrollsToTop:YES];
    [scroll_view setShowsHorizontalScrollIndicator:NO];
    [scroll_view setShowsVerticalScrollIndicator:NO];
    [scroll_view setBackgroundColor:[UIColor clearColor]];
    [self.view addSubview:scroll_view];
    
    [detailsView removeFromSuperview];
    detailsView = [[CSAnimationView alloc] init];
    [detailsView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [detailsView setType:CSAnimationTypeFadeIn];
    [detailsView setDelay:0.3];
    [detailsView setDuration:0.6];
    [scroll_view addSubview:detailsView];
    
    [book_image removeFromSuperview];
    book_image = [[UIImageView alloc] init];
    [book_image setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSData *img_data = [NSData dataWithContentsOfURL:[NSURL URLWithString:[self.bookDetails objectForKey:@"Image"]]];
    if (img_data)
        [book_image setImage:[UIImage imageWithData:img_data]];
    else
        [book_image setImage:[UIImage imageNamed:@"Pattern_Image"]];
    [book_image setContentMode:UIViewContentModeScaleAspectFill];
    [book_image setUserInteractionEnabled:YES];
    [book_image setClipsToBounds:YES];
    [detailsView addSubview:book_image];
   
    [self.download_Btn removeFromSuperview];
    self.download_Btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.download_Btn setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.download_Btn setImage:[UIImage imageNamed:@"Download_Btn"] forState:UIControlStateNormal];
    [self.download_Btn setBackgroundColor:[UIColor clearColor]];
    [self.download_Btn addTarget:self action:@selector(installEpub:) forControlEvents:UIControlEventTouchUpInside];
    [self.download_Btn setHidden:YES];
    [detailsView addSubview:self.download_Btn];
    
    [self.open_Btn removeFromSuperview];
    self.open_Btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.open_Btn setTranslatesAutoresizingMaskIntoConstraints:NO];
    if ([[UIScreen mainScreen] bounds].size.width > 320)
    {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"ar"])
            [self.open_Btn setImage:[UIImage imageNamed:@"Open_Btn(ar)"] forState:UIControlStateNormal];
        else
            [self.open_Btn setImage:[UIImage imageNamed:@"Open_Btn(en)"] forState:UIControlStateNormal];
    }
    else
    {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"ar"])
            [self.open_Btn setImage:[UIImage imageNamed:@"Open_BtnS(ar)"] forState:UIControlStateNormal];
        else
            [self.open_Btn setImage:[UIImage imageNamed:@"Open_BtnS(en)"] forState:UIControlStateNormal];
    }
    [self.open_Btn setBackgroundColor:[UIColor clearColor]];
    [self.open_Btn addTarget:self action:@selector(openItem) forControlEvents:UIControlEventTouchUpInside];
    [self.open_Btn setHidden:YES];
    [detailsView addSubview:self.open_Btn];

    [self.delete_Btn removeFromSuperview];
    self.delete_Btn = [UIButton buttonWithType:UIButtonTypeCustom];
    [self.delete_Btn setTranslatesAutoresizingMaskIntoConstraints:NO];
    if ([[UIScreen mainScreen] bounds].size.width > 320)
    {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"ar"])
            [self.delete_Btn setImage:[UIImage imageNamed:@"Delete_Btn(ar)"] forState:UIControlStateNormal];
        else
            [self.delete_Btn setImage:[UIImage imageNamed:@"Delete_Btn(en)"] forState:UIControlStateNormal];
    }
    else
    {
        if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"ar"])
            [self.delete_Btn setImage:[UIImage imageNamed:@"Delete_BtnS(ar)"] forState:UIControlStateNormal];
        else
            [self.delete_Btn setImage:[UIImage imageNamed:@"Delete_BtnS(en)"] forState:UIControlStateNormal];
    }
    [self.delete_Btn setBackgroundColor:[UIColor clearColor]];
    [self.delete_Btn addTarget:self action:@selector(deleteBook) forControlEvents:UIControlEventTouchUpInside];
    [self.delete_Btn setHidden:YES];
    [detailsView addSubview:self.delete_Btn];
    
    [self.progressView removeFromSuperview];
    self.progressView = [[UIProgressView alloc] init];
    [self.progressView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.progressView setTrackTintColor:[ColorTheme lightGrey]];
    [self.progressView setProgressTintColor:[ColorTheme slawLightRed]];
//    [self.progressView setBackgroundColor:[UIColor yellowColor]];
    [self.progressView setHidden:YES];
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"ar"])
        [self.progressView setTransform:CGAffineTransformMakeScale(-1, 1)];
    else
        [self.progressView setTransform:CGAffineTransformMakeScale(1, 1)];
    [detailsView addSubview:self.progressView];

    [bookinfoView removeFromSuperview];
    bookinfoView = [[UIView alloc] init];
    [bookinfoView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [bookinfoView setBackgroundColor:[UIColor whiteColor]];
    [bookinfoView.layer setCornerRadius:7.0];
    [detailsView addSubview:bookinfoView];
    
    UILabel *book_title = [[UILabel alloc] init];
    [book_title setTranslatesAutoresizingMaskIntoConstraints:NO];
    [book_title setTextAlignment:[JSLocalizedString(@"Align", @"") intValue]];
    [book_title setTextColor:[ColorTheme textGrey]];
    [book_title setText:[self.bookDetails objectForKey:@"Title"]];
    [book_title setFont:[FontManager mediumFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]+6]];
    [book_title setBackgroundColor:[UIColor clearColor]];
    [book_title setNumberOfLines:0];
    [book_title setLineBreakMode:NSLineBreakByWordWrapping];
    [book_title setClipsToBounds:YES];
    [bookinfoView addSubview:book_title];
    
    UILabel *book_writer = [[UILabel alloc] init];
    [book_writer setTranslatesAutoresizingMaskIntoConstraints:NO];
    [book_writer setText:JSLocalizedString(@"Author", @"")];
    [book_writer setTextAlignment:[JSLocalizedString(@"Align", @"") intValue]];
    [book_writer setTextColor:[ColorTheme textGrey]];
    [book_writer setFont:[FontManager mediumFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]+2]];
    [book_writer setBackgroundColor:[UIColor clearColor]];
    [bookinfoView addSubview:book_writer];

    UILabel *book_writer_txt = [[UILabel alloc] init];
    [book_writer_txt setTranslatesAutoresizingMaskIntoConstraints:NO];
    [book_writer_txt setText:[self.bookDetails objectForKey:@"Author"]];
    [book_writer_txt setTextAlignment:[JSLocalizedString(@"Align", @"") intValue]];
    [book_writer_txt setTextColor:[ColorTheme textGrey]];
    [book_writer_txt setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [book_writer_txt setBackgroundColor:[UIColor clearColor]];
    [book_writer_txt setNumberOfLines:0];
    [book_writer_txt setLineBreakMode:NSLineBreakByWordWrapping];
    [bookinfoView addSubview:book_writer_txt];
    
    UILabel *book_language = [[UILabel alloc] init];
    [book_language setTranslatesAutoresizingMaskIntoConstraints:NO];
    [book_language setText:JSLocalizedString(@"Language", @"")];
    [book_language setTextAlignment:[JSLocalizedString(@"Align", @"") intValue]];
    [book_language setTextColor:[ColorTheme textGrey]];
    [book_language setFont:[FontManager mediumFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]+2]];
    [book_language setBackgroundColor:[UIColor clearColor]];
    [bookinfoView addSubview:book_language];
    
    UILabel *book_language_txt = [[UILabel alloc] init];
    [book_language_txt setTranslatesAutoresizingMaskIntoConstraints:NO];
    NSString *lang;
    if ([[self.bookDetails objectForKey:@"Language"] intValue] == 1)
        lang = @"العربية";
    [book_language_txt setText:lang];
    [book_language_txt setTextAlignment:[JSLocalizedString(@"Align", @"") intValue]];
    [book_language_txt setTextColor:[ColorTheme textGrey]];
    [book_language_txt setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [book_language_txt setBackgroundColor:[UIColor clearColor]];
    [book_language_txt setNumberOfLines:0];
    [book_language_txt setLineBreakMode:NSLineBreakByWordWrapping];
    [bookinfoView addSubview:book_language_txt];
    
    [book_update removeFromSuperview];
    book_update = [[UILabel alloc] init];
    [book_update setTranslatesAutoresizingMaskIntoConstraints:NO];
    [book_update setText:JSLocalizedString(@"Last Update", @"")];
    [book_update setTextAlignment:[JSLocalizedString(@"Align", @"") intValue]];
    [book_update setTextColor:[ColorTheme textGrey]];
    [book_update setFont:[FontManager mediumFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [book_update setBackgroundColor:[UIColor clearColor]];
    [bookinfoView addSubview:book_update];
    [book_update layoutIfNeeded];
    
    // Set date in the format: Aug 01 2015 05:48 AM
    dateTimeformatter=[[NSDateFormatter alloc]init];
    [dateTimeformatter setLocale:[NSLocale currentLocale]];
    [dateTimeformatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [dateTimeformatter setDateFormat:@"YYYY-MM-dd HH:mm:ss"];

    UILabel *book_update_txt = [[UILabel alloc] init];
    [book_update_txt setTranslatesAutoresizingMaskIntoConstraints:NO];
    [book_update_txt setText:[dateTimeformatter stringFromDate:[self deserializeJsonDateString:[_bookDetails objectForKey:@"PublishedDate"]]]];
    [book_update_txt setTextAlignment:[JSLocalizedString(@"Align", @"") intValue]];
    [book_update_txt setTextColor:[ColorTheme textGrey]];
    [book_update_txt setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [book_update_txt setBackgroundColor:[UIColor clearColor]];
    [book_update_txt setNumberOfLines:0];
    [book_update_txt setLineBreakMode:NSLineBreakByWordWrapping];
    [bookinfoView addSubview:book_update_txt];
    
    [summaryView removeFromSuperview];
    summaryView = [[UIView alloc] init];
    [summaryView setTranslatesAutoresizingMaskIntoConstraints:NO];
    [summaryView setBackgroundColor:[UIColor whiteColor]];
    [summaryView.layer setCornerRadius:7.0];
    [detailsView addSubview:summaryView];
    [summaryView layoutIfNeeded];

    [book_summary removeFromSuperview];
    book_summary = [[UILabel alloc] init];
    [book_summary setTranslatesAutoresizingMaskIntoConstraints:NO];
    [book_summary setText:JSLocalizedString(@"Summary", @"")];
    [book_summary setTextAlignment:[JSLocalizedString(@"Align", @"") intValue]];
    [book_summary setTextColor:[ColorTheme textGrey]];
    [book_summary setFont:[FontManager mediumFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]+2]];
    [book_summary setBackgroundColor:[UIColor clearColor]];
    [summaryView addSubview:book_summary];
    [book_summary layoutIfNeeded];
    
    [book_summary_txt removeFromSuperview];
    book_summary_txt = [[UILabel alloc] init];
    [book_summary_txt setTranslatesAutoresizingMaskIntoConstraints:NO];
    [book_summary_txt setText:[self.bookDetails objectForKey:@"Summary"]];
    [book_summary_txt setTextColor:[ColorTheme textGrey]];
    [book_summary_txt setTextAlignment:[JSLocalizedString(@"Align", @"") intValue]];
    [book_summary_txt setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [book_summary_txt setBackgroundColor:[UIColor clearColor]];
    [book_summary_txt setNumberOfLines:0];
    [book_summary_txt setLineBreakMode:NSLineBreakByWordWrapping];
    [book_summary_txt setClipsToBounds:YES];
    [summaryView addSubview:book_summary_txt];
    [book_summary_txt layoutIfNeeded];
    
    [detailsView startCanvasAnimation];
  
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    [dict setObject:self.view forKey:@"view"];
    [dict setObject:scroll_view forKey:@"scroll"];
    [dict setObject:detailsView forKey:@"detailV"];
    [dict setObject:book_image forKey:@"bimage"];
    [dict setObject:book_title forKey:@"btitle"];
    [dict setObject:book_writer forKey:@"bwriter"];
    [dict setObject:book_writer_txt forKey:@"bwritertxt"];
    [dict setObject:book_language forKey:@"blanguage"];
    [dict setObject:book_language_txt forKey:@"blanguagetxt"];
    [dict setObject:book_update forKey:@"bupdate"];
    [dict setObject:book_update_txt forKey:@"bupdatetxt"];
    [dict setObject:book_summary forKey:@"bsummary"];
    [dict setObject:book_summary_txt forKey:@"summary"];
    [dict setObject:self.open_Btn forKey:@"openBtn"];
    [dict setObject:self.delete_Btn forKey:@"deleteBtn"];
    [dict setObject:self.download_Btn forKey:@"downloadBtn"];
    [dict setObject:self.progressView forKey:@"progress"];
    [dict setObject:bookinfoView forKey:@"bookInfoV"];
    [dict setObject:summaryView forKey:@"summaryV"];
    
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[scroll(==view)]" options:0 metrics:nil views:dict]];
    [self.view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[scroll(==view)]" options:0 metrics:nil views:dict]];

    [scroll_view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[detailV(==scroll)]" options:0 metrics:nil views:dict]];
    [scroll_view addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[detailV(==scroll)]" options:0 metrics:nil views:dict]];
    
    [detailsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[bimage]" options:0 metrics:dict views:dict]];
    [detailsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[bimage]-0-|" options:0 metrics:dict views:dict]];
    [detailsView addConstraint:[NSLayoutConstraint constraintWithItem:book_image attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:book_image attribute:NSLayoutAttributeHeight multiplier:1.0 constant:0.0]];

    [detailsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[btitle]-10-|" options:0 metrics:nil views:dict]];
    [detailsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[bimage]-30-[btitle]" options:0 metrics:nil views:dict]];

    if (self.bookDownloaded == YES)
    {
        [self.download_Btn setHidden:YES];
        [self.open_Btn setHidden:NO];
        [self.delete_Btn setHidden:NO];

        float open_constant = self.open_Btn.imageView.image.size.width/2 + 5;
        float delete_constant = self.delete_Btn.imageView.image.size.width/2 + 5;

        [detailsView addConstraint:[NSLayoutConstraint constraintWithItem:self.open_Btn attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:detailsView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:open_constant]];
        [detailsView addConstraint:[NSLayoutConstraint constraintWithItem:self.delete_Btn attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:detailsView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:-delete_constant]];
        
        [detailsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[btitle]-30-[openBtn]" options:0 metrics:nil views:dict]];
        [detailsView addConstraint:[NSLayoutConstraint constraintWithItem:self.delete_Btn attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:self.open_Btn attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];
    }
    else if (self.bookDownloaded == NO)
    {
        [detailsView addConstraint:[NSLayoutConstraint constraintWithItem:self.download_Btn attribute:NSLayoutAttributeCenterX relatedBy:NSLayoutRelationEqual toItem:detailsView attribute:NSLayoutAttributeCenterX multiplier:1.0 constant:0.0]];
        [detailsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[btitle]-30-[downloadBtn]" options:0 metrics:nil views:dict]];
        
        [self.download_Btn setHidden:NO];
        [self.open_Btn setHidden:YES];
        [self.delete_Btn setHidden:YES];
    }

    
    [detailsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[bookInfoV]-20-|" options:0 metrics:nil views:dict]];
    if ([self.open_Btn isHidden])
        [detailsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[downloadBtn]-20-[bookInfoV]" options:0 metrics:nil views:dict]];
    else
        [detailsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[openBtn]-20-[bookInfoV]" options:0 metrics:nil views:dict]];

    [detailsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-30-[progress]-30-|" options:0 metrics:nil views:dict]];
    [detailsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[btitle]-35-[progress(3)]" options:0 metrics:nil views:dict]];
    
    [detailsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[summaryV]-20-|" options:0 metrics:nil views:dict]];
    [detailsView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[bookInfoV]-20-[summaryV]" options:0 metrics:nil views:dict]];
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"en"])
    {
        [bookinfoView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[bwriter]" options:0 metrics:nil views:dict]];
        [bookinfoView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[bwriter]-5-[bwritertxt]" options:0 metrics:nil views:dict]];
        [bookinfoView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[blanguage]" options:0 metrics:nil views:dict]];
        [bookinfoView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[blanguage]-5-[blanguagetxt]" options:0 metrics:nil views:dict]];
        [bookinfoView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[bupdate]" options:0 metrics:nil views:dict]];
        [bookinfoView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[bupdate]-5-[bupdatetxt]" options:0 metrics:nil views:dict]];
        [summaryView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[bsummary]" options:0 metrics:nil views:dict]];
    }
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"ar"])
    {
        [bookinfoView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[bwriter]-20-|" options:0 metrics:nil views:dict]];
        [bookinfoView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[bwritertxt]-5-[bwriter]" options:0 metrics:nil views:dict]];

        [bookinfoView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[blanguage]-20-|" options:0 metrics:nil views:dict]];
        [bookinfoView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[blanguagetxt]-5-[blanguage]" options:0 metrics:nil views:dict]];

        [bookinfoView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[bupdate]-20-|" options:0 metrics:nil views:dict]];
        [bookinfoView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[bupdatetxt]-10-[bupdate]" options:0 metrics:nil views:dict]];
        [summaryView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:[bsummary]-20-|" options:0 metrics:nil views:dict]];
    }


    [bookinfoView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[bwriter]" options:0 metrics:nil views:dict]];

    [bookinfoView addConstraint:[NSLayoutConstraint constraintWithItem:book_writer_txt attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:book_writer attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];

    [bookinfoView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[bwritertxt]-10-[blanguage]" options:0 metrics:nil views:dict]];
    
    [bookinfoView addConstraint:[NSLayoutConstraint constraintWithItem:book_language_txt attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:book_language attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];

    [bookinfoView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[blanguagetxt]-10-[bupdate]" options:0 metrics:nil views:dict]];
    
    [bookinfoView addConstraint:[NSLayoutConstraint constraintWithItem:book_update_txt attribute:NSLayoutAttributeCenterY relatedBy:NSLayoutRelationEqual toItem:book_update attribute:NSLayoutAttributeCenterY multiplier:1.0 constant:0.0]];

    [summaryView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-10-[bsummary]" options:0 metrics:nil views:dict]];

    [summaryView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-20-[summary]-20-|" options:0 metrics:nil views:dict]];
    [summaryView addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[bsummary]-10-[summary]" options:0 metrics:nil views:dict]];
}

-(void)viewDidLayoutSubviews
{
    
    if (self.updateUI == YES)
    {
        [super viewDidLayoutSubviews];

        float S_height = book_summary_txt.frame.origin.y + book_summary_txt.frame.size.height + 10;
        
        CGRect info_rect = bookinfoView.frame;
        float I_height = book_update.frame.origin.y + book_update.frame.size.height + 10;
        info_rect.size.height = I_height;
        bookinfoView.frame = info_rect;
        
        [summaryView setFrame:CGRectMake(summaryView.frame.origin.x, bookinfoView.frame.size.height + bookinfoView.frame.origin.y + 20, summaryView.frame.size.width, S_height)];
        
        //    // update scroll view content size after all subviews are loaded
        [scroll_view setContentSize:CGSizeMake([UIScreen mainScreen].bounds.size.width, summaryView.frame.size.height + summaryView.frame.origin.y + 10)];
    }
}

#pragma mark - Install Epub
#pragma mark -

// Function called when download button is pressed
-(IBAction)installEpub:(id)sender {

    if ([Appdelegate.reach currentReachabilityStatus] == NotReachable)
    {
        [Appdelegate hideLoadingIndicator];
        [Appdelegate showConnectionAlertWithView:self.view];
    }
    else
    {
        // call install epub from delegate
        if(!self.isDownloading && !self.isDownloaded)
        {
            self.bookIsDownloading = YES;
            [self.progressView setHidden:NO];
            [self.download_Btn setHidden:YES];
            [self.open_Btn setHidden:YES];
            [self.delete_Btn setHidden:YES];
            
            [self.progressView setProgress:0.0];
            
            [Appdelegate installEPubWithLink:[self.bookDetails objectForKey:@"EPUB"] andfilName:[NSString stringWithFormat:@"%@.epub",[self.bookDetails objectForKey:@"Code"]] andBookId:[self.bookDetails objectForKey:@"ID"] andIndex:nil andLastUpdateDate:[dateTimeformatter stringFromDate:[self.bookDetails objectForKey:@"PublishedDate"]] withType:3];
        }
        else if(self.isDownloaded) {
            self.bookIsDownloading = NO;
            [self openItem];
        }
    }
}

// Function called as a result of notification observer when book has finished downloading
-(void)bookDownloaded:(NSNotification *)dict
{
    self.bookIsDownloading = NO;
    self.bookDownloaded = YES;

    NSDictionary *userInfo = [dict userInfo];
    _bDetails = [userInfo objectForKey:@"book"];

    self.isDownloaded = YES;
    [self.progressView setHidden:YES];


    dispatch_async(dispatch_get_main_queue(), ^{
        [self loadInterface];
    });

    [self.download_Btn setHidden:YES];
    [self.delete_Btn setHidden:NO];
    [self.open_Btn setHidden:NO];
}

// Function to call open book with bookinformation sent
-(void)openItem {
    [self openBook:_bDetails];
}

// Update the download button's progress when you get a progress update from your item
-(void)downloadProgressed:(CGFloat)progress {
    [self.progressView setProgress:progress animated:YES];
}

// Check if book is downloading
-(BOOL)isDownloading {
    return self.fakeDownloadRequest.operationCount > 0;
}

// Alert user before proceeding with the book removal
-(void)deleteBook
{
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:JSLocalizedString(@"Delete", @"") andMessage:[NSString stringWithFormat:JSLocalizedString(@"Delete Message", @""), _bDetails.title]];
    
    [alertView addButtonWithTitle:JSLocalizedString(@"No", @"")
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alert) {
                              NSLog(@"Button1 Clicked");
                          }];
    [alertView addButtonWithTitle:JSLocalizedString(@"Yes", @"")
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alert) {
                              NSLog(@"Button2 Clicked");
                              [Appdelegate deleteBookByCode:_bDetails.identifier withName:_bDetails.title andFileName:_bDetails.fileName];
                          }];
   
    [alertView setTransitionStyle:SIAlertViewTransitionStyleFade];
    [alertView setMessageFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [alertView setTitleFont:[FontManager mediumFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]+2]];
    [alertView setMessageColor:[ColorTheme textGrey]];
    [alertView setTitleColor:[ColorTheme textGrey]];
    [alertView setButtonFont:[FontManager mediumFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [alertView setButtonColor:[ColorTheme slawLightRed]];
    [alertView show];
    
}

// Reload list if book was removed/deleted or downloading was cancelled (connection, app exit etc.)
-(void)reloadAfterDelete
{
    self.bookIsDownloading = NO;
    self.bookDownloaded = NO;

    [self.delete_Btn setHidden:YES];
    [self.open_Btn setHidden:YES];
    self.isDownloaded = NO;
    [self.download_Btn addTarget:self action:@selector(installEpub:) forControlEvents:UIControlEventTouchUpInside];
    
    dispatch_async(dispatch_get_main_queue(), ^{
        [self loadInterface];
    });
}

#pragma mark - Open Book
#pragma mark -

// Send bookinformation to bookviewcontroller
-(void)openBook:(BookInformation*)bi {
    
    self.updateUI = NO;
    
    [Appdelegate showLoadingIndicatorWithView:self.view andText:JSLocalizedString(@"Loading Book", @"")];
    
    @autoreleasepool {
        
        BookViewController *bvc = [[BookViewController alloc]init];
        bvc.bookInformation = bi;
        [self presentViewController:bvc animated:YES completion:nil];
    }
}


#pragma mark - Deserialize Json DateString
#pragma mark

// Function to handle JSON dates sent by the server and convert them to NSDate format
-(NSDate *)deserializeJsonDateString: (NSString *)jsonDateString
{
    if (jsonDateString != (id)[NSNull null])
    {
        NSInteger offset = [[NSTimeZone defaultTimeZone] secondsFromGMT]; //get number of seconds to add or subtract according to the client default time zone
        
        NSInteger startPosition = [jsonDateString rangeOfString:@"("].location + 1; //start of the date value
        
        NSTimeInterval unixTime = [[jsonDateString substringWithRange:NSMakeRange(startPosition, 13)] doubleValue] / 1000; //WCF will send 13 digit-long value for the time interval since 1970 (millisecond precision) whereas iOS works with 10 digit-long values (second precision), hence the divide by 1000
        
        NSDate *date = [[NSDate dateWithTimeIntervalSince1970:unixTime] dateByAddingTimeInterval:offset];
        
        return date;
    }
    
    return nil;
}

#pragma mark - Reachability
#pragma mark

// Handle connection reachability and change
-(void)reachabilityChanged:(NSNotification *)notification {
    
    Reachability *reach = [notification object];
    if (reach.currentReachabilityStatus == NotReachable)
    {
        // if book is downloading and connection is not reachable: stop downloading, remove book and cover image
        if (self.isDownloading == YES || self.bookIsDownloading == YES)
        {
            self.bookIsDownloading = NO;
            [Appdelegate deleteBookByCode:[self.bookDetails objectForKey:@"ID"] withName:[self.bookDetails objectForKey:@"Title"] andFileName:[[self.bookDetails objectForKey:@"Code"] stringByAppendingString:@".epub"]];
            [self reloadAfterDelete];
        }
    }
}

#pragma mark - Download Progress
#pragma mark

// Animate progressview with the download progress percentage
-(void) updateProgress:(NSNotification *)notification
{
    if ([Appdelegate.reach currentReachabilityStatus] == NotReachable)
    {
//        [self.downloadButton reset];
        
        [Appdelegate hideLoadingIndicator];
        [Appdelegate showConnectionAlertWithView:self.view];
    }
    else
    {
        dispatch_async(dispatch_get_main_queue(), ^{
            [self.progressView setProgress:[[[notification userInfo] objectForKey:@"progress"] fractionCompleted] animated:YES];
        });
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
