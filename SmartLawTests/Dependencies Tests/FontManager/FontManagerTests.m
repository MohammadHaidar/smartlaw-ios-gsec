//
//  FontManagerTests.m
//  SmartLaw
//
//  Created by Krystel Chaccour on 6/15/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "FontManager.h"

@interface FontManagerTests : XCTestCase

@end

@implementation FontManagerTests

- (void)setUp {
    [super setUp];
    // Put setup code here. This method is called before the invocation of each test method in the class.
}

- (void)tearDown {
    // Put teardown code here. This method is called after the invocation of each test method in the class.
    [super tearDown];
}

- (void)testRegularFontOfSize {
    
    XCTAssertNotNil([FontManager regularFontOfSize:15], @"Regular font should not be nil");
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"en"])
        XCTAssertEqualObjects([FontManager regularFontOfSize:15], [UIFont fontWithName:@"CorporateS-Regular" size:15], @"Regular Font != CorporateS-Regular");
    else
        XCTAssertEqualObjects([FontManager regularFontOfSize:15], [UIFont fontWithName:@"GESSTextLight-Light" size:15], @"Regular Font != GESSTextLight-Light");
}

- (void)testBoldFontOfSize {
    
    XCTAssertNotNil([FontManager boldFontOfSize:15], @"Bold font should not be nil");
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"en"])
        XCTAssertEqualObjects([FontManager boldFontOfSize:15], [UIFont fontWithName:@"CorporateS-Bold" size:15], @"Bold Font != CorporateS-Bold");
    else
        XCTAssertEqualObjects([FontManager boldFontOfSize:15], [UIFont fontWithName:@"GESSTextBold-Bold" size:15], @"Bold Font != GESSTextBold-Bold");

}

- (void)testMediumFontOfSize {
    
    XCTAssertNotNil([FontManager mediumFontOfSize:15], @"Medium font should not be nil");
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"en"])
        XCTAssertEqualObjects([FontManager mediumFontOfSize:15], [UIFont fontWithName:@"CorporateS-Bold" size:15], @"Medium Font != CorporateS-Bold");
    else
        XCTAssertEqualObjects([FontManager mediumFontOfSize:15], [UIFont fontWithName:@"GESSTextMedium-Medium" size:15], @"Bold Font != GESSTextMedium-Medium");

}

- (void)testLightFontOfSize {
    
    XCTAssertNotNil([FontManager lightFontOfSize:15], @"Medium font should not be nil");
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"en"])
        XCTAssertEqualObjects([FontManager lightFontOfSize:15], [UIFont fontWithName:@"CorporateS-Light" size:15], @"Light Font != CorporateS-Light");
    else
        XCTAssertEqualObjects([FontManager lightFontOfSize:15], [UIFont fontWithName:@"GESSTextLight-Light" size:15], @"Light Font != GESSTextLight-Light");

}

@end
