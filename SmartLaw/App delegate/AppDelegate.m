//
//  AppDelegate.m
//  SmartLaw
//
//  Created by Krystel Chaccour on 3/1/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import "AppDelegate.h"
#import "LandingPage.h"
#include <sys/time.h>
#import <CommonCrypto/CommonDigest.h>
#import <sys/xattr.h>
#import <AFNetworking/AFNetworking.h>
#import <KSToastView/KSToastView.h>
#import <SSZipArchive/SSZipArchive.h>
#import "RowObject.h"

#import <AFNetworking/AFNetworking.h>//add to the header of class


@interface AppDelegate ()
{
    RTSpinKitView *spinner;
    MBProgressHUD *hud;
    NSMutableArray *structuredArray;
}
@property (nonatomic, retain) IBOutlet UIProgressView *progressView;

@end

@implementation AppDelegate
@synthesize window = _window;
@synthesize isRotationLocked;
@synthesize booksListArray;

-(BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    
     [NSThread sleepForTimeInterval:3]; //add 5 seconds longer.
    
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"mode"] == NULL)
        [[NSUserDefaults standardUserDefaults] setObject:@"on" forKey:@"mode"];
    
    // Override point for customization after application launch.
    
    if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"en"])
        JSSetLanguage(@"en");
    else if ([[[NSUserDefaults standardUserDefaults] objectForKey:@"language"] isEqualToString:@"ar"])
        JSSetLanguage(@"ar");
    else
    {
        [[NSUserDefaults standardUserDefaults] setObject:@"ar" forKey:@"language"];
        JSSetLanguage(@"ar");
    }

    [[NSUserDefaults standardUserDefaults] synchronize];

    [[UITextView appearance] setTintColor:[ColorTheme textGrey]];
    [[UITextField appearance] setTintColor:[ColorTheme textGrey]];
        
    // Allocate a reachability object
    self.reach = [Reachability reachabilityWithHostName:@"www.google.com"];
    // Start the notifier, which will cause the reachability object to retain itself!
    [self.reach startNotifier];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object: nil];
    
    [[UIView appearance] setTintColor:[ColorTheme slawRed]];
    
    int cacheSizeMemory = 4*1024*1024; // 4MB
    int cacheSizeDisk = 32*1024*1024; // 32MB
    cacheSizeMemory  = 0;
    cacheSizeDisk = 0;
    NSURLCache *sharedCache = [[NSURLCache alloc] initWithMemoryCapacity:cacheSizeMemory diskCapacity:cacheSizeDisk diskPath:@"nsurlcache"];
    [NSURLCache setSharedURLCache:sharedCache];
    
    NSMutableDictionary *titleBarAttributes = [NSMutableDictionary dictionaryWithDictionary: [[UINavigationBar appearance] titleTextAttributes]];
    [titleBarAttributes setValue:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]+4] forKey:NSFontAttributeName];
    [[UINavigationBar appearance] setTitleTextAttributes:titleBarAttributes];
    
    // View controllers for tab bar controller
    LandingPage *landingPage = [[LandingPage alloc] init];
    landingPage.view.backgroundColor = [UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]];
    UINavigationController *nc1 = [[UINavigationController alloc] initWithRootViewController:landingPage];
    [nc1.navigationBar setBarTintColor:[ColorTheme barBlack]];
    [nc1.navigationBar setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}];
    [nc1.navigationBar setTintColor:[UIColor whiteColor]];
    if([UIDevice currentDevice].systemVersion.floatValue >= 8.0)
        [[UINavigationBar appearance] setTranslucent:NO];
 
    self.window = [[UIWindow alloc] init];
    [self.window setFrame:[[UIScreen mainScreen]bounds]];
    [self.window setRootViewController:nc1];
    [self.window makeKeyAndVisible];
    
    return YES;
}


#pragma mark - Retrieve Favorites
#pragma mark -

/**
 Retrieve list of favorites laws saved locally in 'Favorites.plist'

 @return array of favorites ids
 */
-(NSArray *)retrieveFavorites
{
    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsPath = [paths objectAtIndex:0];
    NSString *plistPath = [documentsPath stringByAppendingPathComponent:@"Favorites.plist"];
    
    if (![[NSFileManager defaultManager] fileExistsAtPath:plistPath])
    {
        plistPath = [[NSBundle mainBundle] pathForResource:@"Favorites" ofType:@"plist"];
    }
    
    self.favorites_list = [NSArray arrayWithContentsOfFile:plistPath];
    
    return self.favorites_list;
}

#pragma mark - Check functions
#pragma mark -

/**
 Check if device id iPad or iPhone

 */
-(BOOL)isPad {
    if(UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad) {
        return YES;
    }else {
        return NO;
    }
}

/**
 Check if device ios verion is above 5.0
 */
-(BOOL)isAbove5 {
    // A system version of 3.1 or greater is required to use CADisplayLink. The NSTimer
    // class is used as fallback when it isn't available.
    NSString *reqSysVer = @"5.0";
    NSString *currSysVer = [[UIDevice currentDevice] systemVersion];
    if ([currSysVer compare:reqSysVer options:NSNumericSearch] != NSOrderedAscending) {
        return YES;
    }else {
        return NO;
    }
}

#pragma mark - Loading
#pragma mark -

-(void)showLoadingIndicatorWithView:(UIView *)on_view andText:(NSString *)loadText
{
    [spinner stopAnimating];
    spinner = [[RTSpinKitView alloc] initWithStyle:RTSpinKitViewStyleArcAlt color:[UIColor whiteColor]];
    
    hud = [MBProgressHUD showHUDAddedTo:on_view animated:YES];
    hud.square = NO;
    hud.mode = MBProgressHUDModeCustomView;
    hud.customView = spinner;
    hud.label.text = loadText;
    hud.label.font = [FontManager mediumFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]];
    [spinner startAnimating];
}

-(void)hideLoadingIndicator
{
    [spinner stopAnimating];
    [hud removeFromSuperview];
}

#pragma mark - Reachability
#pragma mark

/*
 ** Handle connection reachability and change
 */

-(void)reachabilityChanged:(NSNotification *)notification {
    
    self.reach = [notification object];
    if (self.reach.currentReachabilityStatus == NotReachable)
        [self showConnectionAlertWithView:self.window];
}

/*
 ** Show toast message when connection is lost, automatically hides after a specific duration (here 4.0)
 */
-(void)showConnectionAlertWithView:(UIView *)on_View
{
    [KSToastView ks_setAppearanceMaxLines:0];
    [KSToastView ks_setAppearanceMaxWidth:[UIScreen mainScreen].bounds.size.width - 60];
    [KSToastView ks_setAppearanceTextFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]+2]];
    [KSToastView ks_setAppearanceTextColor:[UIColor whiteColor]];
    [KSToastView ks_setAppearanceTextAligment:NSTextAlignmentCenter];
    [KSToastView ks_setAppearanceBackgroundColor:[ColorTheme textGrey]];
    [KSToastView ks_setAppearanceCornerRadius:5.0];
    [KSToastView ks_setAppearanceOffsetBottom:60.0];
    [KSToastView ks_setAppearanceTextInsets:UIEdgeInsetsMake(5, 5, 5, 5)];
    [KSToastView ks_showToast:JSLocalizedString(@"Connection", @"") duration:4.0 delay:0.5];
}

#pragma mark - Download Offline
#pragma mark -

/**
 Function that gets called when the user switched to offline mode and downloads content
 */
-(void)downloadOfflineContent
{
    // check if the internet connection is valid
    if ([Appdelegate.reach currentReachabilityStatus] == NotReachable)
    {
        [Appdelegate hideLoadingIndicator];
        [Appdelegate showConnectionAlertWithView:self.window];
    }
    else
    {        
        TacoShell *ts = [[TacoShell alloc] initWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@/GetMobileContentOffline",stagingURL]]];
        ts.method = BKTSRequestMethodPOST;
        ts.contentType = BKTSRequestContentTypeApplicationJSON;
        
        ts.completionBlock = ^(NSDictionary *dictionary, NSInteger httpResponseStatusCode, id response){
            
            if ([[[[response objectForKey:@"d"] objectForKey:@"Status"] objectForKey:@"StatusCode"] intValue] == 0)
            {
                //download the file in a seperate thread.
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
                    NSLog(@"Downloading Started");
                    
                    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"LastUpdated"])
                    {
                        if (![[[NSUserDefaults standardUserDefaults] objectForKey:@"LastUpdated"] isEqualToString:[[response objectForKey:@"d"] objectForKey:@"lastUpdated"]])
                            [self updateCheckWithURL:[[response objectForKey:@"d"] objectForKey:@"zipURL"]];
                        else
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"updateToast" object:nil userInfo:nil];
                    }
                    else
                    {
                        [[NSNotificationCenter defaultCenter] postNotificationName:@"startProgress" object:nil userInfo:nil];
                        [self downloadZipWithUrlStr:[[response objectForKey:@"d"] objectForKey:@"zipURL"]];

                    }
                    
                    [[NSUserDefaults standardUserDefaults] setObject:[[response objectForKey:@"d"] objectForKey:@"lastUpdated"] forKey:@"LastUpdated"];

                });
            }
        };
        
        ts.errorBlock = ^(NSError *error){
            [self hideLoadingIndicator];
            if (error)
            {
//                [self.spinner stopAnimating];
                
                [self hideLoadingIndicator];
//                [Appdelegate showConnectionAlertWithView:self.view];
                NSLog(@"error: %@", error);
            }
        };
        
        [ts start];
    }
}

/**
 Check for updated content

 @param zipURL URL to download the new zip folder
 */
-(void)updateCheckWithURL:(NSString *)zipURL
{
    SIAlertView *alertView = [[SIAlertView alloc] initWithTitle:@"" andMessage:JSLocalizedString(@"Switch Update", @"")];
    
    [alertView addButtonWithTitle:JSLocalizedString(@"No", @"")
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alert) {
                              NSLog(@"Button1 Clicked");
                          }];
    [alertView addButtonWithTitle:JSLocalizedString(@"Yes", @"")
                             type:SIAlertViewButtonTypeDefault
                          handler:^(SIAlertView *alert) {
                              NSLog(@"Button2 Clicked");
//                              [self hideLoadingIndicator];
                              [[NSNotificationCenter defaultCenter] postNotificationName:@"startProgress" object:nil userInfo:nil];
                              [self downloadZipWithUrlStr:zipURL];
                          }];
    [alertView setShowProgress:NO];
    [alertView setTransitionStyle:SIAlertViewTransitionStyleFade];
    [alertView setMessageFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [alertView setTitleFont:[FontManager mediumFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]+2]];
    [alertView setMessageColor:[ColorTheme textGrey]];
    [alertView setTitleColor:[ColorTheme textGrey]];
    [alertView setButtonFont:[FontManager mediumFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [alertView setButtonColor:[ColorTheme slawLightRed]];
    [alertView show];

}


#pragma mark - Deserialize Json DateString
#pragma mark

/*
 ** Function to handle JSON dates sent by the server and convert them to NSDate format
 */
- (NSDate *)deserializeJsonDateString: (NSString *)jsonDateString
{
    if (jsonDateString != (id)[NSNull null])
    {
        NSInteger offset = [[NSTimeZone defaultTimeZone] secondsFromGMT]; //get number of seconds to add or subtract according to the client default time zone
        
        NSInteger startPosition = [jsonDateString rangeOfString:@"("].location + 1; //start of the date value
        
        NSTimeInterval unixTime = [[jsonDateString substringWithRange:NSMakeRange(startPosition, 13)] doubleValue] / 1000; //WCF will send 13 digit-long value for the time interval since 1970 (millisecond precision) whereas iOS works with 10 digit-long values (second precision), hence the divide by 1000
        
        NSDate *date = [[NSDate dateWithTimeIntervalSince1970:unixTime] dateByAddingTimeInterval:offset];
        
        return date;
    }
    
    return nil;
}


#pragma mark - Download Zip File
#pragma mark -

/**
 Download zip folder from URL to local directory

 @param urlStr download Url string
 */
-(void)downloadZipWithUrlStr:(NSString *)urlStr {
    AFHTTPSessionManager *manager = [[AFHTTPSessionManager alloc] initWithBaseURL:[NSURL URLWithString:htmlURL]];
    
    NSString *url = [NSString stringWithFormat:@"%@%@", htmlURL, urlStr];

    NSString *path =  [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
    
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:[NSURL URLWithString:url] cachePolicy:NSURLRequestUseProtocolCachePolicy  timeoutInterval:0];
    
    NSURLSessionDownloadTask *task = [manager downloadTaskWithRequest:request progress:^(NSProgress * _Nonnull downloadProgress) {
            NSDictionary *dict = [NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:downloadProgress.fractionCompleted]
                                                             forKey:@"progress"];
            [[NSNotificationCenter defaultCenter] postNotificationName:@"downloadProgress"
                                                                object:nil
                                                              userInfo:dict];
    } destination:^NSURL * _Nonnull(NSURL * _Nonnull targetPath, NSURLResponse * _Nonnull response) {
        return [NSURL fileURLWithPath:[path stringByAppendingPathComponent:response.suggestedFilename]];
    } completionHandler:^(NSURLResponse * _Nonnull response, NSURL * _Nullable filePath, NSError * _Nullable error) {
        if (error) {
            NSLog(@"errorReason:%@",error.localizedDescription);
        }else
        {
            NSString *dPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
            [self unZipWithFilePath:filePath destination:dPath];
        }
    }];
    [task resume];
}

#pragma mark - Handle Zip file
#pragma mark -

/**
 Unzip downloaded folder and retrieve the files inside
 */
- (void)unZipWithFilePath:(NSURL *)filePath destination: (NSString *)destination
{
    NSString *input = [filePath.absoluteString stringByReplacingOccurrencesOfString:@"file://" withString:@""];
    NSLog(@"Output: %@, Write: %@",input,destination);
    [self delOlderVersionInPath];
    
    BOOL success = [SSZipArchive unzipFileAtPath:input toDestination:destination];
    if (success)
    {
        NSLog(@"targetPath:%@",destination);
        
        [self createDatabase];

        NSError *error = nil;
        NSMutableArray<NSString *> *items = [[[NSFileManager defaultManager]
                                              contentsOfDirectoryAtPath:destination
                                              error:&error] mutableCopy];
        
        if (error) {
            return;
        }
        
        [items enumerateObjectsUsingBlock:^(NSString * _Nonnull obj, NSUInteger idx, BOOL * _Nonnull stop) {
            switch (idx) {
                case 0: {
                    
                    NSError *error = nil;
                    
                    NSMutableArray<NSString *> *items = [[[NSFileManager defaultManager]
                                                          contentsOfDirectoryAtPath:[destination stringByAppendingString:@"/HTMLFiles"]
                                                          error:&error] mutableCopy];
                    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
                    NSString *indexPath = [[[paths objectAtIndex:0] stringByAppendingString:@"/HTMLFiles/"] stringByAppendingString:[items objectAtIndex:1]];
                    NSString *content = [NSString stringWithContentsOfFile:indexPath encoding:NSUTF8StringEncoding error:nil];
                }
                    break;
                case 1: {
                    NSError *error = nil;
                    
                    NSArray *paths = NSSearchPathForDirectoriesInDomains (NSDocumentDirectory, NSUserDomainMask, YES);
                    
                    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingString:@"/SmartLawOffline.json"];
                    
                    // Load the file into an NSData object called JSONData
                    
                    NSData *JSONData = [NSData dataWithContentsOfFile:filePath options:NSDataReadingMappedIfSafe error:&error];
                    
                    // Create an Objective-C object from JSON Data
                    
                    id JSONObject = [NSJSONSerialization
                                     JSONObjectWithData:JSONData
                                     options:NSJSONReadingAllowFragments
                                     error:&error];
                    structuredArray = [NSMutableArray array];
                    
                    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{

                        [self structureArrayOfContent:JSONObject];

                    });
                }
                    break;
                default: {
                    NSLog(@"Went beyond index of assumed files");
                    break;
                }
            }
        }];
    }else
        NSLog(@"Decompression failed");
    
    [self delZipWithFilePath:input];
    
    NSDictionary *dict = [NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:1] forKey:@"progress"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"downloadProgress"
                                                        object:nil
                                                      userInfo:dict];
}

/**
 Check and remove any older folders from documents
 */
- (void)delOlderVersionInPath {
    
    NSString *dPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    
    NSString *filePath =[[NSString alloc] initWithFormat:@"%@/",dPath];
    if ([fileManager removeItemAtPath:filePath error:NULL]) {
        NSLog(@"Deleting the extracted file was successful");
    }else{
        NSLog(@"Deleting unzipped file failed");
    }
}

/**
 Delete/remove the zip file downloaded from Cache folder
 */
- (void)delZipWithFilePath :(NSString *)path
{
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if ([fileManager removeItemAtPath:path error:NULL]) {
        NSLog(@"Delete ZIP successfully");
    }else{
        NSLog(@"Deleting ZIP failed");
    }
}


#pragma mark - Retract from Json file
#pragma mark -

float downloadProgress = 0;

/**
 Loop through the json file from the downloaded offline folder and insert them respectively to the Database
 */
-(void)structureArrayOfContent:(NSArray *)array
{
    for (id arrayOfContents in array)
    {
        downloadProgress++;
        
        if (arrayOfContents)
        {
            NSArray *keys = [arrayOfContents allKeys];
            NSArray *values = [arrayOfContents allValues];
            NSMutableDictionary *driverI = [NSMutableDictionary dictionary];
            
            for (int j=0; j<[values count];j++)
            {
                if (![[keys objectAtIndex:j] isEqualToString:@"ChildNodes"])
                {
                    [driverI setObject:[values objectAtIndex:j] forKey:[keys objectAtIndex:j]];
                }
            }
            
                [self insertIntoTableFromJSON:driverI];
            }
        
        if ([[arrayOfContents valueForKey:@"ChildNodes"] count] > 0)
            [self structureArrayOfContent:[arrayOfContents valueForKey:@"ChildNodes"]];
    }
    
    NSDictionary *dict = [NSDictionary dictionaryWithObject:[NSNumber numberWithFloat:(downloadProgress*1)/6023]
                                                     forKey:@"progress"];
    [[NSNotificationCenter defaultCenter] postNotificationName:@"downloadProgress"
                                                        object:nil
                                                      userInfo:dict];
}


#pragma mark - Prevent Backup to iCloud
#pragma mark -

/**
 These functions will prevent Documents folder from being backuped by iCloud.
 */

-(void)preventDocumentsBackup {
    NSString *documentPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) lastObject];
    NSURL *pathURL= [NSURL fileURLWithPath:documentPath];
    NSLog(@"%@",documentPath);
    
    if([[[UIDevice currentDevice] systemVersion] floatValue] > 5.0f){
        [self addSkipBackupAttributeToItemAtURL:pathURL];
    }else{
        NSLog(@"Failed because version is under 5.0.1");
    }
}

-(BOOL)addSkipBackupAttributeToItemAtURL:(NSURL *)URL {
    NSLog(@"iCloud will not backup %@.",[URL absoluteString]);
    const char* filePath = [[URL path] fileSystemRepresentation];
    
    const char* attrName = "com.apple.MobileBackup";
    u_int8_t attrValue = 1;
    
    int result = setxattr(filePath, attrName, &attrValue, sizeof(attrValue), 0, 0);
    return result == 0;
}

#pragma mark - Database Functions
#pragma mark -

/**
 Create the databse to store all the laws offline
 */
-(void) createDatabase
{
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    NSFileManager *fileManager = [NSFileManager defaultManager];
    [fileManager removeItemAtPath:[documentsDir stringByAppendingPathComponent:@"SmartLaw.sqlite"] error:NULL];

    NSString *dbPath = [documentsDir stringByAppendingPathComponent:@"SmartLaw.sqlite"];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    [database executeUpdate:@"CREATE VIRTUAL TABLE IF NOT EXISTS TRecord USING fts4(ID INTEGER  PRIMARY KEY DEFAULT NULL, Name TEXT DEFAULT NULL, NodeType INTEGER DEFAULT NULL, Selectable BOOL DEFAULT NULL, HTMLLink TEXT DEFAULT NULL, HTMLTextLink TEXT DEFAULT NULL, ChildNodesCount INTEGER DEFAULT NULL, ParentID INTEGER DEFAULT NULL, DocumentID INTEGER DEFAULT NULL, DocumentTypeID INTEGER DEFAULT NULL)"];
    [database close];
}

/**
 Retrieve the list of laws from database

 @param conditionStr parent id or document id
 @param docType document type integer
 @return array of list of laws retrieved
 */
-(NSArray *) retrieveFromTableWithCondition:(NSString *)conditionStr andDocumentType:(NSInteger)docType
{
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    NSString *dbPath = [documentsDir   stringByAppendingPathComponent:@"SmartLaw.sqlite"];
    NSMutableArray *allDetails = [NSMutableArray array];
    booksListArray = [NSMutableArray array];

    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
  
    NSString* condition = @"";
    
    if (docType == -1)
    {
        if (!(conditionStr == NULL || [conditionStr length] == 0))
            condition = [NSString stringWithFormat:@"WHERE DocumentID IN (%@) and NodeType = 2",conditionStr];
    }
    else
    {
        if ([conditionStr isKindOfClass:[NSString class]])
        {
            if (!(conditionStr == NULL || [conditionStr length] == 0)) {
                condition = [NSString stringWithFormat:@"WHERE ParentID LIKE '%@' and DocumentTypeID Like '%ld'", conditionStr, (long)docType];
            }
        }
        else
        {
            if (conditionStr > 0) {
                condition = [NSString stringWithFormat:@"WHERE ParentID LIKE '%@' and DocumentTypeID Like '%ld'", (NSString *)conditionStr, (long)docType];
            }
            
        }
    }
    
    NSString* baseSql = @"SELECT * FROM TRecord";
    NSString* sql = [NSString stringWithFormat:@"%@ %@",baseSql,condition];
    
    FMResultSet *results = [database executeQuery:sql];
    
    if(results)
    {
        NSArray *keys, *values;
        NSMutableDictionary *content;
        NSArray *children = [NSArray array];
        
        while([results next])
        {
            keys = [[results resultDictionary] allKeys];
            values = [[results resultDictionary] allValues];
            content = [NSMutableDictionary dictionary];
            
            for (int j=0; j<[values count];j++)
            {
                [content setObject:[values objectAtIndex:j] forKey:[keys objectAtIndex:j]];
            }
            [allDetails addObject:content];
        }
        
        NSString *fileName, *fileId, *masterId, *fileParentId, *htmlLink;
        NSInteger nodeType, childNodesCount;
        BOOL selectable;
        
        for (int d = 0; d < [allDetails count]; d++)
        {
            if ([[allDetails objectAtIndex:d] objectForKey:@"ID"] != (id)[NSNull null])
            {
                fileName = [[allDetails objectAtIndex:d] objectForKey:@"Name"];
                fileId = [[allDetails objectAtIndex:d] objectForKey:@"ID"];
                masterId = [[allDetails objectAtIndex:d] objectForKey:@"DocumentID"];
                fileParentId = [[allDetails objectAtIndex:d] objectForKey:@"ParentID"];
                htmlLink = [[allDetails objectAtIndex:d] objectForKey:@"HTMLLink"];
                nodeType = [[[allDetails objectAtIndex:d] objectForKey:@"NodeType"] integerValue];
                if (docType == -1)
                    childNodesCount = 0;
                else
                    childNodesCount = [[[allDetails objectAtIndex:d] objectForKey:@"ChildNodesCount"] integerValue];
                selectable = [[[allDetails objectAtIndex:d] objectForKey:@"Selectable"] boolValue];
                
                [booksListArray addObject:[RowObject fileWithName:fileName fileId:fileId masterId:masterId parentId:fileParentId nodeType:nodeType selectable:selectable childNodesCount:childNodesCount childNodes:children htmlLink:htmlLink expand:NO isChild:NO isParent:YES depth:0]];
            }
        }
    }
    
    [database close];
    
    return booksListArray;
}

/**
 Retrieve list of laws that act as children

 @param conditionStr array (converted to string) or string of parent ids
 @param newDepth level of depth
 @return array of children for a specific law
 */
-(NSArray *) retrieveChildrenFromTableWithCondition:(NSString *)conditionStr andDepth:(NSInteger)newDepth
{
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    NSString *dbPath = [documentsDir   stringByAppendingPathComponent:@"SmartLaw.sqlite"];
    NSMutableArray *allDetails = [NSMutableArray array];
    NSMutableArray *children = [NSMutableArray array];
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    NSString* condition = @"";
    if ([conditionStr isKindOfClass:[NSString class]])
    {
        if (!(conditionStr == NULL || [conditionStr length] == 0)) {
            //        condition = [NSString stringWithFormat:@"WHERE ID IN (%@)",conditionStr];
            condition = [NSString stringWithFormat:@"WHERE ParentID LIKE '%@'", conditionStr];
        }
    }
    else
    {
        if (conditionStr > 0) {
            //        condition = [NSString stringWithFormat:@"WHERE ID IN (%@)",conditionStr];
            condition = [NSString stringWithFormat:@"WHERE ParentID LIKE '%@'", (NSString *)conditionStr];
        }
        
    }
    
    NSString* baseSql = @"SELECT * FROM TRecord";
    NSString* sql = [NSString stringWithFormat:@"%@ %@",baseSql,condition];
    
    FMResultSet *results = [database executeQuery:sql];
    
    if(results)
    {
        NSArray *keys, *values;
        NSMutableDictionary *content;
        
        while([results next])
        {
            keys = [[results resultDictionary] allKeys];
            values = [[results resultDictionary] allValues];
            content = [NSMutableDictionary dictionary];
            
            for (int j=0; j<[values count];j++)
            {
                [content setObject:[values objectAtIndex:j] forKey:[keys objectAtIndex:j]];
            }
            [allDetails addObject:content];
        }
        
        NSString *fileName, *fileId, *masterId, *fileParentId, *htmlLink;
        NSInteger nodeType, childNodesCount;
        BOOL selectable;
        BOOL shouldExpand;
        
        for (int d = 0; d < [allDetails count]; d++)
        {
            if ([[allDetails objectAtIndex:d] objectForKey:@"ID"] != (id)[NSNull null])
            {
                fileName = [[allDetails objectAtIndex:d] objectForKey:@"Name"];
                fileId = [[allDetails objectAtIndex:d] objectForKey:@"ID"];
                masterId = [[allDetails objectAtIndex:d] objectForKey:@"DocumentID"];
                fileParentId = [[allDetails objectAtIndex:d] objectForKey:@"ParentID"];
                htmlLink = [[allDetails objectAtIndex:d] objectForKey:@"HTMLLink"];
                nodeType = [[[allDetails objectAtIndex:d] objectForKey:@"NodeType"] integerValue];
                childNodesCount = [[[allDetails objectAtIndex:d] objectForKey:@"ChildNodesCount"] integerValue];
                selectable = [[[allDetails objectAtIndex:d] objectForKey:@"Selectable"] boolValue];
               
                if ([[[allDetails objectAtIndex:d] objectForKey:@"ChildNodesCount"] integerValue] > 0 && [[[allDetails objectAtIndex:d] objectForKey:@"Selectable"] boolValue] == YES)
                    shouldExpand = YES;
                else shouldExpand = NO;
                                
                [children insertObject:[RowObject fileWithName:[[allDetails objectAtIndex:d] objectForKey:@"Name"] fileId:[[allDetails objectAtIndex:d] objectForKey:@"ID"] masterId:[[allDetails objectAtIndex:d] objectForKey:@"DocumentID"] parentId:[[allDetails objectAtIndex:d] objectForKey:@"ParentID"] nodeType:[[[allDetails objectAtIndex:d] objectForKey:@"NodeType"]integerValue] selectable:[[[allDetails objectAtIndex:d] objectForKey:@"Selectable"] boolValue] childNodesCount:[[[allDetails objectAtIndex:d] objectForKey:@"ChildNodesCount"]integerValue] childNodes:nil htmlLink:[[allDetails objectAtIndex:d] objectForKey:@"HTMLLink"] expand:NO isChild:YES isParent:NO depth:newDepth] atIndex:d];
            }
        }
    }
    
    [database close];
    
    return children;
}

/**
 Search the databse/table and returns an array of results

 @param keywords search string value
 @return array of results
 */
-(NSArray *)searchDatabaseWithKeywords:(NSString *)keywords
{    
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    NSString *dbPath = [documentsDir stringByAppendingPathComponent:@"SmartLaw.sqlite"];
    
    NSMutableArray *allDetails = [NSMutableArray array];
    booksListArray = [NSMutableArray array];

    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    NSString* sql = [NSString stringWithFormat:@"Select * FROM TRecord Where Name MATCH '%@' UNION Select * FROM TRecord Where HTMLTextLink MATCH '%@'", keywords, keywords];

    FMResultSet *results = [database executeQuery:sql];
    if(results)
    {
        NSArray *keys, *values;
        NSMutableDictionary *content;
        NSArray *children = [NSArray array];
        
        while([results next])
        {
            keys = [[results resultDictionary] allKeys];
            values = [[results resultDictionary] allValues];
            content = [NSMutableDictionary dictionary];
            
            for (int j=0; j<[values count];j++)
            {
                [content setObject:[values objectAtIndex:j] forKey:[keys objectAtIndex:j]];
            }
            [allDetails addObject:content];
        }
        
        NSString *fileName, *fileId, *masterId, *fileParentId, *htmlLink;
        NSInteger nodeType, childNodesCount;
        BOOL selectable;
        
        for (int d = 0; d < [allDetails count]; d++)
        {
            if ([[allDetails objectAtIndex:d] objectForKey:@"ID"] != (id)[NSNull null])
            {
                fileName = [[allDetails objectAtIndex:d] objectForKey:@"Name"];
                fileId = [[allDetails objectAtIndex:d] objectForKey:@"ID"];
                masterId = [[allDetails objectAtIndex:d] objectForKey:@"DocumentID"];
                fileParentId = [[allDetails objectAtIndex:d] objectForKey:@"ParentID"];
                htmlLink = [[allDetails objectAtIndex:d] objectForKey:@"HTMLLink"];
                nodeType = [[[allDetails objectAtIndex:d] objectForKey:@"NodeType"] integerValue];
                childNodesCount = 0;
//                    childNodesCount = [[[allDetails objectAtIndex:d] objectForKey:@"ChildNodesCount"] integerValue];
                selectable = [[[allDetails objectAtIndex:d] objectForKey:@"Selectable"] boolValue];
                
                [booksListArray addObject:[RowObject fileWithName:fileName fileId:fileId masterId:masterId parentId:fileParentId nodeType:nodeType selectable:selectable childNodesCount:childNodesCount childNodes:children htmlLink:htmlLink expand:NO isChild:NO isParent:YES depth:0]];
            }
        }
    }

    [database close];

    return booksListArray;
}

/**
 Insert content/law into database table

 @param content Contains the law content/info
 */
-(void) insertIntoTableFromJSON:(NSDictionary *)content
{
    NSArray *docPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDir = [docPaths objectAtIndex:0];
    NSString *dbPath = [documentsDir stringByAppendingPathComponent:@"SmartLaw.sqlite"];
    NSString *textFile;
    
    FMDatabase *database = [FMDatabase databaseWithPath:dbPath];
    [database open];
    
    if ([[content objectForKey:@"HTMLTextLink"] length] > 0)
        textFile = [NSString stringWithContentsOfFile: [[documentsDir stringByAppendingString:@"/SmartLaw"] stringByAppendingString:[content objectForKey:@"HTMLTextLink"]] encoding:NSUTF8StringEncoding error:nil];
    else
        textFile = [content objectForKey:@"HTMLTextLink"];
    
    [database executeUpdate:@"INSERT INTO TRecord (ID, Name, NodeType, Selectable, HTMLLink, HTMLTextLink, ChildNodesCount, ParentID, DocumentID, DocumentTypeID) VALUES (?,?,?,?,?,?,?,?,?,?)", [content objectForKey:@"ID"], [content objectForKey:@"Name"], [content objectForKey:@"NodeType"], [content objectForKey:@"Selectable"], [content objectForKey:@"HTMLLink"], textFile, [content objectForKey:@"ChildNodesCount"], [content objectForKey:@"ParentID"], [content objectForKey:@"DocumentID"], [content objectForKey:@"DocumentTypeID"], nil];
    [database close];
}

#pragma mark - Delegate Functions
#pragma mark -

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults synchronize];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults synchronize];
}

@end
