//
//  UIView+DBZAdditions.h
//  DubizzleHorizontal
//
//  Created by Robert Dimitrov on 4/29/14.
//  Copyright (c) 2014 Dubizzle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIView (DBZAdditions)

- (UIImage *)dbz_imageWithView:(UIView *)view;
- (UIView *)dbz_getSnapShot;

@end
