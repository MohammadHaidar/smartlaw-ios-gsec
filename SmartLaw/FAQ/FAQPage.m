//
//  FAQPage.m
//  SmartLaw
//
//  Created by Krystel Chaccour on 9/27/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import "FAQPage.h"

@interface FAQPage () <KMAccordionTableViewControllerDataSource, KMAccordionTableViewControllerDelegate>
@property (nonatomic) BOOL isReloaded;
@property NSArray *sections;
@property (nonatomic, retain) NSMutableArray *faq_array;
@property (nonatomic, retain) UIView *viewOfSection4, *viewOfSection5,*viewOfSection6,*viewOfSection7, *viewOfSection8,*viewOfSection9,*viewOfSection10,*viewOfSection11,*viewOfSection12,*viewOfSection13;
@property (nonatomic, retain) UILabel *faq4_label, *faq5_label, *faq6_label,*faq7_label, *faq8_label, *faq9_label,*faq10_label,*faq11_label, *faq12_label, *faq13_label;
@end

@implementation FAQPage
@synthesize titleView;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [self.view setBackgroundColor:[UIColor colorWithPatternImage:[UIImage imageNamed:@"Background"]]];

    if ([self respondsToSelector:@selector(edgesForExtendedLayout)])
        self.edgesForExtendedLayout = UIRectEdgeNone;
    if ([self respondsToSelector:@selector(automaticallyAdjustsScrollViewInsets)])
        self.automaticallyAdjustsScrollViewInsets = NO;
    
    // set the navigation bar style (color, tint, etc)
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.navigationBar.tintColor = [ColorTheme barBlack];
    self.navigationController.navigationBar.barTintColor = [ColorTheme barBlack];
    self.navigationController.navigationBar.barStyle = UIBarStyleBlackTranslucent;

    // create label for custom navigation bar title
    titleView = [[UILabel alloc] init];
    titleView.frame = CGRectMake(0, 0, 0, 44);
    [titleView setTextAlignment:NSTextAlignmentCenter];
    [titleView setTextColor:[UIColor whiteColor]];
    [titleView setText:JSLocalizedString(@"FAQT", @"")];
    [titleView setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]+4]];
    [titleView setBackgroundColor:[UIColor clearColor]];
    [self.navigationItem setTitleView:titleView];
    
    [self setUpDismissButton];
}

-(void)viewWillAppear:(BOOL)animated
{
    [self fetchFAQList];
    
    self.dataSource = self;
    self.delegate = self;
    [self setupAppearence];
}

/**
 Get list of faq content from local json file
 */
-(void)fetchFAQList
{
    NSString* path  = [[NSBundle mainBundle] pathForResource:@"FAQ" ofType:@"json"];
    NSString* jsonString = [[NSString alloc] initWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
    NSData* jsonData = [jsonString dataUsingEncoding:NSUTF8StringEncoding];
    NSError *error = nil;
    self.faq_array = [NSJSONSerialization JSONObjectWithData:jsonData options:0 error:&error];
   
    self.sections = [self getSectionArray];
}

- (void)setupAppearence {
    [self setHeaderHeight:60];
    [self setHeaderArrowImageClosed:[UIImage imageNamed:@"carat-open"]];
    [self setHeaderArrowImageOpened:[UIImage imageNamed:@"carat"]];
    [self setHeaderFont:[FontManager mediumFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [self setHeaderTitleColor:[ColorTheme textGrey]];
    [self setHeaderSeparatorColor:[ColorTheme pantoneGrey]];
    [self setHeaderColor:[ColorTheme lightGrey]];
    [self setOneSectionAlwaysOpen:NO];
}

/**
 Create the section views UI
 */
- (NSArray *)getSectionArray {
    
    self.isReloaded = NO;
    
//    self.viewOfSection1 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 500)];
//    KMSection *section1 = [KMSection new];
//    section1.view = self.viewOfSection1;
//    section1.title = [[self.faq_array objectAtIndex:0] objectForKey:@"Question"];
//    NSMutableAttributedString *strText = [[NSMutableAttributedString alloc] initWithString:[[self.faq_array objectAtIndex:0] objectForKey:@"Answer"]];
//    [strText addAttribute:NSFontAttributeName
//                    value:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]
//                    range:NSMakeRange(0, strText.length)];
//    
//    
//    self.faq1_label = [[UILabel alloc] init];
//    [self.faq1_label setTranslatesAutoresizingMaskIntoConstraints:NO];
//    [self.faq1_label setAttributedText:strText];
//    [self.faq1_label setTextAlignment:NSTextAlignmentRight];
//    [self.faq1_label setNumberOfLines:0];
//    [self.faq1_label setLineBreakMode:NSLineBreakByWordWrapping];
//    [self.faq1_label setTextColor:[ColorTheme textGrey]];
//    //[self.faq1_label setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
//    [self.faq1_label setBackgroundColor:[UIColor clearColor]];
//    [self.viewOfSection1 addSubview:self.faq1_label];
//    [self.faq1_label layoutIfNeeded];
//    
//    self.viewOfSection2 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 500)];
//    KMSection *section2 = [KMSection new];
//    section2.view = self.viewOfSection2;
//    section2.title = [[self.faq_array objectAtIndex:1] objectForKey:@"Question"];
//    
//    self.faq2_label = [[UILabel alloc] init];
//    [self.faq2_label setTranslatesAutoresizingMaskIntoConstraints:NO];
//    [self.faq2_label setText:[[self.faq_array objectAtIndex:1] objectForKey:@"Answer"]];
//    [self.faq2_label setTextAlignment:NSTextAlignmentRight];
//    [self.faq2_label setNumberOfLines:0];
//    [self.faq2_label setLineBreakMode:NSLineBreakByWordWrapping];
//    [self.faq2_label setTextColor:[ColorTheme textGrey]];
//    [self.faq2_label setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
//    [self.faq2_label setBackgroundColor:[UIColor clearColor]];
//    [self.viewOfSection2 addSubview:self.faq2_label];
//    [self.faq2_label layoutIfNeeded];
//    
//    self.viewOfSection3 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 500)];
//    KMSection *section3 = [KMSection new];
//    section3.view = self.viewOfSection3;
//    section3.title = [[self.faq_array objectAtIndex:2] objectForKey:@"Question"];
//    
//    self.faq3_label = [[UILabel alloc] init];
//    [self.faq3_label setTranslatesAutoresizingMaskIntoConstraints:NO];
//    [self.faq3_label setText:[[self.faq_array objectAtIndex:2] objectForKey:@"Answer"]];
//    [self.faq3_label setTextAlignment:NSTextAlignmentRight];
//    [self.faq3_label setNumberOfLines:0];
//    [self.faq3_label setLineBreakMode:NSLineBreakByWordWrapping];
//    [self.faq3_label setTextColor:[ColorTheme textGrey]];
//    [self.faq3_label setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
//    [self.faq3_label setBackgroundColor:[UIColor clearColor]];
//    [self.viewOfSection3 addSubview:self.faq3_label];
//    [self.faq3_label layoutIfNeeded];
    
    
    
        self.viewOfSection4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 580)];
        KMSection *section4 = [KMSection new];
        section4.view = self.viewOfSection4;
        section4.title = [[self.faq_array objectAtIndex:0] objectForKey:@"Question"];
        NSMutableAttributedString *strText = [[NSMutableAttributedString alloc] initWithString:[[self.faq_array objectAtIndex:0] objectForKey:@"Answer"]];
        [strText addAttribute:NSFontAttributeName
                        value:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]
                        range:NSMakeRange(0, strText.length)];
    
    
        self.faq4_label = [[UILabel alloc] init];
        [self.faq4_label setTranslatesAutoresizingMaskIntoConstraints:NO];
        [self.faq4_label setAttributedText:strText];
        [self.faq4_label setTextAlignment:NSTextAlignmentRight];
        [self.faq4_label setNumberOfLines:0];
        [self.faq4_label setLineBreakMode:NSLineBreakByWordWrapping];
        [self.faq4_label setTextColor:[ColorTheme textGrey]];
        //[self.faq4_label setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
        [self.faq4_label setBackgroundColor:[UIColor clearColor]];
        [self.viewOfSection4 addSubview:self.faq4_label];
        [self.faq4_label layoutIfNeeded];
//    self.viewOfSection4 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 580)];
//    KMSection *section4 = [KMSection new];
//    section4.view = self.viewOfSection4;
//    section4.title = [[self.faq_array objectAtIndex:0] objectForKey:@"Question"];
//    NSMutableAttributedString *strText = [[NSMutableAttributedString alloc] initWithString:[[self.faq_array objectAtIndex:0] objectForKey:@"Answer"]];
//        [strText addAttribute:NSFontAttributeName
//                        value:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]
//                        range:NSMakeRange(0, strText.length)];
//
//    
//    self.faq4_label = [[UILabel alloc] init];
//    [self.faq4_label setTranslatesAutoresizingMaskIntoConstraints:NO];
//    
//    [self.faq4_label setTextAlignment:NSTextAlignmentRight];
//    [self.faq4_label setNumberOfLines:0];
//    [self.faq4_label setLineBreakMode:NSLineBreakByWordWrapping];
//    [self.faq4_label setText:[[self.faq_array objectAtIndex:0] objectForKey:@"Answer"]];
//    [self.faq4_label setTextColor:[ColorTheme textGrey]];
//    [self.faq4_label setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
//    [self.viewOfSection4 addSubview:self.faq4_label];
//    [self.faq4_label layoutIfNeeded];

    
    self.viewOfSection5 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 500)];
    KMSection *section5 = [KMSection new];
    section5.view = self.viewOfSection5;
    section5.title = [[self.faq_array objectAtIndex:1] objectForKey:@"Question"];
    
    self.faq5_label = [[UILabel alloc] init];
    [self.faq5_label setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.faq5_label setText:[[self.faq_array objectAtIndex:1] objectForKey:@"Answer"]];
    [self.faq5_label setTextAlignment:NSTextAlignmentRight];
    [self.faq5_label setNumberOfLines:0];
    [self.faq5_label setLineBreakMode:NSLineBreakByWordWrapping];
    [self.faq5_label setTextColor:[ColorTheme textGrey]];
    [self.faq5_label setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [self.faq5_label setBackgroundColor:[UIColor clearColor]];
    [self.viewOfSection5 addSubview:self.faq5_label];
    [self.faq5_label layoutIfNeeded];

    self.viewOfSection6 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 400)];
    KMSection *section6 = [KMSection new];
    section6.view = self.viewOfSection6;
    section6.title = [[self.faq_array objectAtIndex:2] objectForKey:@"Question"];
    
    self.faq6_label = [[UILabel alloc] init];
    [self.faq6_label setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.faq6_label setText:[[self.faq_array objectAtIndex:2] objectForKey:@"Answer"]];
    [self.faq6_label setTextAlignment:NSTextAlignmentRight];
    [self.faq6_label setNumberOfLines:0];
    [self.faq6_label setLineBreakMode:NSLineBreakByWordWrapping];
    [self.faq6_label setTextColor:[ColorTheme textGrey]];
    [self.faq6_label setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [self.faq6_label setBackgroundColor:[UIColor clearColor]];
    [self.viewOfSection6 addSubview:self.faq6_label];
    [self.faq6_label layoutIfNeeded];
    
    self.viewOfSection7 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 150)];
    KMSection *section7 = [KMSection new];
    section7.view = self.viewOfSection7;
    section7.title = [[self.faq_array objectAtIndex:3] objectForKey:@"Question"];
    
    self.faq7_label = [[UILabel alloc] init];
    [self.faq7_label setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.faq7_label setText:[[self.faq_array objectAtIndex:3] objectForKey:@"Answer"]];
    [self.faq7_label setTextAlignment:NSTextAlignmentRight];
    [self.faq7_label setNumberOfLines:0];
    [self.faq7_label setLineBreakMode:NSLineBreakByWordWrapping];
    [self.faq7_label setTextColor:[ColorTheme textGrey]];
    [self.faq7_label setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [self.faq7_label setBackgroundColor:[UIColor clearColor]];
    [self.viewOfSection7 addSubview:self.faq7_label];
    [self.faq7_label layoutIfNeeded];
    
    self.viewOfSection8 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 100)];
    KMSection *section8 = [KMSection new];
    section8.view = self.viewOfSection8;
    section8.title = [[self.faq_array objectAtIndex:4] objectForKey:@"Question"];
    
    self.faq8_label = [[UILabel alloc] init];
    [self.faq8_label setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.faq8_label setText:[[self.faq_array objectAtIndex:4] objectForKey:@"Answer"]];
    [self.faq8_label setTextAlignment:NSTextAlignmentRight];
    [self.faq8_label setNumberOfLines:0];
    [self.faq8_label setLineBreakMode:NSLineBreakByWordWrapping];
    [self.faq8_label setTextColor:[ColorTheme textGrey]];
    [self.faq8_label setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [self.faq8_label setBackgroundColor:[UIColor clearColor]];
    [self.viewOfSection8 addSubview:self.faq8_label];
    [self.faq8_label layoutIfNeeded];
    
    self.viewOfSection9 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 100)];
    KMSection *section9 = [KMSection new];
    section9.view = self.viewOfSection9;
    section9.title = [[self.faq_array objectAtIndex:5] objectForKey:@"Question"];
    
    self.faq9_label = [[UILabel alloc] init];
    [self.faq9_label setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.faq9_label setText:[[self.faq_array objectAtIndex:5] objectForKey:@"Answer"]];
    [self.faq9_label setTextAlignment:NSTextAlignmentRight];
    [self.faq9_label setNumberOfLines:0];
    [self.faq9_label setLineBreakMode:NSLineBreakByWordWrapping];
    [self.faq9_label setTextColor:[ColorTheme textGrey]];
    [self.faq9_label setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [self.faq9_label setBackgroundColor:[UIColor clearColor]];
    [self.viewOfSection9 addSubview:self.faq9_label];
    [self.faq9_label layoutIfNeeded];
    
    self.viewOfSection10 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 250)];
    KMSection *section10 = [KMSection new];
    section10.view = self.viewOfSection10;
    section10.title = [[self.faq_array objectAtIndex:6] objectForKey:@"Question"];
    
    self.faq10_label = [[UILabel alloc] init];
    [self.faq10_label setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.faq10_label setText:[[self.faq_array objectAtIndex:6] objectForKey:@"Answer"]];
    [self.faq10_label setTextAlignment:NSTextAlignmentRight];
    [self.faq10_label setNumberOfLines:0];
    [self.faq10_label setLineBreakMode:NSLineBreakByWordWrapping];
    [self.faq10_label setTextColor:[ColorTheme textGrey]];
    [self.faq10_label setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [self.faq10_label setBackgroundColor:[UIColor clearColor]];
    [self.viewOfSection10 addSubview:self.faq10_label];
    [self.faq10_label layoutIfNeeded];
    
    self.viewOfSection11 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 450)];
    KMSection *section11 = [KMSection new];
    section11.view = self.viewOfSection11;
    section11.title = [[self.faq_array objectAtIndex:7] objectForKey:@"Question"];
    
    self.faq11_label = [[UILabel alloc] init];
    [self.faq11_label setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.faq11_label setText:[[self.faq_array objectAtIndex:7] objectForKey:@"Answer"]];
    [self.faq11_label setTextAlignment:NSTextAlignmentRight];
    [self.faq11_label setNumberOfLines:0];
    [self.faq11_label setLineBreakMode:NSLineBreakByWordWrapping];
    [self.faq11_label setTextColor:[ColorTheme textGrey]];
    [self.faq11_label setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [self.faq11_label setBackgroundColor:[UIColor clearColor]];
    [self.viewOfSection11 addSubview:self.faq11_label];
    [self.faq11_label layoutIfNeeded];
    
    self.viewOfSection12 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 280)];
    KMSection *section12 = [KMSection new];
    section12.view = self.viewOfSection12;
    section12.title = [[self.faq_array objectAtIndex:8] objectForKey:@"Question"];
    
    self.faq12_label = [[UILabel alloc] init];
    [self.faq12_label setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.faq12_label setText:[[self.faq_array objectAtIndex:8] objectForKey:@"Answer"]];
    [self.faq12_label setTextAlignment:NSTextAlignmentRight];
    [self.faq12_label setNumberOfLines:0];
    [self.faq12_label setLineBreakMode:NSLineBreakByWordWrapping];
    [self.faq12_label setTextColor:[ColorTheme textGrey]];
    [self.faq12_label setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [self.faq12_label setBackgroundColor:[UIColor clearColor]];
    [self.viewOfSection12 addSubview:self.faq12_label];
    [self.faq12_label layoutIfNeeded];

    self.viewOfSection13 = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 250)];
    KMSection *section13 = [KMSection new];
    section13.view = self.viewOfSection13;
    section13.title = [[self.faq_array objectAtIndex:9] objectForKey:@"Question"];
    
    self.faq13_label = [[UILabel alloc] init];
    [self.faq13_label setTranslatesAutoresizingMaskIntoConstraints:NO];
    [self.faq13_label setText:[[self.faq_array objectAtIndex:9] objectForKey:@"Answer"]];
    [self.faq13_label setTextAlignment:NSTextAlignmentRight];
    [self.faq13_label setNumberOfLines:0];
    [self.faq13_label setLineBreakMode:NSLineBreakByWordWrapping];
    [self.faq13_label setTextColor:[ColorTheme textGrey]];
    [self.faq13_label setFont:[FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]]];
    [self.faq13_label setBackgroundColor:[UIColor clearColor]];
    [self.viewOfSection13 addSubview:self.faq13_label];
    [self.faq13_label layoutIfNeeded];
    
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
//    [dict setObject:self.faq1_label forKey:@"faq1"];
//    [dict setObject:self.faq2_label forKey:@"faq2"];
//    [dict setObject:self.faq3_label forKey:@"faq3"];
    [dict setObject:self.faq4_label forKey:@"faq4"];
    [dict setObject:self.faq5_label forKey:@"faq5"];
    [dict setObject:self.faq6_label forKey:@"faq6"];
    [dict setObject:self.faq7_label forKey:@"faq7"];
    [dict setObject:self.faq8_label forKey:@"faq8"];
    [dict setObject:self.faq9_label forKey:@"faq9"];
    [dict setObject:self.faq10_label forKey:@"faq10"];
    [dict setObject:self.faq11_label forKey:@"faq11"];
    [dict setObject:self.faq12_label forKey:@"faq12"];
    [dict setObject:self.faq13_label forKey:@"faq13"];

    
//    [self.viewOfSection1 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[faq1]-10-|" options:0 metrics:nil views:dict]];
//    [self.viewOfSection1 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[faq1]-20-|" options:0 metrics:nil views:dict]];
//
//    [self.viewOfSection2 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[faq2]-10-|" options:0 metrics:nil views:dict]];
//    [self.viewOfSection2 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[faq2]-20-|" options:0 metrics:nil views:dict]];
//
//    [self.viewOfSection3 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[faq3]-10-|" options:0 metrics:nil views:dict]];
//    [self.viewOfSection3 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[faq3]-20-|" options:0 metrics:nil views:dict]];
    
    [self.viewOfSection4 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[faq4]-10-|" options:0 metrics:nil views:dict]];
    [self.viewOfSection4 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[faq4]-20-|" options:0 metrics:nil views:dict]];
    
    [self.viewOfSection5 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[faq5]-10-|" options:0 metrics:nil views:dict]];
    [self.viewOfSection5 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[faq5]-20-|" options:0 metrics:nil views:dict]];

    [self.viewOfSection6 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[faq6]-10-|" options:0 metrics:nil views:dict]];
    [self.viewOfSection6 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[faq6]-20-|" options:0 metrics:nil views:dict]];
    
    [self.viewOfSection7 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[faq7]-10-|" options:0 metrics:nil views:dict]];
    [self.viewOfSection7 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[faq7]-20-|" options:0 metrics:nil views:dict]];
    
    [self.viewOfSection8 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[faq8]-10-|" options:0 metrics:nil views:dict]];
    [self.viewOfSection8 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[faq8]-20-|" options:0 metrics:nil views:dict]];
    
    [self.viewOfSection9 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[faq9]-10-|" options:0 metrics:nil views:dict]];
    [self.viewOfSection9 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[faq9]-20-|" options:0 metrics:nil views:dict]];
    
    [self.viewOfSection10 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[faq10]-10-|" options:0 metrics:nil views:dict]];
    [self.viewOfSection10 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[faq10]-20-|" options:0 metrics:nil views:dict]];
    
    [self.viewOfSection11 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[faq11]-10-|" options:0 metrics:nil views:dict]];
    [self.viewOfSection11 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[faq11]-20-|" options:0 metrics:nil views:dict]];
    
    [self.viewOfSection12 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[faq12]-10-|" options:0 metrics:nil views:dict]];
    [self.viewOfSection12 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[faq12]-20-|" options:0 metrics:nil views:dict]];
    
    [self.viewOfSection13 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|-10-[faq13]-10-|" options:0 metrics:nil views:dict]];
    [self.viewOfSection13 addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|-20-[faq13]-20-|" options:0 metrics:nil views:dict]];
    
    

    
    return @[section4, section5, section6,section7, section8, section9,section10, section11, section12,section13];
}


#pragma mark - 
#pragma mark -

-(void)setUpDismissButton
{
    // add custom back button to navigation bar
    UIImage *dismiss_image= [UIImage imageNamed:@"Left-Arrow"];
    self.dismiss_btn = [UIButton buttonWithType:UIButtonTypeCustom];
    self.dismiss_btn.bounds = CGRectMake( 10, 0, dismiss_image.size.width, dismiss_image.size.height);
    [self.dismiss_btn addTarget:self action:@selector(dismissPage) forControlEvents:UIControlEventTouchUpInside];
    [self.dismiss_btn setImage:dismiss_image forState:UIControlStateNormal];
    
    UIBarButtonItem *leftButton = [[UIBarButtonItem alloc] initWithCustomView:self.dismiss_btn];
    self.navigationItem.leftBarButtonItem = leftButton;
    
}

// Function called to go back to the previous/parent view controller
-(void)dismissPage
{
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - KMAccordion Table
#pragma mark -

- (NSInteger)numberOfSectionsInAccordionTableViewController:(KMAccordionTableViewController *)accordionTableView {
    return [self.sections count];
}

- (KMSection *)accordionTableView:(KMAccordionTableViewController *)accordionTableView sectionForRowAtIndex:(NSInteger)index {
    return self.sections[index];
}

- (CGFloat)accordionTableView:(KMAccordionTableViewController *)accordionTableView heightForSectionAtIndex:(NSInteger)index{
    KMSection *section = self.sections[index];
    return section.view.frame.size.height;
}


- (UITableViewRowAnimation)accordionTableViewOpenAnimation:(KMAccordionTableViewController *)accordionTableView
{
    return UITableViewRowAnimationFade;
}

- (UITableViewRowAnimation)accordionTableViewCloseAnimation:(KMAccordionTableViewController *)accordionTableView
{
    return UITableViewRowAnimationFade;
}

- (void)accordionTableViewControllerSectionDidClose:(KMSection *)section
{
    self.isReloaded = NO;
}

- (void)accordionTableViewControllerSectionDidOpen:(KMSection *)section
{
    CGSize maximumLabelSize = CGSizeMake(self.view.frame.size.width -20, 1000);
    
//    if (section == self.sections[0])
//    {
//        if (self.isReloaded == NO)
//        {
//            int height = 0;
//            int width = 0;
//            
//            UIFont *fontDate = [FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]];
//            CGRect expectedLabelSizeDate = [[[self.faq_array objectAtIndex:0] objectForKey:@"Answer"] boundingRectWithSize:maximumLabelSize options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:fontDate} context:nil];
//            
//            height = expectedLabelSizeDate.size.height;
//            width = expectedLabelSizeDate.size.width;
//            
//            self.isReloaded = YES;
//            [self.viewOfSection1 setFrame:CGRectMake(0, 0, width, height + 40)];
//            [self reloadOpenedSection];
//            
//        }
//    }
//    else if (section == self.sections[1])
//    {
//        if (self.isReloaded == NO)
//        {
//            int height = 0;
//            int width = 0;
//            
//            UIFont *fontDate = [FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]];
//            CGRect expectedLabelSizeDate = [[[self.faq_array objectAtIndex:1] objectForKey:@"Answer"] boundingRectWithSize:maximumLabelSize options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:fontDate} context:nil];
//            
//            height = expectedLabelSizeDate.size.height;
//            width = expectedLabelSizeDate.size.width;
//            
//            self.isReloaded = YES;
//            [self.viewOfSection2 setFrame:CGRectMake(0, 0, width, height + 40)];
//            [self reloadOpenedSection];
//            
//        }
//    }
//    else if (section == self.sections[2])
//    {
//        if (self.isReloaded == NO)
//        {
//            int height = 0;
//            int width = 0;
//            
//            UIFont *fontDate = [FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]];
//            CGRect expectedLabelSizeDate = [[[self.faq_array objectAtIndex:2] objectForKey:@"Answer"] boundingRectWithSize:maximumLabelSize options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:fontDate} context:nil];
//            
//            height = expectedLabelSizeDate.size.height;
//            width = expectedLabelSizeDate.size.width;
//            
//            self.isReloaded = YES;
//            [self.viewOfSection3 setFrame:CGRectMake(0, 0, width, height + 40)];
//            [self reloadOpenedSection];
//            
//        }
//    }
     if (section == self.sections[0])
    {
        if (self.isReloaded == NO)
        {
            int height = 0;
            int width = 0;
            
            UIFont *fontDate = [FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]];
            CGRect expectedLabelSizeDate = [[[self.faq_array objectAtIndex:0] objectForKey:@"Answer"] boundingRectWithSize:maximumLabelSize options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:fontDate} context:nil];
            
            height = expectedLabelSizeDate.size.height;
            width = expectedLabelSizeDate.size.width;

            self.isReloaded = YES;
            [self.viewOfSection4 setFrame:CGRectMake(0, 0, width, height + 40)];
            [self reloadOpenedSection];
            
        }
    }
    else if (section == self.sections[1])
    {
        if (self.isReloaded == NO)
        {
            int height = 0;
            int width = 0;
            
            UIFont *fontDate = [FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]];
            CGRect expectedLabelSizeDate = [[[self.faq_array objectAtIndex:1] objectForKey:@"Answer"] boundingRectWithSize:maximumLabelSize options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:fontDate} context:nil];
            
            height = expectedLabelSizeDate.size.height;
            width = expectedLabelSizeDate.size.width;

            self.isReloaded = YES;
            [self.viewOfSection5 setFrame:CGRectMake(0, 0, width, height + 50)];
            [self reloadOpenedSection];
            
        }
    }
    else if (section == self.sections[2])
    {
        if (self.isReloaded == NO)
        {
            int height = 0;
            int width = 0;
            
            UIFont *fontDate = [FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]];
            CGRect expectedLabelSizeDate = [[[self.faq_array objectAtIndex:2] objectForKey:@"Answer"] boundingRectWithSize:maximumLabelSize options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:fontDate} context:nil];
            
            height = expectedLabelSizeDate.size.height;
            width = expectedLabelSizeDate.size.width;

            self.isReloaded = YES;
            [self.viewOfSection6 setFrame:CGRectMake(0, 0, width, height + 30)];
            [self reloadOpenedSection];
            
        }
    }
    else if (section == self.sections[3])
    {
        if (self.isReloaded == NO)
        {
            int height = 0;
            int width = 0;
            
            UIFont *fontDate = [FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]];
            CGRect expectedLabelSizeDate = [[[self.faq_array objectAtIndex:3] objectForKey:@"Answer"] boundingRectWithSize:maximumLabelSize options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:fontDate} context:nil];
            
            height = expectedLabelSizeDate.size.height;
            width = expectedLabelSizeDate.size.width;
            
            self.isReloaded = YES;
            [self.viewOfSection7 setFrame:CGRectMake(0, 0, width, height + 30)];
            [self reloadOpenedSection];
            
        }
    }
    else if (section == self.sections[4])
    {
        if (self.isReloaded == NO)
        {
            int height = 0;
            int width = 0;
            
            UIFont *fontDate = [FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]];
            CGRect expectedLabelSizeDate = [[[self.faq_array objectAtIndex:4] objectForKey:@"Answer"] boundingRectWithSize:maximumLabelSize options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:fontDate} context:nil];
            
            height = expectedLabelSizeDate.size.height;
            width = expectedLabelSizeDate.size.width;
            
            self.isReloaded = YES;
            [self.viewOfSection8 setFrame:CGRectMake(0, 0, width, height + 60)];
            [self reloadOpenedSection];
            
        }
    }
    else if (section == self.sections[5])
    {
        if (self.isReloaded == NO)
        {
            int height = 0;
            int width = 0;
            
            UIFont *fontDate = [FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]];
            CGRect expectedLabelSizeDate = [[[self.faq_array objectAtIndex:5] objectForKey:@"Answer"] boundingRectWithSize:maximumLabelSize options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:fontDate} context:nil];
            
            height = expectedLabelSizeDate.size.height;
            width = expectedLabelSizeDate.size.width;
            
            self.isReloaded = YES;
            [self.viewOfSection9 setFrame:CGRectMake(0, 0, width, height + 40)];
            [self reloadOpenedSection];
            
        }
    }
    else if (section == self.sections[6])
    {
        if (self.isReloaded == NO)
        {
            int height = 0;
            int width = 0;
            
            UIFont *fontDate = [FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]];
            CGRect expectedLabelSizeDate = [[[self.faq_array objectAtIndex:6] objectForKey:@"Answer"] boundingRectWithSize:maximumLabelSize options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:fontDate} context:nil];
            
            height = expectedLabelSizeDate.size.height;
            width = expectedLabelSizeDate.size.width;
            
            self.isReloaded = YES;
            [self.viewOfSection10 setFrame:CGRectMake(0, 0, width, height + 40)];
            [self reloadOpenedSection];
            
        }
    }
    else if (section == self.sections[7])
    {
        if (self.isReloaded == NO)
        {
            int height = 0;
            int width = 0;
            
            UIFont *fontDate = [FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]];
            CGRect expectedLabelSizeDate = [[[self.faq_array objectAtIndex:7] objectForKey:@"Answer"] boundingRectWithSize:maximumLabelSize options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:fontDate} context:nil];
            
            height = expectedLabelSizeDate.size.height;
            width = expectedLabelSizeDate.size.width;
            
            self.isReloaded = YES;
            [self.viewOfSection11 setFrame:CGRectMake(0, 0, width, height + 30)];
            [self reloadOpenedSection];
            
        }
    }
    else if (section == self.sections[8])
    {
        if (self.isReloaded == NO)
        {
            int height = 0;
            int width = 0;
            
            UIFont *fontDate = [FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]];
            CGRect expectedLabelSizeDate = [[[self.faq_array objectAtIndex:8] objectForKey:@"Answer"] boundingRectWithSize:maximumLabelSize options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:fontDate} context:nil];
            
            height = expectedLabelSizeDate.size.height;
            width = expectedLabelSizeDate.size.width;
            
            self.isReloaded = YES;
            [self.viewOfSection12 setFrame:CGRectMake(0, 0, width, height + 50)];
            [self reloadOpenedSection];
            
        }
    }
    else if (section == self.sections[9])
    {
        if (self.isReloaded == NO)
        {
            int height = 0;
            int width = 0;
            
            UIFont *fontDate = [FontManager lightFontOfSize:[JSLocalizedString(@"FontSize", @"") intValue]];
            CGRect expectedLabelSizeDate = [[[self.faq_array objectAtIndex:9] objectForKey:@"Answer"] boundingRectWithSize:maximumLabelSize options:NSStringDrawingUsesLineFragmentOrigin| NSStringDrawingUsesFontLeading attributes:@{NSFontAttributeName:fontDate} context:nil];
            
            height = expectedLabelSizeDate.size.height;
            width = expectedLabelSizeDate.size.width;
            
            self.isReloaded = YES;
            [self.viewOfSection13 setFrame:CGRectMake(0, 0, width, height + 30)];
            [self reloadOpenedSection];
            
        }
    }
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
