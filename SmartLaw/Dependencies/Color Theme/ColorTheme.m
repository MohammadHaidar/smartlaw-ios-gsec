//
//  ColorTheme.m
//  SmartLaw
//
//  Created by Krystel Chaccour on 3/1/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import "ColorTheme.h"

@implementation ColorTheme

+(UIColor *)colorFromHexString:(NSString *)hexString {
    unsigned rgbValue = 0;
    NSScanner *scanner = [NSScanner scannerWithString:hexString];
    [scanner setScanLocation:1]; // bypass '#' character
    [scanner scanHexInt:&rgbValue];
    return [UIColor colorWithRed:((rgbValue & 0xFF0000) >> 16)/255.0 green:((rgbValue & 0xFF00) >> 8)/255.0 blue:(rgbValue & 0xFF)/255.0 alpha:1.0];
}


#pragma mark - Background Colors
#pragma mark

+(UIColor *)backgroundGrey
{
    return [self colorFromHexString:background_grey];
}

+(UIColor *)backgroundBeige
{
    return [self colorFromHexString:background_beige];
}

#pragma mark - Smart Law
#pragma mark

+(UIColor *)slawRed
{
    return [self colorFromHexString:slaw_red];
}

+(UIColor *)slawLightRed
{
    return [self colorFromHexString:slaw_lightRed];
}

#pragma mark -
#pragma mark

+(UIColor *)pantoneGrey
{
    return [self colorFromHexString:pantone_grey];
}

+(UIColor *)textGrey
{
    return [self colorFromHexString:text_grey];
}

+(UIColor *)categGrey
{
    return [self colorFromHexString:categ_grey];
}

+(UIColor *)barBlack
{
    return [self colorFromHexString:bar_black];
}

+(UIColor *)lightGrey
{
    return [self colorFromHexString:light_grey];
}

+(UIColor *)barGrey
{
    return [self colorFromHexString:bar_grey];
}

#pragma mark - Highlight
#pragma mark

+(UIColor *)highlightBlue
{
    return [self colorFromHexString:highlight_blue];
}

+(UIColor *)highlightGreen
{
    return [self colorFromHexString:highlight_green];
}

+(UIColor *)highlightRed
{
    return [self colorFromHexString:highlight_red];
}

+(UIColor *)highlightYellow
{
    return [self colorFromHexString:highlight_yellow];
}

@end
