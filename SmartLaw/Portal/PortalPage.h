//
//  PortalPage.h
//  SmartLaw
//
//  Created by Krystel Chaccour on 9/27/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PortalPage : UIViewController
@property (nonatomic, retain) UIButton *dismiss_btn;
@property (nonatomic, retain) UILabel *titleView;

@end
