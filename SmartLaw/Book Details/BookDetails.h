//
//  BookDetails.h
//  SmartLaw
//
//  Created by Krystel Chaccour on 3/3/16.
//  Copyright © 2016 Krystel Chaccour. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BookDetails : UIViewController
@property (nonatomic, retain) NSMutableDictionary *bookDetails;
@property (nonatomic, assign) BOOL bookDownloaded;
@property (nonatomic, retain) UIScrollView *scroll_view;
@property (nonatomic, retain) UIImageView *book_image;

@property (retain, nonatomic) IBOutlet UIButton *download_Btn;
@property (retain, nonatomic) IBOutlet UIButton *open_Btn;
@property (retain, nonatomic) IBOutlet UIButton *delete_Btn;

@property (nonatomic, assign) BOOL isDownloaded;
@property (nonatomic, assign) BOOL bookIsDownloading;
@property (nonatomic, readonly) BOOL isDownloading;


-(void)loadInterface;
-(void)retrieveDetails;
-(IBAction)installEpub:(id)sender;
-(void)bookDownloaded:(NSNotification *)dict;
-(void)openItem;
-(BOOL)isDownloading;
-(void)downloadProgressed:(CGFloat)progress;
-(void)deleteBook;
-(void)reloadAfterDelete;
-(void)openBook:(BookInformation*)bi;
-(NSDate *)deserializeJsonDateString: (NSString *)jsonDateString;

@end
